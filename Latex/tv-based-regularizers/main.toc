\select@language {ngerman}
\contentsline {chapter}{\nonumberline List of Figures}{v}{chapter*.4}
\contentsline {chapter}{\numberline {1}Inverse Problems in Imaging}{3}{chapter.1}
\contentsline {chapter}{\numberline {2}TV Based Regularizers}{7}{chapter.2}
\contentsline {section}{\numberline {2.1}Total Variation}{7}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Background}{8}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Primal and Dual Formulations}{10}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}Basic Properties}{12}{subsection.2.1.3}
\contentsline {subsection}{\numberline {2.1.4}Nullspace}{15}{subsection.2.1.4}
\contentsline {subsection}{\numberline {2.1.5}Existence and Uniqueness of Solutions}{16}{subsection.2.1.5}
\contentsline {section}{\numberline {2.2}Total Generalized Variation}{20}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Background}{21}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Primal and Dual Formulations}{22}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Basic Properties}{25}{subsection.2.2.3}
\contentsline {subsection}{\numberline {2.2.4}Nullspace}{27}{subsection.2.2.4}
\contentsline {subsection}{\numberline {2.2.5}Topological Equivalence to Total Variation}{27}{subsection.2.2.5}
\contentsline {subsection}{\numberline {2.2.6}Existence and Uniqueness of Solutions}{29}{subsection.2.2.6}
\contentsline {section}{\numberline {2.3}Infimal Convolution Type Regularizers}{30}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Primal and Dual Formulations}{31}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Basic Properties}{32}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}Nullspace}{32}{subsection.2.3.3}
\contentsline {subsection}{\numberline {2.3.4}Topological Equivalence to Total Variation}{33}{subsection.2.3.4}
\contentsline {subsection}{\numberline {2.3.5}Existence and Uniqueness of Solutions}{34}{subsection.2.3.5}
\contentsline {chapter}{\numberline {3}A TV-Type Regularizer Promoting Piecewise-Lipschitz Reconstructions}{37}{chapter.3}
\contentsline {section}{\numberline {3.1}Background}{38}{section.3.1}
\contentsline {section}{\numberline {3.2}Primal and Dual Formulations}{42}{section.3.2}
\contentsline {section}{\numberline {3.3}Relationship with Total Variation}{45}{section.3.3}
\contentsline {section}{\numberline {3.4}Relationship with Infimal Convolution Type Regularizers}{46}{section.3.4}
\contentsline {section}{\numberline {3.5}Basic Properties}{47}{section.3.5}
\contentsline {section}{\numberline {3.6}Nullspace}{47}{section.3.6}
\contentsline {section}{\numberline {3.7}Existence and Uniqueness of Solutions}{48}{section.3.7}
\contentsline {chapter}{\numberline {4}Numerical Results}{50}{chapter.4}
\contentsline {section}{\numberline {4.1}Numerical Setting}{50}{section.4.1}
\contentsline {section}{\numberline {4.2}One-Dimensional Experiments}{56}{section.4.2}
\contentsline {section}{\numberline {4.3}Two-Dimensional Experiments}{66}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Synthetic Images}{69}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Natural Images}{72}{subsection.4.3.2}
\contentsline {chapter}{\numberline {5}Conclusion and Outlook}{80}{chapter.5}
\contentsline {chapter}{\nonumberline Bibliography}{82}{chapter*.50}
