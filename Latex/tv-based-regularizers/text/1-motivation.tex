\chapter{Inverse Problems in Imaging} 

\label{cha:introduction}

According to the well-known definition of J.B. Keller \cite{Ke}, two problems are called inverses of one another if the formulation of one problem involves the solution of the other problem.
The one that has been studied earlier and is better understood is called the forward problem and the other one the inverse problem. Another way of defining an inverse problem is as the task of determining from an observed or desired effect its cause while for a forward problem the effect is determined from the cause \cite{EnHaNe}. 
%The difference between an observed and a desired effect has mathematical consequences \cite{BuDi}. While for an observed effect one aims to find a unique solution of the problem, for a desired effect the uniqueness is less important since the aspired goal can be obtained in different ways.

An important application of inverse problems is in image processing.
From a mathematical point of view an image can be seen as a function which relates every point of the image domain to a certain color or grayscale value. We differentiate between the continuous and discrete definition \cite{BuDi}:
\begin{itemize}
\item Continuous definition: A continuous grayscale image can be defined as a function $u:\Omega\to\R$ where $\Omega\subset\R^2$ is typically a rectangular domain and $u$ associates every element to a grayscale value. For the extension to color images, $u$ maps every element to a vector: $u:\Omega\to\R^3$, i.e. we attach to every $x\in\Omega$ a red, green and blue value. In practice, the continuous representation cannot be realized on computers. But it provides a solid foundation for the analysis.
\item Discrete definition: We use a regular grid with $N\times M$ points. Then a digital grayscale image is defined as a matrix $u\in\R^{N\times M}$. The grayscale values in every grid point are also referred to as pixels. For the extension to color images the matrix is extended to $u\in\R^{N\times M\times 3}$.
\end{itemize}

%From a mathematical point of view an image can be seen as a function which relates every point of the image domain to a certain color or grayscale value. In practice an image that is obtained for example by photography, microscopy or X-ray scanning can exhibit noise. %Even digital pictures with high definition contain inaccurate information. 
%The task of denoising as formulated by \cite{BrLo} is as follows: Identify and remove the noise and conserve important information and structures at the same time. 

%This type of problem is called an inverse problem. The inverse problem as defined in \cite{Ri} is the task of determining from an observed effect its cause while for a forward problem the effect is determined from the cause. 
%As Keller stated in \cite{Ke} two problems are called inverses of one another if the formulation of one problem involves the solution of the other problem. 
%As explained in \cite{BeBo} one problem can be obtained from the other by exchanging the roles of the data and the unknowns. That is to say the unknowns of one problem are the data of the other one. 
%For the definition of a direct-inverse pair we need to specify what the causes and what the effects are and give the corresponding equations. Besides, the direct problem leads to a loss of information. That is to say the information content decreases which results in a higher smoothness of the solution.
%In our case we would call adding noise to an image the direct problem and denoising the noisy image the inverse problem.

Typical tasks in imaging are denoising, deblurring or the reconstruction of images from indirect measurements like the inverse Radon transformation in X-ray imaging.
Such a task can be formulated as an operator equation
\begin{eqnarray}\label{eq:inv_problem}
Au=f
\end{eqnarray}
where $u\in U$ is the solution we aim to obtain, $f\in F$ contains the data and $A: U\to F$ is a linear and bounded operator between two vector spaces, typically Hilbert or Banach spaces. We give a short overview of the imaging tasks above \cite{BuDi,BuOs,Bu}:

\begin{itemize}
\item Denoising: Due to errors in measurement, transmission errors et cetera we obtain noisy images which contain wrong or partly disturbed pixel values. The measurement can be given by
\begin{eqnarray}
f(x)=(Au)(x)=u(x)+n(x)\nonumber
\end{eqnarray}
for every $x\in\Omega$ where $n$ gives the noise in every pixel. The noisy image $f$ is typically assumed to be an element of the Hilbert space $L^2(\Omega)$ while the clean image $u$ is modeled as an element of a smaller function space $U$, for example the Sobolev space $W^{1,p}(\Omega)$ or in our case $BV(\Omega)\cap L^2(\Omega)$ where $BV(\Omega)$ is the space of functions with bounded variation. In the case of additive Gaussian noise, $n$ is assumed to satisfy
\begin{eqnarray}
\int_{\Omega}(n(x))^2~dx\leq\delta^2\nonumber
\end{eqnarray} 
where $\delta$ is a positive constant. The operator $A$ is the embedding operator into $L^2(\Omega)$. The task is to find the unknown $u$ from the noisy measurement $f$. 
%Of course there are severel possibilities to write a function as the sum of two functions. Therefore, it is reasonable to limit the options by making assumptions on the structure of $u$.

\item Deblurring: Blurring occurs when not the exact color value in every point but a local averaging is measured. Mathematically, this is formalized as an integral operator of the form
\begin{eqnarray}
f(x)=(Au)(x)=\int_{\Omega}k(x,y)u(y)~dy\nonumber
\end{eqnarray}
for every $x\in\Omega$. The integral kernel $k$ is typically assumed in $L^2(\Omega\times\Omega)$. A common example is Gaussian blur given by
\begin{eqnarray}
k(x,y)=c~exp(-\frac{|x-y|^2}{\sigma}).\nonumber
\end{eqnarray}
With the assumption $k\in L^2(\Omega\times\Omega)$, $A$ is a bounded operator from $L^2(\Omega)$ to $L^2(\Omega)$.

\item X-Ray imaging:  The X-ray or Radon transform is used to model the data acquisition in several imaging modalities such as computer tomography (CT) and Positron-emission tomography (PET). For a function $u:\R^2\to\R$ which represents the density in a domain $\Omega\subset\R^2$ (e.g. a cross section of a human body) the Radon transform is defined as
\begin{eqnarray}
f(s,w)=(Au)(s,w)=\int_{\R}u(sw+tw^{\perp})~dt\nonumber
\end{eqnarray}
for $w\in\R^2$ with $|w|=1$, $s\in\R_{+}$ and where $w^{\perp}$ is the orthogonal vector to $w$. For each X-ray, $s$ gives its distance from the origin and $w$ its unit normal vector. The inverse problem is to recover $u$ from its line integrals in $\Omega$. We assume that the decay $-\Delta I$ of the intensity along a small segment $\Delta t$ of an X-ray is proportional to the intensity $I$, the density $u$ and to $\Delta t$:
\begin{eqnarray}
\frac{\Delta I(sw+tw^{\perp})}{\Delta t}=-I(sw+tw^{\perp})u(sw+tw^{\perp}).\nonumber
\end{eqnarray}
For $\Delta t\to 0$ we get the ordinary differential equation
\begin{eqnarray}
\frac{dI(sw+tw^{\perp})}{dt}=-I(sw+tw^{\perp})u(sw+tw^{\perp}).\nonumber
\end{eqnarray}
Let $t=0$ be the position of the emitter and $t=L$ the position of the detector. Then with integration we obtain
\begin{eqnarray}
ln~I(sw+Lw^{\perp})-ln~I(sw)=\int_0^L u(sw+tw^{\perp})~dt.\nonumber
\end{eqnarray}
Since positions of emitters and detectors can be changed, the functions $I_L(s,w):=I(sw+Lw^{\perp})$ and $I_0:=I(sw)$ can for all $s$ and $w$ be measured at the emitters and detectors and $f$ can be extended to be zero  for $t\notin (0,L)$. Therefore, the inverse problem of computerized tomography  as the inversion of the Radon transform reads as follows:
\begin{eqnarray}
(Au)(s,w)=ln~I_0(s,w)-ln~I_L(s,w)\nonumber
\end{eqnarray} 
for $w\in\R^2$, $|w|=1$ and $s\in\R_+$.
\end{itemize}

A typical feature of inverse problems that makes them challenging is that they are often ill-posed in the sense of Hadamard \cite{Ha,Ha23}.

\begin{definition}
Let $U$ and $F$ be Banach or Hilbert spaces and let $A:U\to F$ be an operator. For $f\in F$ the problem \eqref{eq:inv_problem} is called  well-posed if the following three conditions hold:
\begin{enumerate}
\item There exists a solution of the problem $Au=f$ for every $f\in F$,
\item The solution of the problem is unique,
\item The solution continuously depends on the data, i.e. from $f_n\to f$ follows $u_n\to u$ where $Au_n=f_n$ for all $n\in\N$.
\end{enumerate}
\end{definition}

A problem is called ill-posed if it violates one or more of the three conditions above. 
%These postulates involve that for all admissible data the problem might not have a solution or solutions might not be unique or might not depend continuously on the data.
%Therefore, if a problem does not have a unique solution or no solution at all or if the data does not depend on the data continuously, we call the problem ill-posed. The consideration of ill-posed problems mostly commenced with the discovery of the ill-posedness of inverse problems.
Inverse problems are often ill-posed and most commonly the last condition is not fulfilled. 
If the stability condition is violated, the solution of the inverse problem is difficult to obtain numerically and often contains instabilities. For a stable approximation of the solution regularization methods are used. 
%Ideally, from these methods we obtain a functional that possesses a unique minimizer. 
%A typical situation is that the forward problem is well-posed while the inverse problem is ill-posed.
%As stated in \cite{BeBo} for a suitable mathematical setting the direct problem is well-posed while the inverse problem is ill-posed.
%In the field of imaging we provide this setting by the following assumptions. First we define the class of objects we want to regard. We describe these objects by functions with certain properties and we call the space of those functions the object space. Using a distance to indicate when two objects are close and when they are not we obtain a metric and therefore the object space is a metric space. Furthermore, we attach to every object a noise-free image which is rather smooth. This smoothness may not be true for the measured images since they can contain noise and which we therefore call noisy images. Last, we define the class of images such that it contains the noise-free images as well as the noisy images. We again introduce a distance and call the arising metric space the image space. 
%The procedure of attaching an image to an object defines an operator which in \eqref{eq:inv_problem} we already denoted by $A$. This operator transforms an object of the object space $U$ to an image of the image space $F$. The operator is continuous since images of two close objects are also close. The range of $A$ is the set of noise-free images and it does not coincide with the image space $F$ since $F$ also contains the noisy images.
%The inverse problem we are interested in is the problem of determining the object corresponding to a given image that is typically noisy.
Variational regularization is a popular approach for the regularization of inverse problems and in particular for mathematical imaging problems. It approximates solutions to \eqref{eq:inv_problem} with noisy data $f$ by solutions of the following minimization problem
\begin{eqnarray}\label{variationalapproach}
\min_u ~\mathcal{F}(Au,f)+\alpha \mathcal{R}(u).
\end{eqnarray}
Here $\mathcal{F}$ is the data fidelity term that measures the closeness between the predicted and the measured data.
%Useful assumptions are that the noise is independent from its neighborhood in every pixel and that it follows a certain distribution. 
%For our numerical experiments we will consider the case of additive Gaussian noise.  
$\mathcal{R}$ is the regularization term which forces the solution to have certain regularity. The weight $\alpha$ is called regularization parameter and balances the influence of the fidelity term and the regularizer. 

%The value obtained by the objective functional $\mathcal{F}(Au,f)+\alpha \mathcal{R}(u)$ indicates how much both the demand for a satisfying data fit and the compliance with the requested requirements are satisfied at the same time. So we are looking for a solution that minimizes the given functional.

%A possible approach for determining the existence of a minimizer of a functional is the direct method in the calculus of variations \cite{BrLo}. This method contains three essential steps. First, one examines whether the considered functional is bounded from below. Then we can conclude that there exists a minimizing sequence. Secondly, one shows that this sequence is element of a set that is compact with respect to the topology on the function space. Then there exists a convergent subsequence whose limit is a candidate for the minimizer. The third step is to show that the functional is lower semicontinuous with respect to the topology above. It follows that the limit of the subsequence is a minimizer of the functional.

The choice of the regularizer defines how regular the reconstruction will be. A popular regularizer in imaging is the total variation due to the fact that it enforces sufficient regularity while also preserving discontinuities. In imaging discontinuities are important since they correspond to edges of images. Based on the total variation approach several other regularizers have arisen which extend its scope. We aim to give an overview of the most important members of the total variation family: the total variation, the total generalized variation and some infimal convolution type regularizers. We also introduce a new regularizer in this family and study its properties.
% and therefore present the total variation and other regularization terms.
%==============================================================================

%\blindtext

%\section{Motivation}
%\label{sec:motivation}
%==============================================================================

% \blindtext \cite{Loepmeier:2015TR} \label{example:reference}

% %http://get-software.net/macros/latex/contrib/caption/subcaption.pdf
% \begin{figure}	
% 	\centering
% 	\begin{subfigure}[t]{0.45\textwidth}
% 		\caption{Logo des Institut für Informatik}
% 		\includegraphics[width=1.0\textwidth]{graphics/cs_logo_nl_blue.pdf}
% 	\end{subfigure}
% 	\begin{subfigure}[t]{0.45\textwidth}
% 		\caption{Logo der Uni}
% 		\includegraphics[width=1.0\textwidth]{graphics/wwu-logo-neu.pdf}
% 	\end{subfigure}

% 	\begin{subfigure}[t]{0.45\textwidth}		
% 		\phantomsubcaption\label{fig:inner}
% 		\missingfigure[figwidth=\textwidth]{Logo des FB10}
% 	\end{subfigure}
% 	\begin{subfigure}[t]{0.45\textwidth}
% 		\phantomsubcaption
% 		\missingfigure[figwidth=\textwidth]{Logo der Numerik}		
% 	\end{subfigure}
% \caption[Logos der Uni]{Logos der Uni. Dabei kann man auch wieder referenzieren wie auf \subref{fig:inner}. Dabei muss noch nicht einmal eine Unterüberschrift existieren. Dies ist besonders praktisch, falls die Buchstaben schon in den Bildern vorhanden sind. Details \href{http://get-software.net/macros/latex/contrib/caption/subcaption.pdf}{hier}
% }
% \label{fig:meth:bgsub:whole}
% \end{figure}
% %
% %\Blindtext
% %
% %\section{Thesis Structure}
% %\label{secStruktur}
% %%==============================================================================
% %
% %\Blindtext
