\chapter{Inverse Problems in Imaging} 

\label{cha:introduction}

From a mathematical point of view an image can be seen as a function which relates every point of a domain of definition to a certain colour value. In case of poor lighting conditions an image that is obtained for example by photography, microscopy or scanning can exhibit noise. Even digital pictures with high definition contain inaccurate information. The task of denoising as formulated by \cite{BrLo} is as follows: Identify and remove the noise and conserve important information and structures at the same time. 

This type of problem is called an inverse problem. The inverse problem as defined in \cite{Ri} is the conclusion from an observed effect on its cause while for a direct problem the effect is determined from the cause. 
As Keller stated in \cite{Ke} two problems are called inverses of one another if the formulation of one problem involves the solution of the other problem. 
As explained in \cite{BeBo} one problem can be obtained from the other by exchanging the roles of the data and the unknowns. That is to say the unknowns of one problem are the data of the other one. 
For the definition of a direct-inverse pair we need to specify what the causes and what the effects are and give the corresponding equations. Besides, the direct problem leads to a loss of information. That is to say the information content decreases which results in a higher smoothness of the solution.
In our case we would call adding noise to an image the direct problem and denoising the noisy image the inverse problem.

For the purpose of reconstructing such an image from corrupted data we observe the ill-posed problem
\begin{eqnarray}\label{eq:inv_problem}
Au=f
\end{eqnarray}
where $u\in U$ is the solution we aim to obtain, $f\in F$ contains the data and $A: U\to F$ is a linear and bounded operator between two vector spaces. A problem is called ill-posed if it does not fulfill Hadamard's postulates of well-posedness he stated in \cite{Ha} and \cite{Ha23}. These postulates involve that for all admissible data the problem might not have a solution or solutions might not be unique or might not depend continuously on the data. Therefore, if a problem does not have a unique solution or no solution at all or if the data does not depend on the data continuously, we call the problem ill-posed. The consideration of ill-posed problems mostly commenced with the discovery of the ill-posedness of inverse problems. As stated in \cite{BeBo} for a suitable mathematical setting the direct problem is well-posed while the inverse problem is ill-posed.

In the field of imaging we provide this setting by the following assumptions. First we define the class of objects we want to regard. We describe these objects by functions with certain properties and we call the space of those functions the object space. Using a distance to indicate when two objects are close and when they are not we obtain a metric and therefore the object space is a metric space. Furthermore, we attach to every object a noise-free image which is rather smooth. This smoothness may not be true for the measured images since they can contain noise and which we therefore call noisy images. Last, we define the class of images such that it contains the noise-free images as well as the noisy images. We again introduce a distance and call the arising metric space the image space. 

The procedure of attaching an image to an object defines an operator which in \eqref{eq:inv_problem} we already denoted by $A$. This operator transforms an object of the object space $U$ to an image of the image space $F$. The operator is continuous since images of two close objects are also close. The range of $A$ is the set of noise-free images and it does not coincide with the image space $F$ since $F$ also contains the noisy images.

The inverse problem we are interested in is the problem of determining the object corresponding to a given image that is typically noisy.

The variational approach most commonly used for inverse problems and in particular for mathematical imaging problems is of the form
\begin{eqnarray}\label{variationalapproach}
\min_u ~\mathcal{F}(Au,f)+\alpha \mathcal{R}(u).
\end{eqnarray}
Here $\mathcal{F}$ is the data fidelity term, it gives the amplitude of the noise. Useful assumptions are that the noise is independent from its neighborhood in every pixel and that it follows a certain distribution. For our numerical experiments we will consider the case of additive Gaussian noise.  

$\mathcal{R}$ is the regularization term which determines the structure of the solution and is used to report for every image how much it corresponds to a natural image. The weight $\alpha$ is an appropriate regularization parameter. The value obtained by the objective functional $\mathcal{F}(Au,f)+\alpha \mathcal{R}(u)$ indicates how much both the demand for a satisfying data fit and the compliance of a natural image are satisfied at the same time. So we are looking for a solution that minimizes the given functional.

A possible approach for determining the existence of a minimizer of a functional is the direct method in the calculus of variations \cite{BrLo}. This method contains three essential steps. First, one examines whether the considered functional is bounded from below. Then we can conclude that there exists a minimizing sequence. Secondly, one shows that this sequence is element of a set that is compact with respect to the topology on the function space. Then there exists a convergent subsequence whose limit is a candidate for the minimizer. The third step is to show that the functional is lower semicontinuous with respect to the topology above. It follows that the limit of the subsequence is a minimizer of the functional.

Below we will focus on the choice of the regularization term $\mathcal{R}$ and therefore present the total variation and other regularization terms.
%==============================================================================

%\blindtext

%\section{Motivation}
\label{sec:motivation}
%==============================================================================

% \blindtext \cite{Loepmeier:2015TR} \label{example:reference}

% %http://get-software.net/macros/latex/contrib/caption/subcaption.pdf
% \begin{figure}	
% 	\centering
% 	\begin{subfigure}[t]{0.45\textwidth}
% 		\caption{Logo des Institut für Informatik}
% 		\includegraphics[width=1.0\textwidth]{graphics/cs_logo_nl_blue.pdf}
% 	\end{subfigure}
% 	\begin{subfigure}[t]{0.45\textwidth}
% 		\caption{Logo der Uni}
% 		\includegraphics[width=1.0\textwidth]{graphics/wwu-logo-neu.pdf}
% 	\end{subfigure}

% 	\begin{subfigure}[t]{0.45\textwidth}		
% 		\phantomsubcaption\label{fig:inner}
% 		\missingfigure[figwidth=\textwidth]{Logo des FB10}
% 	\end{subfigure}
% 	\begin{subfigure}[t]{0.45\textwidth}
% 		\phantomsubcaption
% 		\missingfigure[figwidth=\textwidth]{Logo der Numerik}		
% 	\end{subfigure}
% \caption[Logos der Uni]{Logos der Uni. Dabei kann man auch wieder referenzieren wie auf \subref{fig:inner}. Dabei muss noch nicht einmal eine Unterüberschrift existieren. Dies ist besonders praktisch, falls die Buchstaben schon in den Bildern vorhanden sind. Details \href{http://get-software.net/macros/latex/contrib/caption/subcaption.pdf}{hier}
% }
% \label{fig:meth:bgsub:whole}
% \end{figure}
% %
% %\Blindtext
% %
% %\section{Thesis Structure}
% %\label{secStruktur}
% %%==============================================================================
% %
% %\Blindtext
