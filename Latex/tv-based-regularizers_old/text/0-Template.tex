\chapter*{Introduction}

Image processing is a modern but extremely fast growing field of research
which today is mainly driven by digital photography and medical imaging.
Typical tasks in image processing are noise reduction, sharpening or completion of the image.

Even images taken with modern digital cameras contain noise. Typically, an increase in resolution entails an increase in noise. The image sensor of the camera determines information about the brightness from the number of captured photons. But since the emission of photons is a random
process the measurement must also be modeled by a random variable
and is thus afflicted with an error \cite{BrLo}. Further use of a noisy image such as for computer vision or even the wish for a visually more appealing image demands a reduction of the contained noise.

Image reconstruction belongs to the problems of calculating the causal factors from the observations. We cannot measure the desired information directly and therefore need to reconstruct it from the available data. These kind of problems are called inverse problems since the causes are calculated from the result. Inverse problems are often ill-posed which means that there might not exist a solution of the problem, the solution might not be unique or the solution might not depend continuously on the data. To receive a well-posed problem we add a regularization term which forces the solution to suffice desired requirements \cite{BuDi}. This thesis is concerned with the choice of the regularization term. 

One of the earlier approaches in image reconstruction is the total variation regularization. Reducing the total variation of an image reduces detail and preserves important structures such as edges. Solutions of this approach are typically piecewise constant which leads to the so-called staircasing effect. 

Based on the total variation various other regularization terms have arisen. The total generalized variation involves derivatives of higher order and leads to smoother solutions. For instance the total generalized variation of second order creates piecewise affine images which leads to a reduction of the criticized staircasing effect. Another possibility is to use infimal convolution type regularizers. For example convoluting the Radon norm involved in the total variation regularization with an $L^p$-norm leads to smooth solutions. For the case $p\to\infty$ one observes that solutions become piecewise affine similar to the case of the total generalized variation of second order.

Inspired by the total variation and the total variation based regularizers above we establish a new regularizer which convolutes the Radon norm and the characteristic function of a closed set. By this means the derivative of the solution is allowed a wider scope and the solutions are piecewise Lipschitz continuous.

After giving a short introduction to variational methods in chapter \ref{cha:introduction}, we introduce some already known regularizers in chapter \ref{cha:tv_based}. We start with the total variation and from there explicate the total generalized variation and the infimal convolution type regularizer $TVL^p$. For these regularization functionals we state a primal and dual formulation and we examine their basic properties. We investigate the nullspaces of the regularizers for a better understanding of the structure of solutions and for the total generalized variation and the $TVL^p$ functional depict the topological equivalence with the total variation. Finally, we focus on the existence and uniqueness of solutions of the erupting optimization problems.

In chapter \ref{cha:tv_pwl} we introduce the new regularizer $TV_{pwL}^{\gamma}$ promoting piecewise Lipschitz solutions. We examine the same properties like for the regularizers above and here, too, investigate the existence and uniqueness of solutions of the optimization problem.

Afterwards, in chapter \ref{cha:experiments} we compile a discrete setting for the optimization problems and apply the different regularizers to one-dimensional discrete functions and two-dimensional images considering the different structures of the solutions.

In chapter \ref{cha:thesis_outcome} we come to an conclusion and we give a short outlook.

% Auch hier sollte ein Text stehen. Jedes Kapitel braucht ein paar einleitende Sätze. Niemals beginnt ein Kapitel mit einem neuen Abschnitt (\textit{Sektion})!
% \section{Anleitung zum Template}
% \label{secHowTo}
% %==============================================================================
% Für die  Benutzung des \gls{FB10}-Templates gibt es einige wichtige Regeln:
% \begin{enumerate}
%     \item Benutze \gls{pdflatex} oder \texttt{latexmk} zum kompilieren des  Latex Quellcodes. Für Linux liegt eine Makefile bei.

% 	\item Wir versuchen immer Vektorgrafiken zu verwenden (\texttt{svg}, \texttt{ai}, \texttt{pdf}) und vermeiden gerasterte Grafiken (\texttt{jpg}, \texttt{png}, \texttt{tiff}, \texttt{gif}, \texttt{bmp}). Als Programme bieten sich dabei \href{http://www.inkscape.org/}{Inkscape} (OpenSource), Adobe Illustrator (z.B. auf dem IVV5-Terminalserver) oder CorelDraw (Gibt es beim ZIV) an. 
% 	\item Bevor du das aussehen dieses Templates veränderst, sprich mit deinem Betreuer
% 	\item Füge gelesenen Paper und ähnliche in die \texttt{bibliography.bib} ein; Benutze dabei \href{http://jabref.sourceforge.net/}{JabRef} als Editor.
% 	\item Floating Umgebungen (Tabellen, Abbildungen, etc) machen sich oft oben auf der Seite ganz gut.
% 	\item Jede Floating-Umgebung hat immer eine kurze, als auf eine lange Beschreibung! 
% 	\item Quellcode sollte in einer Extra-Datei gespeichert werden und mittels entsprechenden Befehl in das Dokument eingebunden werden. Sehr kurze Abschnitte kann man auch direkt in die Tex-File schreiben.
% 	\item In einer Abschlussarbeit findet sich in der Regel nicht sehr viel Quellcode....
% \end{enumerate}

% \section{\LaTeX Examples}

% Zu den folgenden Elementen finden sich Beispiele in dieser Vorlage
% \begin{itemize}
%     \item Aufzählungen, also das hier
% 	\item Aufzählungen auf Seite \pageref{example:enumeration}
% 	\item Beschreibungen auf Seite \pageref{example:description}
% 	\item Tabellen auf Seite \pageref{tab:table}
% 	\item Quellcode auf Seite \pageref{lst:useless}
% 	\item abgesetzte Mathematische Formeln Seite \pageref{eqn:formula}
% 	\item Quellenverweise Seite \pageref{example:reference}
% 	\item Acronyme, siehe das Paket \texttt{glossaries} 
% 	\item Glossar, siehe die Datei \texttt{glossary.tex}
% 	\item Bytefelder, siehe Seite \pageref{fig:bytefield}
% 	\item Referenzierung von Abschnitten, Abbildungen, Quellcode, Tabellen und vielen anderen Sachen finden sich im gesamten Dokument
% 	\item Abbildung auf Seite \pageref{fig:smiley}
% 	\item Theoreme, Satz, Beweis, Definition, inklusive einer Umgebung für Formeln die über mehrere Zeilen gehen auf Seite \pageref{example:theorem.multiline}
% 	\item Mathematische Diagramme auf Seite \pageref{example:diagram}
% 	\item Unterabbildungen auf Seite \pageref{fig:meth:bgsub:whole}
% 	\item Den Mathemodus innerhalb einer Zeile, z.B. $\exists x \in \{1,\frac{3}{2},2,\ldots,9\}$
% 	\item Anmerkungen mittels des Todo-Paketes \pageref{example:todo} und auch für fehlende Bilder \pageref{fig:meth:bgsub:whole}
% \end{itemize}

% Zur Verwendung des Glossars muss der Befehl \texttt{makeindex} wie folgt benutzt werden:
% \begin{verbatim}
% makeindex -s main.ist -t main.glg -o main.gls main.glo
% \end{verbatim}
% Für die Liste der Akronyme
% \begin{verbatim}
% makeindex -s main.ist -t main.alg -o main.acr main.acn
% \end{verbatim}
% Unter Linux kann auch die MakeFile benutzt werden. Dazu einfach auf der Konsole im jeweiligen Ordner \texttt{make} eingeben. 
% Alle temporären Dateien lassen sich mit \texttt{make clean} löschen.

% \newpage
% \section{Weitere Dokumentation}
% Wir haben auf unserer Homepage einige praktische Dokumente für die Arbeit mit Latex gesammelt, siehe \href{http://www.uni-muenster.de/Comsys/en/teaching/technical_writing.html}{hier}.
% Latex-Pakete und deren Doumentation finden sich in der Regel im \href{http://www.ctan.org/}{Comprehensive TeX Archive Network}.

% Einige Links zu den wichtigsten Paketen:
% \begin{itemize}
% 	\item \href{http://tug.ctan.org/cgi-bin/ctanPackageInformation.py?id=bytefield}{bytefield - Create illustrations for network protocol specifications}
% 	\item \href{http://tug.ctan.org/cgi-bin/ctanPackageInformation.py?id=colortbl}{colortbl - Add colour to LaTeX tables}
% 	\item \href{http://tug.ctan.org/cgi-bin/ctanPackageInformation.py?id=eqnarray}{eqnarray - More generalised equation arrays with numbering}
% 	\item \href{http://tug.ctan.org/cgi-bin/ctanPackageInformation.py?id=glossaries}{glossaries - Create glossaries and lists of acronyms}
% 	\item \href{http://tug.ctan.org/cgi-bin/ctanPackageInformation.py?id=graphicx}{graphicx - Enhanced support for graphics}
% 	\item \href{http://tug.ctan.org/cgi-bin/ctanPackageInformation.py?id=listings}{listings - Typeset source code listings using LaTeX}
% 	\item \href{http://tug.ctan.org/cgi-bin/ctanPackageInformation.py?id=pdflscape}{pdflscape - Make landscape pages display as landscape}
% 	\item \href{http://tug.ctan.org/cgi-bin/ctanPackageInformation.py?id=supertabular}{supertabular - A multi-page tables package}
% 	\item \href{http://tug.ctan.org/cgi-bin/ctanPackageInformation.py?id=xcolor}{xcolor - Driver-independent color extensions for LaTeX and pdfLaTeX}
% \end{itemize}
