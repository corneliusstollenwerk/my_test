\select@language {ngerman}
\contentsline {chapter}{\nonumberline List of Figures}{v}{chapter*.4}
\contentsline {chapter}{\nonumberline List of Tables}{vi}{chapter*.5}
\contentsline {chapter}{\numberline {1}Inverse Problems in Imaging}{3}{chapter.1}
\contentsline {chapter}{\numberline {2}TV Based Regularizers}{6}{chapter.2}
\contentsline {section}{\numberline {2.1}Total Variation}{6}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Background}{7}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Primal and Dual Formulation}{10}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}Basic Properties}{11}{subsection.2.1.3}
\contentsline {subsection}{\numberline {2.1.4}Nullspace}{16}{subsection.2.1.4}
\contentsline {subsection}{\numberline {2.1.5}Existence and Uniqueness of Solutions}{17}{subsection.2.1.5}
\contentsline {section}{\numberline {2.2}Total Generalized Variation}{20}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Background}{20}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Primal and Dual Formulation}{22}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Basic Properties}{24}{subsection.2.2.3}
\contentsline {subsection}{\numberline {2.2.4}Nullspace}{27}{subsection.2.2.4}
\contentsline {subsection}{\numberline {2.2.5}Topological Equivalence to Total Variation}{27}{subsection.2.2.5}
\contentsline {subsection}{\numberline {2.2.6}Existence and Uniqueness of Solutions}{30}{subsection.2.2.6}
\contentsline {section}{\numberline {2.3}Infimal Convolution Type Regularizers}{31}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Primal and Dual Formulation}{31}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Properties}{33}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}Nullspace}{35}{subsection.2.3.3}
\contentsline {subsection}{\numberline {2.3.4}Topological Equivalence to Total Variation}{36}{subsection.2.3.4}
\contentsline {subsection}{\numberline {2.3.5}Existence and Uniqueness of Solutions}{36}{subsection.2.3.5}
\contentsline {chapter}{\numberline {3}A TV-Type Regularizer Promoting Piecewise-Lipschitz Reconstructions}{39}{chapter.3}
\contentsline {section}{\numberline {3.1}Background}{40}{section.3.1}
\contentsline {section}{\numberline {3.2}Primal and Dual Formulation}{43}{section.3.2}
\contentsline {section}{\numberline {3.3}Relationship with the Total Variation}{46}{section.3.3}
\contentsline {section}{\numberline {3.4}Relationship with Infimal Convolution Type Regularizers}{47}{section.3.4}
\contentsline {section}{\numberline {3.5}Basic Properties}{48}{section.3.5}
\contentsline {section}{\numberline {3.6}Nullspace}{52}{section.3.6}
\contentsline {section}{\numberline {3.7}Existence and Uniqueness of Solutions}{53}{section.3.7}
\contentsline {chapter}{\numberline {4}Numerical Results}{55}{chapter.4}
\contentsline {section}{\numberline {4.1}Numerical Setting}{55}{section.4.1}
\contentsline {section}{\numberline {4.2}One-Dimensional Results}{60}{section.4.2}
\contentsline {section}{\numberline {4.3}Two-Dimensional Results}{72}{section.4.3}
\contentsline {chapter}{\numberline {5}Conclusion and Outlook}{85}{chapter.5}
\contentsline {chapter}{\nonumberline Bibliography}{87}{chapter*.56}
