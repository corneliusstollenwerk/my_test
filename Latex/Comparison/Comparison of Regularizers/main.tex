\documentclass[ paper=a4, 11pt, cleardoubleempty, fancyheaders, oneside, openright, BCOR=7mm, DIV12, listof=totoc, index=totoc,bibliography=totoc, headinclude=false]{scrartcl}

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[colorinlistoftodos]{todonotes}
\usepackage[onehalfspacing]{setspace}
\usepackage{subcaption}
\captionsetup[subfigure]{list=true, font=normalsize, labelfont=normalsize, 
labelformat=brace, position=top, justification=RaggedRight}
\usepackage{parskip}

\title{Comparison of $TV_{pwL}$ with different Regularizers}

\author{}

\date{\today}

\begin{document}
\maketitle

We want to compare the new $TV_{pwL}^{\gamma}$ regularization functional to other regularizers. We aim to choose the parameters for the total variation, the $TVL^p_{\alpha,\beta}$ functional and the total generalized variation such that the arising reconstructions resemble the $TV_{pwL}^{\gamma}$ reconstructions.

\section{Comparison with the Total Variation}

We start by comparing the $TV_{pwL}^{\gamma}$ functional to the total variation. For the $TV_{pwL}^{\gamma}$ regularizer we use the following discrete optimization problem:
\begin{eqnarray}\label{eq:num_tvpwl_modelnoise}
\min_{u\in\mathcal{U}}||Du-g||_1& \quad s.t.& ||g||_{\infty}\leq\gamma,\nonumber\\
&&||u-f||_2\leq B
\end{eqnarray}
where $\gamma$ is a positive constant the the bound $B$ for the fidelity term is given by
\begin{eqnarray}\label{eq:num_bound2d}
B= \sqrt{MN}\cdot\delta\cdot\max\limits_{\substack{i=1,\dots,M\\j=1,\dots,N}}(|v_{i,j}|).
\end{eqnarray}
For the total variation we want to vary the parameter $\alpha$ and we therefore choose the optimization problem given by
\begin{eqnarray}\label{eq:num_tv_modelnonoise}
\min_{u\in\mathcal{U}}~\frac{1}{2} ||u-f||_2^2+\alpha||Du||_1
\end{eqnarray}
where $\alpha$ is a positive constant.

\begin{figure}
\centering
\begin{subfigure}[t]{0.29\textwidth}
\includegraphics[trim={0cm 0cm 0cm 0cm},width=\textwidth]{graphics/comp_tv_butterfly/orig.eps}
\caption{Original image}
\label{subfig:comp_tv_butterfly_orig}
\end{subfigure}\qquad
\begin{subfigure}[t]{0.29\textwidth}
\includegraphics[trim={0cm 0cm 0cm 0cm},width=\textwidth]{graphics/comp_tv_butterfly/noisy.eps}
\caption{Noisy image}
\label{subfig:comp_tv_butterfly_noisy}
\end{subfigure}\qquad
\begin{subfigure}[t]{0.29\textwidth}
\includegraphics[trim={0cm 0cm 0cm 0cm},width=\textwidth]{graphics/comp_tv_butterfly/tvpwl.eps}
\caption{Reconstruction with $TV_{pwL}^{\gamma}$, $\gamma=24.6667$}
\label{subfig:comp_tv_butterfly_tvpwl}
\end{subfigure}\\
\begin{subfigure}[t]{0.29\textwidth}
\includegraphics[trim={0cm 0cm 0cm 0cm},width=\textwidth]{graphics/comp_tv_butterfly/tv1.eps}
\caption{Reconstruction with $TV$, $\alpha=10$}
\label{subfig:comp_tv_butterfly_tv1}
\end{subfigure}\qquad
\begin{subfigure}[t]{0.29\textwidth}
\includegraphics[trim={0cm 0cm 0cm 0cm},width=\textwidth]{graphics/comp_tv_butterfly/tv2.eps}
\caption{Reconstruction with $TV$, $\alpha=15$}
\label{subfig:comp_tv_butterfly_tv2}
\end{subfigure}\qquad
\begin{subfigure}[t]{0.29\textwidth}
\includegraphics[trim={0cm 0cm 0cm 0cm},width=\textwidth]{graphics/comp_tv_butterfly/tv3.eps}
\caption{Reconstruction with $TV$, $\alpha=20$}
\label{subfig:comp_tv_butterfly_tv3}
\end{subfigure}
\caption[Reconstruction butterfly image with $TV_{pwL}$ and $TV$]{Noisy butterfly image with $\delta=0.1$ and reconstructions for $TV_{pwL}$ and $TV$ with different $\alpha$ values.}
\label{fig:comp_tv_butterfly}
\end{figure}
We consider two natural grayscale images for which the results using the $TV_{pwL}^{\gamma}$ regularizer are satisfying. We start with the butterfly image. Figure \ref{fig:comp_tv_butterfly} shows the original image, the image corrupted by Gaussian noise with $\delta=0.1$ and the reconstructions with the $TV_{pwL}^{\gamma}$ regularizer and the total variation for different $\alpha$ values. Here the discrete problems \eqref{eq:num_tvpwl_modelnoise} and \eqref{eq:num_tv_modelnonoise} were used. The parameter $\gamma$ for the $TV_{pwL}^{\gamma}$ functional is chosen to obtain the highest possible SSIM value.
For the total variation the parameter $\alpha=10$ gives an image that contains more noise than the $TV_{pwL}^{\gamma}$ reconstruction.
The image reconstructed with the value $\alpha=15$ shows the highest resemblance with the $TV_{pwL}^{\gamma}$ reconstruction. Only the contrast is slightly higher and we observe blocky artifacts, for example at the edges of the leaves. Therefore, some of the fine details of the butterfly wings are lost which are more clearly visible in the $TV_{pwL}^{\gamma}$ reconstruction.
For the parameter $\alpha=20$ the total variation reconstruction is smoother than the $TV_{pwL}^{\gamma}$ reconstruction. There are greater areas of the same shade and the contrast is higher. On the other hand we clearly observe the staircasing effect for example in the background and at the edges of the wings.
These results match the SSIM values in Table \ref{tab:comp_tv_butterfly} where we not only calculated the SSIM values between the different reconstructions and the original image but also between the total variation reconstructions and the $TV_{pwL}^{\gamma}$ reconstruction. Obviously the value is highest for the reconstruction with $\alpha=15$ which supports our observations.

\begin{table}
\centering
\begin{tabular}{|p{2.5cm}|p{3cm}|p{3cm}|}
\hline
 & $SSIM$& $SSIM_{TV_{pwL}}$ \\
 \hline
 $TV_{pwL}$&$ 0.6082$ & \\
 \hline
 $TV$, $\alpha=10$&$ 0.5505$ &$0.7685 $ \\
 \hline
 $TV$, $\alpha=15$&$ 0.6332$ &$ 0.8533$ \\
 \hline
 $TV$, $\alpha=20$&$0.6556 $ &$0.8253 $ \\
 \hline
\end{tabular}
 \caption[SSIM for $TV_{pwL}^{\gamma}$ and $TV$: Butterfly]{SSIM values for reconstruction with $TV_{pwL}^{\gamma}$ and $TV$ of butterfly image with ${\delta=0.1}$.}
 \label{tab:comp_tv_butterfly}
\end{table}

\begin{figure}
\centering
\begin{subfigure}[t]{0.29\textwidth}
\includegraphics[trim={0cm 0cm 0cm 0cm},width=\textwidth]{graphics/comp_tv_owl/orig.eps}
\caption{Original image}
\label{subfig:comp_tv_owl_orig}
\end{subfigure}\qquad
\begin{subfigure}[t]{0.29\textwidth}
\includegraphics[trim={0cm 0cm 0cm 0cm},width=\textwidth]{graphics/comp_tv_owl/noisy.eps}
\caption{Noisy image}
\label{subfig:comp_tv_owl_noisy}
\end{subfigure}\qquad
\begin{subfigure}[t]{0.29\textwidth}
\includegraphics[trim={0cm 0cm 0cm 0cm},width=\textwidth]{graphics/comp_tv_owl/tvpwl.eps}
\caption{Reconstruction with $TV_{pwL}^{\gamma}$, $\gamma=34.4444$}
\label{subfig:comp_tv_owl_tvpwl}
\end{subfigure}\\
\begin{subfigure}[t]{0.29\textwidth}
\includegraphics[trim={0cm 0cm 0cm 0cm},width=\textwidth]{graphics/comp_tv_owl/tv1.eps}
\caption{Reconstruction with $TV$, $\alpha=15$}
\label{subfig:comp_tv_owl_tv1}
\end{subfigure}\qquad
\begin{subfigure}[t]{0.29\textwidth}
\includegraphics[trim={0cm 0cm 0cm 0cm},width=\textwidth]{graphics/comp_tv_owl/tv2.eps}
\caption{Reconstruction with $TV$, $\alpha=22$}
\label{subfig:comp_tv_owl_tv2}
\end{subfigure}\qquad
\begin{subfigure}[t]{0.29\textwidth}
\includegraphics[trim={0cm 0cm 0cm 0cm},width=\textwidth]{graphics/comp_tv_owl/tv3.eps}
\caption{Reconstruction with $TV$, $\alpha=30$}
\label{subfig:comp_tv_owl_tv3}
\end{subfigure}
\caption[Reconstruction owl image with $TV_{pwL}$ and $TV$]{Noisy owl image with $\delta=0.15$ and reconstructions for $TV_{pwL}$ and $TV$ with different $\alpha$ values.}
\label{fig:comp_tv_owl}
\end{figure}
We now turn to the image of the owl for which the $TV_{pwL}^{\gamma}$ performs especially well. Figure \ref{fig:comp_tv_owl} shows the original image, the noisy image corrupted by Gaussian noise with $\delta=0.15$ and the reconstructions for the $TV_{pwL}^{\gamma}$ functional and the total variation with different $\alpha$ values.
The SSIM values between the reconstructions and the original image and between the total variation reconstructions and the $TV_{pwL}^{\gamma}$ reconstruction are given in Table \ref{tab:comp_tv_owl}.
Again for the $TV_{pwL}^{\gamma}$ functional we choose the parameter $\gamma$ to obtain the highest SSIM value. 
For the total variation reconstruction with $\alpha=15$ we have more noise and a higher contrast than in the $TV_{pwL}^{\gamma}$ reconstruction.
For the value $\alpha=22$ we have the highest similarity with the $TV_{pwL}^{\gamma}$ reconstruction matching the SSIM value between the two. Only the contrast is clearly higher and we notice a slight loss of fine detail such as for the feathers and the fir needles in the background.
For $\alpha=30$ the total variation reconstruction is smoother than the $TV_{pwL}^{\gamma}$ reconstruction. Here as well, the contrast is higher and we observe the staircasing effect, for example visible in the background and for the feathers of the owl.

\begin{table}
\centering
\begin{tabular}{|p{2.5cm}|p{3cm}|p{3cm}|}
\hline
 & $SSIM$& $SSIM_{TV_{pwL}}$ \\
 \hline
 $TV_{pwL}$&$ 0.5151$ & \\
 \hline
 $TV$, $\alpha=15$&$ 0.4942$ &$0.7688 $ \\
 \hline
 $TV$, $\alpha=22$&$ 0.5362$ &$ 0.8458$ \\
 \hline
 $TV$, $\alpha=30$&$0.5007 $ &$0.7603 $ \\
 \hline
\end{tabular}
 \caption[SSIM for $TV_{pwL}^{\gamma}$ and $TV$: Owl]{SSIM values for reconstruction with $TV_{pwL}^{\gamma}$ and $TV$ of owl image with ${\delta=0.15}$.}
 \label{tab:comp_tv_owl}
\end{table}

\section{Comparison with $TVL^{\infty}_{\alpha,\beta}$}

In this section we compare the results for the $TV_{pwL}^{\gamma}$ functional to results for the $TVL^p_{\alpha,\beta}$ functional were we use $p=\infty$.
For the $TV_{pwL}^{\gamma}$ functional we use the discrete problem from \eqref{eq:num_tvpwl_modelnoise} and for the $TVL^{\infty}_{\alpha,\beta}$ functional we use the optimization problem given by
\begin{eqnarray}\label{eq:num_tvlp_modelnoise}
\min_{u\in\mathcal{U}}||Du-g||_1+\beta ||g||_p& \quad s.t.& ||u-f||_2\leq B
\end{eqnarray}
where the bound $B$ is given by \eqref{eq:num_bound2d}. We vary the parameter $\beta$ and examine whether we can find similarities between the $TVL^{\infty}_{\alpha,\beta}$ and the $TV_{pwL}^{\gamma}$ results.

\begin{figure}
\centering
\begin{subfigure}[t]{0.29\textwidth}
\includegraphics[trim={0cm 0cm 0cm 0cm},width=\textwidth]{graphics/comp_tvlinf_butterfly/orig.eps}
\caption{Original image}
\label{subfig:comp_tvlinf_butterfly_orig}
\end{subfigure}\qquad
\begin{subfigure}[t]{0.29\textwidth}
\includegraphics[trim={0cm 0cm 0cm 0cm},width=\textwidth]{graphics/comp_tvlinf_butterfly/noisy.eps}
\caption{Noisy image}
\label{subfig:comp_tvlinf_butterfly_noisy}
\end{subfigure}\qquad
\begin{subfigure}[t]{0.29\textwidth}
\includegraphics[trim={0cm 0cm 0cm 0cm},width=\textwidth]{graphics/comp_tvlinf_butterfly/tvpwl.eps}
\caption{Reconstruction with $TV_{pwL}^{\gamma}$, $\gamma=24.6667$}
\label{subfig:comp_tvlinf_butterfly_tvpwl}
\end{subfigure}\\
\begin{subfigure}[t]{0.29\textwidth}
\includegraphics[trim={0cm 0cm 0cm 0cm},width=\textwidth]{graphics/comp_tvlinf_butterfly/tv1.eps}
\caption{Reconstruction with $TVL^{\infty}_{\alpha,\beta}$, $\beta=5000$}
\label{subfig:comp_tvlinf_butterfly_tv1}
\end{subfigure}\qquad
\begin{subfigure}[t]{0.29\textwidth}
\includegraphics[trim={0cm 0cm 0cm 0cm},width=\textwidth]{graphics/comp_tvlinf_butterfly/tv2.eps}
\caption{Reconstruction with $TVL^{\infty}_{\alpha,\beta}$, $\beta=17000$}
\label{subfig:comp_tvlinf_butterfly_tv2}
\end{subfigure}\qquad
\begin{subfigure}[t]{0.29\textwidth}
\includegraphics[trim={0cm 0cm 0cm 0cm},width=\textwidth]{graphics/comp_tvlinf_butterfly/tv3.eps}
\caption{Reconstruction with $TVL^{\infty}_{\alpha,\beta}$, $\beta=40889$}
\label{subfig:comp_tvlinf_butterfly_tv3}
\end{subfigure}
\caption[Reconstruction butterfly image with $TV_{pwL}$ and $TV$]{Noisy butterfly image with $\delta=0.1$ and reconstructions for $TV_{pwL}$ and $TVL^{\infty}_{\alpha,\beta}$ with different $\beta$ values.}
\label{fig:comp_tvlinf_butterfly}
\end{figure}
Again we start with the image of the butterfly corrupted by Gaussian noise where $\delta=0.1$. Figure \ref{fig:comp_tvlinf_butterfly} shows the results for the $TV_{pwL}^{\gamma}$ functional and the $TVL^{\infty}_{\alpha,\beta}$ functional with different $\beta$ values.
For the $TV_{pwL}^{\gamma}$ reconstruction the parameter $\gamma$ is optimized to achieve the highest SSIM value.
For the $TVL^{\infty}_{\alpha,\beta}$ reconstruction with $\beta=5000$ the image bears resemblance with the $TV_{pwL}^{\gamma}$ reconstruction but it contains more noise and is lower in contrast.
For $\beta=17000$ the $TVL^{\infty}_{\alpha,\beta}$ reconstruction highly resembles the $TV_{pwL}^{\gamma}$ reconstruction. Only the contrast is slightly lower.
For $\beta=40889$ the $TVL^{\infty}_{\alpha,\beta}$ reconstruction is smoother than the $TV_{pwL}^{\gamma}$ reconstruction. The image shows less noise  and is higher in contrast. On the other hand we notice some blocky structures in the background of the image.
These observations match the values in Table \ref{tab:comp_tvlinf_owl} where the SSIM between the $TV_{pwL}^{\gamma}$ reconstruction and the $TVL^{\infty}_{\alpha,\beta}$ reconstruction is highest for the parameter $\beta=17000$.

\begin{table}
\centering
\begin{tabular}{|p{3.5cm}|p{3cm}|p{3cm}|}
\hline
 & $SSIM$& $SSIM_{TV_{pwL}}$ \\
 \hline
 $TV_{pwL}$&$ 0.6082$ & \\
 \hline
 $TVL^{\infty}_{\alpha,\beta}$, $\beta=5000$&$ 0.5574$ &$0.9501 $ \\
 \hline
 $TVL^{\infty}_{\alpha,\beta}$, $\beta=17000$&$ 0.5792$ &$ 0.9564$ \\
 \hline
 $TVL^{\infty}_{\alpha,\beta}$, $\beta=40889$&$0.6319 $ &$0.8525 $ \\
 \hline
\end{tabular}
 \caption[SSIM for $TV_{pwL}^{\gamma}$ and $TVL^{\infty}_{\alpha,\beta}$: Butterfly]{SSIM values for reconstruction with $TV_{pwL}^{\gamma}$ and $TVL^{\infty}_{\alpha,\beta}$ of butterfly image with ${\delta=0.1}$.}
 \label{tab:comp_tvlinf_butterfly}
\end{table}

\begin{figure}
\centering
\begin{subfigure}[t]{0.29\textwidth}
\includegraphics[trim={0cm 0cm 0cm 0cm},width=\textwidth]{graphics/comp_tvlinf_owl/orig.eps}
\caption{Original image}
\label{subfig:comp_tvlinf_owl_orig}
\end{subfigure}\qquad
\begin{subfigure}[t]{0.29\textwidth}
\includegraphics[trim={0cm 0cm 0cm 0cm},width=\textwidth]{graphics/comp_tvlinf_owl/noisy.eps}
\caption{Noisy image}
\label{subfig:comp_tvlinf_owl_noisy}
\end{subfigure}\qquad
\begin{subfigure}[t]{0.29\textwidth}
\includegraphics[trim={0cm 0cm 0cm 0cm},width=\textwidth]{graphics/comp_tvlinf_owl/tvpwl.eps}
\caption{Reconstruction with $TV_{pwL}^{\gamma}$, $\gamma=34.4444$}
\label{subfig:comp_tvlinf_owl_tvpwl}
\end{subfigure}\\
\begin{subfigure}[t]{0.29\textwidth}
\includegraphics[trim={0cm 0cm 0cm 0cm},width=\textwidth]{graphics/comp_tvlinf_owl/tv1.eps}
\caption{Reconstruction with $TVL^{\infty}_{\alpha,\beta}$, $\beta=0$}
\label{subfig:comp_tvlinf_owl_tv1}
\end{subfigure}\qquad
\begin{subfigure}[t]{0.29\textwidth}
\includegraphics[trim={0cm 0cm 0cm 0cm},width=\textwidth]{graphics/comp_tvlinf_owl/tv2.eps}
\caption{Reconstruction with $TVL^{\infty}_{\alpha,\beta}$, $\beta=1$}
\label{subfig:comp_tvlinf_owl_tv2}
\end{subfigure}\qquad
\begin{subfigure}[t]{0.29\textwidth}
\includegraphics[trim={0cm 0cm 0cm 0cm},width=\textwidth]{graphics/comp_tvlinf_owl/tv3.eps}
\caption{Reconstruction with $TVL^{\infty}_{\alpha,\beta}$, $\beta=100000$}
\label{subfig:comp_tvlinf_owl_tv3}
\end{subfigure}
\caption[Reconstruction owl image with $TV_{pwL}$ and $TV$]{Noisy owl image with $\delta=0.15$ and reconstructions for $TV_{pwL}$ and $TVL^{\infty}_{\alpha,\beta}$ with different $\beta$ values.}
\label{fig:comp_tvlinf_owl}
\end{figure}
Continuing with the owl image, Figure \ref{fig:comp_tvlinf_owl} shows the original image, the noisy image with $\delta=0.15$ and the reconstructions with $TV_{pwL}^{\gamma}$ and with $TVL^{\infty}_{\alpha,\beta}$ for different $\beta$ values.
For $\beta=0$ the $TVL^{\infty}_{\alpha,\beta}$ reconstruction bears the highest resemblance with the $TV_{pwL}^{\gamma}$ reconstruction. Both images contain fine structures but are lower in contrast than the original image. The $TVL^{\infty}_{\alpha,\beta}$ reconstruction holds more noise than the $TV_{pwL}^{\gamma}$ reconstruction but on the other hand is slightly higher in contrast.
The $TVL^{\infty}_{\alpha,\beta}$ reconstruction with $\beta=1$ is blurrier than the $TV_{pwL}^{\gamma}$ reconstruction. Then again the contrast is also higher.
For $\beta=100000$ the two reconstructions barely show any resemblance. The $TVL^{\infty}_{\alpha,\beta}$ reconstruction is higher in contrast but also looks slightly unnatural. In the background and for the feathers of the owl we notice blocky artifacts.
The SSIM values between the reconstructions and the original image and between the $TVL^{\infty}_{\alpha,\beta}$ reconstructions and the $TV_{pwL}^{\gamma}$ reconstruction are given in Table \ref{tab:comp_tvlinf_owl}.

\begin{table}
\centering
\begin{tabular}{|p{3.5cm}|p{3cm}|p{3cm}|}
\hline
 & $SSIM$& $SSIM_{TV_{pwL}}$ \\
 \hline
 $TV_{pwL}$&$ 0.5151$ & \\
 \hline
 $TVL^{\infty}_{\alpha,\beta}$, $\beta=0$&$ 0.5040$ &$0.9814 $ \\
 \hline
 $TVL^{\infty}_{\alpha,\beta}$, $\beta=1$&$ 0.4555$ &$ 0.8568$ \\
 \hline
 $TVL^{\infty}_{\alpha,\beta}$, $\beta=100000$&$0.4087 $ &$0.5924 $ \\
 \hline
\end{tabular}
 \caption[SSIM for $TV_{pwL}^{\gamma}$ and $TVL^{\infty}_{\alpha,\beta}$: Owl]{SSIM values for reconstruction with $TV_{pwL}^{\gamma}$ and $TVL^{\infty}_{\alpha,\beta}$ of owl image with ${\delta=0.15}$.}
 \label{tab:comp_tvlinf_owl}
\end{table}

\section{Comparison with $TVL^{2}_{\alpha,\beta}$}

We now want to compare the $TV_{pwL}^{\gamma}$ reconstructions with $TVL^p_{\alpha,\beta}$ reconstructions for the case $p=2$. We use the discrete optimization models \eqref{eq:num_tvpwl_modelnoise} and \eqref{eq:num_tvlp_modelnoise} where we insert the parameter $p=2$. Again we vary the parameter $\beta$ and we try to achieve results visually similar to the $TV_{pwL}^{\gamma}$ reconstruction.

\begin{figure}
\centering
\begin{subfigure}[t]{0.29\textwidth}
\includegraphics[trim={0cm 0cm 0cm 0cm},width=\textwidth]{graphics/comp_tvl2_butterfly/orig.eps}
\caption{Original image}
\label{subfig:comp_tvl2_butterfly_orig}
\end{subfigure}\qquad
\begin{subfigure}[t]{0.29\textwidth}
\includegraphics[trim={0cm 0cm 0cm 0cm},width=\textwidth]{graphics/comp_tvl2_butterfly/noisy.eps}
\caption{Noisy image}
\label{subfig:comp_tvl2_butterfly_noisy}
\end{subfigure}\qquad
\begin{subfigure}[t]{0.29\textwidth}
\includegraphics[trim={0cm 0cm 0cm 0cm},width=\textwidth]{graphics/comp_tvl2_butterfly/tvpwl.eps}
\caption{Reconstruction with $TV_{pwL}^{\gamma}$, $\gamma=24.6667$}
\label{subfig:comp_tvl2_butterfly_tvpwl}
\end{subfigure}\\
\begin{subfigure}[t]{0.29\textwidth}
\includegraphics[trim={0cm 0cm 0cm 0cm},width=\textwidth]{graphics/comp_tvl2_butterfly/tv1.eps}
\caption{Reconstruction with $TVL^2_{\alpha,\beta}$, $\beta=1$}
\label{subfig:comp_tvl2_butterfly_tv1}
\end{subfigure}\qquad
\begin{subfigure}[t]{0.29\textwidth}
\includegraphics[trim={0cm 0cm 0cm 0cm},width=\textwidth]{graphics/comp_tvl2_butterfly/tv2.eps}
\caption{Reconstruction with $TVL^2_{\alpha,\beta}$, $\beta=140$}
\label{subfig:comp_tvl2_butterfly_tv2}
\end{subfigure}\qquad
\begin{subfigure}[t]{0.29\textwidth}
\includegraphics[trim={0cm 0cm 0cm 0cm},width=\textwidth]{graphics/comp_tvl2_butterfly/tv3.eps}
\caption{Reconstruction with $TVL^2_{\alpha,\beta}$, $\beta=166.4444$}
\label{subfig:comp_tvl2_butterfly_tv3}
\end{subfigure}
\caption[Reconstruction butterfly image with $TV_{pwL}$ and $TV$]{Noisy butterfly image with $\delta=0.1$ and reconstructions for $TV_{pwL}$ and $TVL^{2}_{\alpha,\beta}$ with different $\beta$ values.}
\label{fig:comp_tvl2_butterfly}
\end{figure}
We start with the butterfly image where the results for the $TV_{pwL}^{\gamma}$ regularizer and the $TVL^{2}_{\alpha,\beta}$ functional with different $\beta$ values are shown in Figure \ref{fig:comp_tvl2_butterfly}.
For the value $\beta=1$ the $TVL^{2}_{\alpha,\beta}$ reconstruction shows the highest similarity with the $TV_{pwL}^{\gamma}$ reconstruction. The $TVL^{2}_{\alpha,\beta}$ reconstruction is slightly lower in contrast and therefore misses some of the fine structures on the butterfly wings.
For $\beta=140$ the $TVL^{2}_{\alpha,\beta}$ reconstruction obtains less noise than the $TV_{pwL}^{\gamma}$ reconstruction. The image is smoother but also contains less fine details.
The $TVL^{2}_{\alpha,\beta}$ reconstruction with $\beta=166.4444$ shows very different structures than the $TV_{pwL}^{\gamma}$ reconstruction. The image is smoother and appears almost blurry which is especially visible in the background. Also the fine structures of the butterfly wings are almost completely lost.
These observations match the SSIM values in Table \ref{tab:comp_tvl2_butterfly} where the SSIM value between the $TV_{pwL}^{\gamma}$ reconstruction and the $TVL^{2}_{\alpha,\beta}$ reconstruction is highest for $\beta=1$ and lowest for $\beta=166.4444$.

\begin{table}
\centering
\begin{tabular}{|p{4cm}|p{3cm}|p{3cm}|}
\hline
 & $SSIM$& $SSIM_{TV_{pwL}}$ \\
 \hline
 $TV_{pwL}$&$ 0.6082$ & \\
 \hline
 $TVL^{2}_{\alpha,\beta}$, $\beta=1$&$ 0.6294$ &$0.9636 $ \\
 \hline
 $TVL^{2}_{\alpha,\beta}$, $\beta=140$&$ 0.6355$ &$ 0.9207$ \\
 \hline
 $TVL^{2}_{\alpha,\beta}$, $\beta=166.4444$&$0.6378 $ &$0.8790 $ \\
 \hline
\end{tabular}
 \caption[SSIM for $TV_{pwL}^{\gamma}$ and $TVL^{\infty}_{\alpha,\beta}$: Butterfly]{SSIM values for reconstruction with $TV_{pwL}^{\gamma}$ and $TVL^{2}_{\alpha,\beta}$ of butterfly image with ${\delta=0.1}$.}
 \label{tab:comp_tvl2_butterfly}
\end{table}

\begin{figure}
\centering
\begin{subfigure}[t]{0.29\textwidth}
\includegraphics[trim={0cm 0cm 0cm 0cm},width=\textwidth]{graphics/comp_tvl2_owl/orig.eps}
\caption{Original image}
\label{subfig:comp_tvl2_owl_orig}
\end{subfigure}\qquad
\begin{subfigure}[t]{0.29\textwidth}
\includegraphics[trim={0cm 0cm 0cm 0cm},width=\textwidth]{graphics/comp_tvl2_owl/noisy.eps}
\caption{Noisy image}
\label{subfig:comp_tvl2_owl_noisy}
\end{subfigure}\qquad
\begin{subfigure}[t]{0.29\textwidth}
\includegraphics[trim={0cm 0cm 0cm 0cm},width=\textwidth]{graphics/comp_tvl2_owl/tvpwl.eps}
\caption{Reconstruction with $TV_{pwL}^{\gamma}$, $\gamma=34.4444$}
\label{subfig:comp_tvl2_owl_tvpwl}
\end{subfigure}\\
\begin{subfigure}[t]{0.29\textwidth}
\includegraphics[trim={0cm 0cm 0cm 0cm},width=\textwidth]{graphics/comp_tvl2_owl/tv1.eps}
\caption{Reconstruction with $TVL^2_{\alpha,\beta}$, $\beta=0$}
\label{subfig:comp_tvl2_owl_tv1}
\end{subfigure}\qquad
\begin{subfigure}[t]{0.29\textwidth}
\includegraphics[trim={0cm 0cm 0cm 0cm},width=\textwidth]{graphics/comp_tvl2_owl/tv2.eps}
\caption{Reconstruction with $TVL^2_{\alpha,\beta}$, $\beta=27.7778$}
\label{subfig:comp_tvl2_owl_tv2}
\end{subfigure}\qquad
\begin{subfigure}[t]{0.29\textwidth}
\includegraphics[trim={0cm 0cm 0cm 0cm},width=\textwidth]{graphics/comp_tvl2_owl/tv3.eps}
\caption{Reconstruction with $TVL^2_{\alpha,\beta}$, $\beta=500$}
\label{subfig:comp_tvl2_owl_tv3}
\end{subfigure}
\caption[Reconstruction owl image with $TV_{pwL}$ and $TV$]{Noisy owl image with $\delta=0.15$ and reconstructions for $TV_{pwL}$ and $TVL^{2}_{\alpha,\beta}$ with different $\beta$ values.}
\label{fig:comp_tvl2_owl}
\end{figure}
We now turn to the owl image corrupted by Gaussian noise with $\delta=0.15$. Results for the $TV_{pwL}^{\gamma}$ functional and the $TVL^{2}_{\alpha,\beta}$ regularizer with different $\beta$ values are shown in Figure \ref{fig:comp_tvl2_owl}. 
The SSIM values between the reconstructions and the original image and between the $TVL^{2}_{\alpha,\beta}$ reconstructions and the $TV_{pwL}^{\gamma}$ reconstruction are given in Table \ref{tab:comp_tvl2_owl}. 
For the value $\beta=0$ the $TVL^{2}_{\alpha,\beta}$ reconstruction contains more noise than the $TV_{pwL}^{\gamma}$ reconstruction. Also the contrast is lower.
For $\beta=27.7778$ the SSIM value with the $TV_{pwL}^{\gamma}$ reconstruction is highest. Nevertheless, the $TVL^{2}_{\alpha,\beta}$ reconstruction looks blurrier. The contrast is higher but the fine structures of the image, for example the fir needles and the feathers, are lost.
For $\beta=500$ the $TVL^{2}_{\alpha,\beta}$ reconstructions looks very different from the $TV_{pwL}^{\gamma}$ reconstruction. The image is smoother and even contains blocky structures, for instance in the background and for the feathers of the owl.

\begin{table}
\centering
\begin{tabular}{|p{4cm}|p{3cm}|p{3cm}|}
\hline
 & $SSIM$& $SSIM_{TV_{pwL}}$ \\
 \hline
 $TV_{pwL}$&$ 0.5151$ & \\
 \hline
 $TVL^{2}_{\alpha,\beta}$, $\beta=0$&$ 0.3419$ &$0.6409 $ \\
 \hline
 $TVL^{2}_{\alpha,\beta}$, $\beta=27.7778$&$ 0.4572$ &$ 0.8154$ \\
 \hline
 $TVL^{2}_{\alpha,\beta}$, $\beta=500$&$0.4087 $ &$0.5924 $ \\
 \hline
\end{tabular}
 \caption[SSIM for $TV_{pwL}^{\gamma}$ and $TVL^{\infty}_{\alpha,\beta}$: Owl]{SSIM values for reconstruction with $TV_{pwL}^{\gamma}$ and $TVL^{2}_{\alpha,\beta}$ of owl image with ${\delta=0.15}$.}
 \label{tab:comp_tvl2_owl}
\end{table}

\section{Comparison with $TGV^2_{\alpha,\beta}$}

We want to compare the $TV_{pwL}^{\gamma}$ regularizer to the total generalized variation of second order. Therefore, we use the discrete optimization problem \eqref{eq:num_tvpwl_modelnoise}  for the $TV_{pwL}^{\gamma}$ functional and for the total generalized variation we use the following problem:
\begin{eqnarray}\label{eq:num_tgv_modelnoise}
\min_{u\in\mathcal{U}}||Du-g||_1+\beta ||Eg||_1& \quad s.t.& ||u-f||_2\leq B
\end{eqnarray}
where the bound is given by \eqref{eq:num_bound2d}. We vary the positive parameter $\beta$ and compare the arising results to the results of the $TV_{pwL}^{\gamma}$ regularizer.

\begin{figure}
\centering
\begin{subfigure}[t]{0.29\textwidth}
\includegraphics[trim={0cm 0cm 0cm 0cm},width=\textwidth]{graphics/comp_tgv_butterfly/orig.eps}
\caption{Original image}
\label{subfig:comp_tgv_butterfly_orig}
\end{subfigure}\qquad
\begin{subfigure}[t]{0.29\textwidth}
\includegraphics[trim={0cm 0cm 0cm 0cm},width=\textwidth]{graphics/comp_tgv_butterfly/noisy.eps}
\caption{Noisy image}
\label{subfig:comp_tgv_butterfly_noisy}
\end{subfigure}\qquad
\begin{subfigure}[t]{0.29\textwidth}
\includegraphics[trim={0cm 0cm 0cm 0cm},width=\textwidth]{graphics/comp_tgv_butterfly/tvpwl.eps}
\caption{Reconstruction with $TV_{pwL}^{\gamma}$, $\gamma=24.6667$}
\label{subfig:comp_tgv_butterfly_tvpwl}
\end{subfigure}\\
\begin{subfigure}[t]{0.29\textwidth}
\includegraphics[trim={0cm 0cm 0cm 0cm},width=\textwidth]{graphics/comp_tgv_butterfly/tv1.eps}
\caption{Reconstruction with $TGV^2_{\alpha,\beta}$, $\beta=5\cdot 10^{-8}$}
\label{subfig:comp_tgv_butterfly_tv1}
\end{subfigure}\qquad
\begin{subfigure}[t]{0.29\textwidth}
\includegraphics[trim={0cm 0cm 0cm 0cm},width=\textwidth]{graphics/comp_tgv_butterfly/tv2.eps}
\caption{Reconstruction with $TGV^2_{\alpha,\beta}$, $\beta=0.3444$}
\label{subfig:comp_tgv_butterfly_tv2}
\end{subfigure}\qquad
\begin{subfigure}[t]{0.29\textwidth}
\includegraphics[trim={0cm 0cm 0cm 0cm},width=\textwidth]{graphics/comp_tgv_butterfly/tv3.eps}
\caption{Reconstruction with $TGV^2_{\alpha,\beta}$, $\beta=100$}
\label{subfig:comp_tgv_butterfly_tv3}
\end{subfigure}
\caption[Reconstruction butterfly image with $TV_{pwL}$ and $TV$]{Noisy butterfly image with $\delta=0.1$ and reconstructions for $TV_{pwL}$ and $TGV^{2}_{\alpha,\beta}$ with different $\beta$ values.}
\label{fig:comp_tgv_butterfly}
\end{figure}
Figure \ref{fig:comp_tgv_butterfly} shows the original butterfly image, the image corrupted by Gaussian noise with $\delta=0.1$ and the reconstructions with $TV_{pwL}^{\gamma}$ and with $TGV^2_{\alpha,\beta}$ for different $\beta$ values.
The SSIM values between the different reconstructions and the original image and between the $TGV^2_{\alpha,\beta}$ reconstructions and the $TV_{pwL}^{\gamma}$ reconstruction are given in Table \ref{tab:comp_tgv_butterfly}.
For $\beta=5\cdot 10^{-8}$ the $TGV^2_{\alpha,\beta}$ reconstruction bears the highest resemblance with the $TV_{pwL}^{\gamma}$ reconstruction which also matches the SSIM values between the reconstructions. Both images still contain some noise. Only the $TGV^2_{\alpha,\beta}$ reconstruction is slightly higher in contrast.
For $\beta=0.3444$ the $TGV^2_{\alpha,\beta}$ reconstruction is clearly higher in contrast. Furthermore, the image contains less noise  but on the other hand also misses some of the fine details on the butterfly wings.
The $TGV^2_{\alpha,\beta}$ reconstruction with $\beta=100$ is blurrier than the $TV_{pwL}^{\gamma}$ reconstruction. The image is higher in contrast and shows fewer details. In the background we even notice some blocky structures.

\begin{table}
\centering
\begin{tabular}{|p{4cm}|p{3cm}|p{3cm}|}
\hline
 & $SSIM$& $SSIM_{TV_{pwL}}$ \\
 \hline
 $TV_{pwL}$&$ 0.6082$ & \\
 \hline
 $TVL^{2}_{\alpha,\beta}$, $\beta=5\cdot 10^{-8}$&$ 0.6607$ &$0.9703 $ \\
 \hline
 $TVL^{2}_{\alpha,\beta}$, $\beta=0.3444$&$ 0.7093$ &$ 0.8040$ \\
 \hline
 $TVL^{2}_{\alpha,\beta}$, $\beta=100$&$0.5971 $ &$0.7195 $ \\
 \hline
\end{tabular}
 \caption[SSIM for $TV_{pwL}^{\gamma}$ and $TGV^2_{\alpha,\beta}$: Butterfly]{SSIM values for reconstruction with $TV_{pwL}^{\gamma}$ and $TGV^{2}_{\alpha,\beta}$ of butterfly image with ${\delta=0.1}$.}
 \label{tab:comp_tgv_butterfly}
\end{table}

\begin{figure}
\centering
\begin{subfigure}[t]{0.29\textwidth}
\includegraphics[trim={0cm 0cm 0cm 0cm},width=\textwidth]{graphics/comp_tgv_owl/orig.eps}
\caption{Original image}
\label{subfig:comp_tgv_owl_orig}
\end{subfigure}\qquad
\begin{subfigure}[t]{0.29\textwidth}
\includegraphics[trim={0cm 0cm 0cm 0cm},width=\textwidth]{graphics/comp_tgv_owl/noisy.eps}
\caption{Noisy image}
\label{subfig:comp_tgv_owl_noisy}
\end{subfigure}\qquad
\begin{subfigure}[t]{0.29\textwidth}
\includegraphics[trim={0cm 0cm 0cm 0cm},width=\textwidth]{graphics/comp_tgv_owl/tvpwl.eps}
\caption{Reconstruction with $TV_{pwL}^{\gamma}$, $\gamma=34.4444$}
\label{subfig:comp_tgv_owl_tvpwl}
\end{subfigure}\\
\begin{subfigure}[t]{0.29\textwidth}
\includegraphics[trim={0cm 0cm 0cm 0cm},width=\textwidth]{graphics/comp_tgv_owl/tv1.eps}
\caption{Reconstruction with $TGV^2_{\alpha,\beta}$, $\beta= 5\cdot 10^{-8}$}
\label{subfig:comp_tgv_owl_tv1}
\end{subfigure}\qquad
\begin{subfigure}[t]{0.29\textwidth}
\includegraphics[trim={0cm 0cm 0cm 0cm},width=\textwidth]{graphics/comp_tgv_owl/tv2.eps}
\caption{Reconstruction with $TGV^2_{\alpha,\beta}$, $\beta=1$}
\label{subfig:comp_tgv_owl_tv2}
\end{subfigure}\qquad
\begin{subfigure}[t]{0.29\textwidth}
\includegraphics[trim={0cm 0cm 0cm 0cm},width=\textwidth]{graphics/comp_tgv_owl/tv3.eps}
\caption{Reconstruction with $TGV^2_{\alpha,\beta}$, $\beta=100$}
\label{subfig:comp_tgv_owl_tv3}
\end{subfigure}
\caption[Reconstruction owl image with $TV_{pwL}$ and $TGV^{2}_{\alpha,\beta}$]{Noisy owl image with $\delta=0.15$ and reconstructions for $TV_{pwL}$ and $TGV^{2}_{\alpha,\beta}$ with different $\beta$ values.}
\label{fig:comp_tgv_owl}
\end{figure}
Finally, we turn to the image of the owl and compare the $TV_{pwL}^{\gamma}$ result to the $TGV^2_{\alpha,\beta}$ reconstructions for different $\beta$ values. The results are shown in Figure \ref{fig:comp_tgv_owl}.
For $\beta=5\cdot 10^{-8}$ we notice the highest similarity between the reconstructions. The $TGV^2_{\alpha,\beta}$ reconstruction does not contain as many fine details as the $TV_{pwL}^{\gamma}$ reconstruction but on the other hand the contrast is higher. 
The $TGV^2_{\alpha,\beta}$ reconstruction for $\beta=1$ contains different structures than the $TV_{pwL}^{\gamma}$ reconstruction. The image is smoother and higher in contrast.
For $\beta=100$ the $TGV^2_{\alpha,\beta}$ reconstructions misses most of the fine details, for example visible for the feathers of the owl. The image contains larger areas of the same shade and we even notice blocky structures in the background and for the feather.
These observations match the SSIM values in Table \ref{tab:comp_tgv_owl} where the SSIM value between $TGV^2_{\alpha,\beta}$ reconstruction and $TV_{pwL}^{\gamma}$ reconstruction gets smaller the higher the parameter $\beta$ is.

\begin{table}
\centering
\begin{tabular}{|p{4cm}|p{3cm}|p{3cm}|}
\hline
 & $SSIM$& $SSIM_{TV_{pwL}}$ \\
 \hline
 $TV_{pwL}$&$ 0.5151$ & \\
 \hline
 $TVL^{2}_{\alpha,\beta}$, $\beta=5\cdot 10^{-8}$&$ 0.5202$ &$0.8445 $ \\
 \hline
 $TVL^{2}_{\alpha,\beta}$, $\beta=1$&$ 0.4661$ &$ 0.6362$ \\
 \hline
 $TVL^{2}_{\alpha,\beta}$, $\beta=100$&$0.4090 $ &$0.5924 $ \\
 \hline
\end{tabular}
 \caption[SSIM for $TV_{pwL}^{\gamma}$ and $TGV^2_{\alpha,\beta}$: Owl]{SSIM values for reconstruction with $TV_{pwL}^{\gamma}$ and $TGV^{2}_{\alpha,\beta}$ of owl image with ${\delta=0.15}$.}
 \label{tab:comp_tgv_owl}
\end{table}
\end{document}