%calculates optimal parameter lambda with respect to ssim for regularizer
%TV_pwL for given parameter gamma
%gives the solution u for the calculated parameters and the corresponding
%ssim
% plots noisy date and calculated solution

function [u,params_opt,SSIM] = ssim_optimisation_tvpwl ( name, delta, params)

N = params.N;
regularizer = 'TV_pwL';
params.reg = 'TV_pwL';
gamma = params.gamma;

%determining data from given function, stepsize and noise level
[s,s_true,a,b,xx,d]= testfun(name,N,delta);
params.stepsize = d;

%number of steps
d1 = 11;



%initialising ssim and intervals for second parameter
%initial interval for lambda
a1 = 0;
b1 = 25000;
ssim_old = zeros(d1,1);
ssim_new = zeros(d1,1);


j = 1;
iteration = 0;

while j > 10^(-4)
    iteration = iteration +1;
    disp(['Iteration ',num2str(iteration)])
    %calculating ssims for initial interval
    for i = 1:d1
        lambdav = linspace(a1,b1,d1);
        %do_minimization2 and calculate ssims
        lambda = lambdav(i);
        u_TV_pwL = do_minimization2(s, params, lambda);
        ssim_new(i) = ssim(s_true,u_TV_pwL');
    end
    
    %calculate difference between old maximal ssim and new maximal ssim
    [argvalue_old, argmax_old] = max(ssim_old(:));
    [argvalue_new, argmax_new] = max(ssim_new(:));
    j = abs(argvalue_old-argvalue_new);
    
    %choose new interval for optimization

    c = b1-a1;
    a1 = lambdav(argmax_new)-(c)/d1;
    if a1 < 0
       a1 = 0;
    end
    b1 = lambdav(argmax_new)+(c)/d1;
      
    ssim_old = ssim_new;
end

[argvalue, argmax] = max(ssim_old(:));

%set optimal parameter values

params_opt.lambda = lambdav(argmax);
%do_minimization2 and plot noisy data and calculated optimal solution
lambda = params_opt.lambda;
        
u = do_minimization2(s, params, lambda);
plot(xx,s,'b',xx,u,'r','LineWidth',2);
axis([a b min(s) max(s)])
title(num2str(regularizer))
SSIM = ssim(s_true,u');