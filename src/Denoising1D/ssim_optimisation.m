%calculates optimal parameters with respect to ssim for regularizers TV,
%TV_pwL, TGV2, TVLp for a function with noise data, 
%gives the solution u for the calculated parameters and the corresponding
%ssim
% plots noisy date and calculated solution

function [u,params_opt,SSIM] = ssim_optimisation ( name, delta, params)

N = params.N;
regularizer = params.reg;

%determining data from given function, stepsize and noise level
[s,s_true,a,b,xx,d]= testfun(name,N,delta);
params.stepsize = d;

%number of steps
d1 = 11;



%initialising ssim and intervals for second parameter
switch regularizer
    case 'TV'
        %initial interval for lambda
        a1 = 0;
        b1 = 100;
        ssim_old = zeros(d1,1);
        ssim_new = zeros(d1,1);
    case 'TV_pwL'
        %initial interval for lambda
        a1 = 0;
        b1 = 25000;
        %initial interval for gamma
        m1 = 0;
        n1 = max(abs(diff(s)));

        ssim_old = zeros(d1,d1);
        ssim_new = zeros(d1,d1);
    case 'TGV2'
        %initial interval for lambda
        a1 = 0;
        b1 = 10;
        %initial interval for beta
        m1 = 0;
        n1 = 10;

        ssim_old = zeros(d1,d1);
        ssim_new = zeros(d1,d1);
    case 'TVLp'
        %initial interval for lambda
        a1 = 0;
        b1 = 1500;
        %initial interval for beta
        m1 = 0;
        n1 = 1500;

        ssim_old = zeros(d1,d1);
        ssim_new = zeros(d1,d1);
        
end

j = 1;
iteration = 0;

while j > 10^(-4)
    iteration = iteration +1;
    disp(['Iteration ',num2str(iteration)])
    %calculating ssims for initial interval
    switch regularizer
        case 'TV'
            for i = 1:d1
                lambdav = linspace(a1,b1,d1);
    
                %do_minimization2 and calculate ssims
                lambda = lambdav(i);
                u_TV = do_minimization(s, params, lambda);
                ssim_new(i) = ssim(s_true,u_TV');
            end
        case 'TV_pwL'
            for h = 1:d1
                for i = 1:d1
                    lambdav = linspace(a1,b1,d1);
                    param_2 = linspace(m1,n1,d1);
    
                    %do_minimization2 and calculate ssims
                    lambda = lambdav(i);
                    params.gamma = param_2(h);
                    u_TV_pwL = do_minimization(s, params, lambda);
                    ssim_new(i,h) = ssim(s_true,u_TV_pwL');
                end
            end
        case 'TGV2'
            for h = 1:d1
                for i = 1:d1
                    lambdav = linspace(a1,b1,d1);
                    param_2 = linspace(m1,n1,d1);
    
                    %do_minimization2 and calculate ssims
                    lambda = lambdav(i);
                    params.beta = param_2(h);
                    u_TGV = do_minimization(s, params, lambda);
                    ssim_new(i,h) = ssim(s_true,u_TGV');
                end
            end
        case 'TVLp'
            for h = 1:d1
                for i = 1:d1
                    lambdav = linspace(a1,b1,d1);
                    param_2 = linspace(m1,n1,d1);
    
                    %do_minimization2 and calculate ssims
                    lambda = lambdav(i);
                    params.beta = param_2(h);
                    u_TVLp = do_minimization(s, params, lambda);
                    ssim_new(i,h) = ssim(s_true,u_TVLp');
                end
            end
    end
    
    %calculate difference between old maximal ssim and new maximal ssim
    [argvalue_old, argmax_old] = max(ssim_old(:));
    [argvalue_new, argmax_new] = max(ssim_new(:));
    j = abs(argvalue_old-argvalue_new);
    
    %choose new interval for optimization
    switch regularizer
        case 'TV'
            c = b1-a1;
            a1 = lambdav(argmax_new)-(c)/d1;
            if a1 < 0
                a1 = 0;
            end
            b1 = lambdav(argmax_new)+(c)/d1;
            
        case {'TV_pwL', 'TGV2', 'TVLp'}
            c = b1 - a1;
    
            if mod(argmax_new,d1) == 0
                a1 = lambdav(d1) - c/d1;
                b1 = lambdav(d1) + c/d1;
            else
                a1 = lambdav(mod(argmax_new,d1)) - c/d1;
                b1 = lambdav(mod(argmax_new,d1)) + c/d1;
            end
            if a1 < 0
                a1 = 0;
            end
    
            c1 = n1 - m1;
            m1 = param_2(ceil(argmax_new/d1))-c1/d1;
            if m1 <0
                m1 = 0;
            end
            n1 = param_2(ceil(argmax_new/d1))+c1/d1;
    end
    
    ssim_old = ssim_new;
end

[argvalue, argmax] = max(ssim_old(:));

%set optimal parameter values
switch regularizer
    case 'TV'
        params_opt.lambda = lambdav(argmax);
    case 'TV_pwL'
        if mod(argmax,d1) == 0
            params_opt.lambda = lambdav(d1);
        else
            params_opt.lambda = lambdav(mod(argmax,d1));
        end
        params_opt.gamma = param_2(ceil(argmax/d1));
    case {'TGV2', 'TVLp'}
        if mod(argmax,d1) == 0
            params_opt.lambda = lambdav(d1);
        else
            params_opt.lambda = lambdav(mod(argmax,d1));
        end
        params_opt.beta = param_2(ceil(argmax/d1));
end

%do_minimization2 and plot noisy data and calculated optimal solution
lambda = params_opt.lambda;
switch regularizer
    case 'TV_pwL'
        params.gamma = params_opt.gamma;
    case {'TGV2', 'TVLp'}
        params.beta = params_opt.beta;
    case 'TV'
end
u = do_minimization(s, params, lambda);
plot(xx,s,'b',xx,u,'r','LineWidth',2);
axis([a b min(s) max(s)])
title(num2str(regularizer))
SSIM = ssim(s_true,u');