clear all
clc
rng(23454)

%fun=@(x) cos(x).*(x<=0) -cos(x).*(x>0);
N = 1000;
delta = 0.1;
name = 'cos';
[s,s_true,a,b,xx,d]= testfun(name,N,delta);
params.stepsize = d;
params.N = N;
type = 'Gaussian_Bound';
params.fidelity = 'Bound';
if delta == 0
    params.bound = sqrt(N) * max(abs(s_true)) * 0.01;
else 
    params.bound = sqrt(N) * max(abs(s_true)) * delta;
end
lambda = 1;
%%

%do_minimization2 for TV
params.reg = 'TV';
u_TV = do_minimization(s, params, lambda);
subplot(3,2,1)
plot(xx,s,'b',xx,u_TV,'r','LineWidth',1.2);
axis([a b min(s) max(s)])
title('TV')
ssim_TV = ssim(s_true,u_TV')
%%

%do_minimization2 for TV_pwL
params.reg = 'TV_pwL';
switch name
    case 'simplehat'
        if delta == 0.05
            params.gamma = 1;
        elseif delta == 0.1
            params.gamma = 57.0248;
        end
    case 'hat'
        if delta == 0.05
            params.gamma = 1;
        elseif delta == 0.1
            params.gamma = 9.4857;
        end
    case 'constant'
        if delta == 0.05
            params.gamma = 1;
        elseif delta == 0.1
            params.gamma = 148.9994;
        end
    case 'plateau'
        if delta == 0.05
            params.gamma = 1;
        elseif delta == 0.1
            params.gamma = 0.2830;
        end
    case 'affine'
        if delta == 0.05
            params.gamma = 1;
        elseif delta == 0.1
            params.gamma = 20.6612;
        end
    case 'ramp'
        if delta == 0.05
            params.gamma = 1;
        elseif delta == 0.1
            params.gamma = 40.6461;
        end
    case 'quadratic'
        if delta == 0.05
            params.gamma = 1;
        elseif delta == 0.1
            params.gamma = 10.2479;
        end
    case 'cos01'
        if delta == 0.05
            params.gamma = 1;
        elseif delta == 0.1
            params.gamma = 149.6551;
        end
    case 'cos'
        if delta == 0.05
            params.gamma = 0.7347;
        elseif delta == 0.1
            params.gamma = 28.3246;
        end
end
u_TV_pwL = do_minimization(s, params, lambda);
subplot(3,2,2)
plot(xx,s,'b',xx,u_TV_pwL,'r','LineWidth',1.2);
axis([a b min(s) max(s)])
title('TV_{pwL}')
ssim_TV_pwL = ssim(s_true,u_TV_pwL')
%%

%do_minimization2 for TGV2
params.reg = 'TGV2';
switch name
    case 'simplehat'
        if delta == 0.05
            params.beta = 1;
        elseif delta == 0.1
            params.beta = 0.2545;
        end
    case 'hat'
        if delta == 0.05
            params.beta = 1;
        elseif delta == 0.1
            params.beta = 0.0015;
        end
    case 'constant'
        if delta == 0.05
            params.beta = 1;
        elseif delta == 0.1
            params.beta = 0.7636;
        end
    case 'plateau'
        if delta == 0.05
            params.beta = 1;
        elseif delta == 0.1
            params.beta = 0.0159;
        end
    case 'affine'
        if delta == 0.05
            params.beta = 1;
        elseif delta == 0.1
            params.beta = 0.5000;
        end
    case 'ramp'
        if delta == 0.05
            params.beta = 1;
        elseif delta == 0.1
            params.beta = 0.1182;
        end
    case 'quadratic'
        if delta == 0.05
            params.beta = 1;
        elseif delta == 0.1
            params.beta = 0.0273;
        end
    case 'cos01'
        if delta == 0.05
            params.beta = 1;
        elseif delta == 0.1
            params.beta = 0.0051;
        end
    case 'cos'
        if delta == 0.05
            params.beta = 0.1555;
        elseif delta == 0.1
            params.beta = 0.2421;
        end
end

u_TGV = do_minimization(s, params, lambda);
subplot(3,2,3)
plot(xx,s,'b',xx,u_TGV,'r','LineWidth',1.2);
axis([a b min(s) max(s)])
title('TGV')
ssim_TGV = ssim(s_true,u_TGV')
%%

%do_minimization2 for TVLp
params.reg = 'TVLp';
params.p = 2;
switch name
    case 'simplehat'
        if delta == 0.05
            params.beta = 1;
        elseif delta == 0.1
            params.beta = 0.3182;
        end
    case 'hat'
        if delta == 0.05
            params.beta = 1;
        elseif delta == 0.1
            params.beta = 0.2182;
        end
    case 'constant'
        if delta == 0.05
            params.beta = 1;
        elseif delta == 0.1
            params.beta = 0.6545;
        end
    case 'plateau'
        if delta == 0.05
            params.beta = 1;
        elseif delta == 0.1
            params.beta = 0.2364;
        end
    case 'affine'
        if delta == 0.05
            params.beta = 1;
        elseif delta == 0.1
            params.beta = 0.3818;
        end
    case 'ramp'
        if delta == 0.05
            params.beta = 1;
        elseif delta == 0.1
            params.beta = 0.7182;
        end
    case 'quadratic'
        if delta == 0.05
            params.beta = 1;
        elseif delta == 0.1
            params.beta = 0.6000;
        end
    case 'cos01'
        if delta == 0.05
            params.beta = 1;
        elseif delta == 0.1
            params.beta = 0.3000;
        end
    case 'cos'
        if delta == 0.05
            params.beta = 56.4920;
        elseif delta == 0.1
            params.beta =0.7182;
        end
end

u_TVLp = do_minimization(s, params, lambda);
subplot(3,2,4)
plot(xx,s,'b',xx,u_TVLp,'r','LineWidth',1.2);
axis([a b min(s) max(s)])
title('TVL_2')
ssim_TVL3 = ssim(s_true,u_TVLp')

%%

%do_minimization2 for TVLinfty
params.reg = 'TVLp';
params.p = inf;
switch name
    case 'simplehat'
        if delta == 0.05
            params.beta = 1;
        elseif delta == 0.1
            params.beta = 0.1774;
        end
    case 'hat'
        if delta == 0.05
            params.beta = 1;
        elseif delta == 0.1
            params.beta = 0.4245;
        end
    case 'constant'
        if delta == 0.05
            params.beta = 1;
        elseif delta == 0.1
            params.beta =  0.0902;
        end
    case 'plateau'
        if delta == 0.05
            params.beta = 1;
        elseif delta == 0.1
            params.beta = 0.2344;
        end
    case 'affine'
        if delta == 0.05
            params.beta = 1;
        elseif delta == 0.1
            params.beta = 0.3814;
        end
    case 'ramp'
        if delta == 0.05
            params.beta = 1;
        elseif delta == 0.1
            params.beta = 0.6161;
        end
    case 'quadratic'
        if delta == 0.05
            params.beta = 1;
        elseif delta == 0.1
            params.beta = 0.4774;
        end
    case 'cos01'
        if delta == 0.05
            params.beta = 1;
        elseif delta == 0.1
            params.beta = 0.4952;
        end
    case 'cos'
        if delta == 0.05
            params.beta = 233.0579;
        elseif delta == 0.1
            params.beta = 276.5523;%0.1445;
        end
end

u_TVL_infty = do_minimization(s, params, lambda);
subplot(3,2,5)
plot(xx,s,'b',xx,u_TVL_infty,'r','LineWidth',1.2);
axis([a b min(s) max(s)])
title('TVL_{\infty}')
ssim_TVL_infty = ssim(s_true,u_TVL_infty')

