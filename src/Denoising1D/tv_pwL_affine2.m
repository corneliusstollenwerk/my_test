clear all
clc

seed = 5849374;
rng(seed)

params.reg = 'TV_pwL';
params.fidelity = 'L2 Squared';

grad = 0.7;
jump = 1-grad;

alpha0 = 0;
alphaend = 1;

data = affine ( grad , jump );
s = size(data);
params.stepsize = 1/(s(2)-1);
xx = 0:(1/(s(2)-1)):1;


%%
delta = 0;
noisy = data + max(abs(data)) * delta * randn(s);

params.gamma = grad;
ssim_neg = @(lambda) - ssim( do_minimization2 (noisy , params , lambda),data');
[lambda1, SSIM_neg] = fminbnd( ssim_neg , alpha0 , alphaend );
u_TV_pwL = do_minimization2(noisy, params, lambda1);
subplot(2,3,1)
plot(xx',noisy,'b',xx',data,'c',xx',u_TV_pwL,'r','LineWidth',1);
axis([0 1 min(noisy) max(noisy)])
title('TV_{pwL} for delta = = 0')
ssim1 = ssim(data,u_TV_pwL')

%%
delta = 0.025;
noisy = data + max(abs(data)) * delta * randn(s);

params.gamma = grad;
ssim_neg = @(lambda) - ssim( do_minimization2 (noisy , params , lambda),data');
[lambda2, SSIM_neg] = fminbnd( ssim_neg , alpha0 , alphaend );
u_TV_pwL = do_minimization2(noisy, params, lambda2);
subplot(2,3,2)
plot(xx',noisy,'b',xx',data,'c',xx',u_TV_pwL,'r','LineWidth',1);
axis([0 1 min(noisy) max(noisy)])
title('TV_{pwL} for delta = = 0.025')
ssim2 = ssim(data,u_TV_pwL')

%%
delta = 0.05;
noisy = data + max(abs(data)) * delta * randn(s);

params.gamma = grad;
ssim_neg = @(lambda) - ssim( do_minimization2 (noisy , params , lambda),data');
[lambda3, SSIM_neg] = fminbnd( ssim_neg , alpha0 , alphaend );
u_TV_pwL = do_minimization2(noisy, params, lambda3);
subplot(2,3,3)
plot(xx',noisy,'b',xx',data,'c',xx',u_TV_pwL,'r','LineWidth',1);
axis([0 1 min(noisy) max(noisy)])
title('TV_{pwL} for delta = = 0.05')
ssim3 = ssim(data,u_TV_pwL')

%%
delta = 0.075;
noisy = data + max(abs(data)) * delta * randn(s);

params.gamma = grad;
ssim_neg = @(lambda) - ssim( do_minimization2 (noisy , params , lambda),data');
[lambda4, SSIM_neg] = fminbnd( ssim_neg , alpha0 , alphaend );
u_TV_pwL = do_minimization2(noisy, params, lambda4);
subplot(2,3,4)
plot(xx',noisy,'b',xx',data,'c',xx',u_TV_pwL,'r','LineWidth',1);
axis([0 1 min(noisy) max(noisy)])
title('TV_{pwL} for delta = = 0.075')
ssim4 = ssim(data,u_TV_pwL')

%%
delta = 0.1;
noisy = data + max(abs(data)) * delta * randn(s);

params.gamma = grad;
ssim_neg = @(lambda) - ssim( do_minimization2 (noisy , params , lambda),data');
[lambda5, SSIM_neg] = fminbnd( ssim_neg , alpha0 , alphaend );
u_TV_pwL = do_minimization2(noisy, params, lambda5);
subplot(2,3,5)
plot(xx',noisy,'b',xx',data,'c',xx',u_TV_pwL,'r','LineWidth',1);
axis([0 1 min(noisy) max(noisy)])
title('TV_{pwL} for delta = = 0.1')
ssim5 = ssim(data,u_TV_pwL')

%%
delta = 0.2;
noisy = data + max(abs(data)) * delta * randn(s);

params.gamma = grad;
ssim_neg = @(lambda) - ssim( do_minimization2 (noisy , params , lambda),data');
[lambda6, SSIM_neg] = fminbnd( ssim_neg , alpha0 , alphaend );
u_TV_pwL = do_minimization2(noisy, params, lambda6);
subplot(2,3,6)
plot(xx',noisy,'b',xx',data,'c',xx',u_TV_pwL,'r','LineWidth',1);
axis([0 1 min(noisy) max(noisy)])
title('TV_{pwL} for delta = = 0.2')
ssim6 = ssim(data,u_TV_pwL')