clear all
clc
close all
rng(23454)

%fun=@(x) cos(x).*(x<=0) -cos(x).*(x>0);
N = 1000;
delta = 0;
name = 'all';
[s,s_true,a,b,xx,d]= testfun(name,N,delta);
params.stepsize = d;
params.N = N;
type = 'Gaussian';
params.fidelity = 'L2 Squared';

% subplot(2,2,1)
% plot(xx,s,'b','LineWidth',1.5);
lambda = 5;

%%
%do_minimization2 for TV_pwL

params.reg = 'TV_pwL';
params.gamma = 8.6292;%3.9439;%2;

u_TVpwl = do_minimization(s, params, lambda);
f1=figure('pos',[0 0 900 600]);
subplot(1,2,1)
plot(xx,s,'b',xx,u_TVpwl,'r','LineWidth',1.5);
L=legend('f','TV_{pwL}^{\gamma}')
set (L,'FontSize',7)
set (L,'Location','northeast')
pbaspect([2 1.4 1 ])
axis([a b min(s)-0.2 max(s)+0.2])
%title('TV_{pwL}')
ssim1 = ssim(s_true,u_TVpwl')

%%
%lambda= 5;

params.reg = 'TVLp';
params.p = inf;
params.beta = 5;

u_TVLp = do_minimization(s, params, lambda);
subplot(1,2,2)
plot(xx,s,'b',xx,u_TVLp,'r','LineWidth',1.5);
L=legend('f','TVL^{\infty}')
set (L,'FontSize',7)
set (L,'Location','northeast')
pbaspect([2 1.4 1 ])
axis([a b min(s)-0.2 max(s)+0.2])
%title('TV_{pwL}')
ssim2 = ssim(s_true,u_TVLp')



%%
%print(f1,'images_nonoise/comp_all2','-dpdf')