function [u,params_opt,SSIM] = ssim_optimisation_bound ( name, delta, params)

N = params.N;
regularizer = params.reg;

%determining data from given function, stepsize and noise level
[s,s_true,a,b,xx,d]= testfun(name,N,delta);
params.stepsize = d;
lambda = 1;

%number of steps
d1 = 11;



%initialising ssim and intervals for second parameter
switch regularizer
    case 'TV'
        %initial interval for lambda
        params_opt = 1;
    case 'TV_pwL'
        %initial interval for gamma
        a1 = 0;
        %b1 = 2*max(abs(diff(s)));
        b1 = 1000;

        ssim_old = zeros(d1,1);
        ssim_new = zeros(d1,1);
    case 'TGV2'
        %initial interval for beta
        a1 = 0;
        b1 = 1;

        ssim_old = zeros(d1,1);
        ssim_new = zeros(d1,1);
    case 'TVLp'
        %initial interval for beta
        a1 = 0;
        b1 = 1;

        ssim_old = zeros(d1,1);
        ssim_new = zeros(d1,1);
        
end

j = 1;
iteration = 0;

while j > 10^(-4)
    iteration = iteration +1;
    disp(['Iteration ',num2str(iteration)])
    %calculating ssims for initial interval
    switch regularizer
        case 'TV'

        case 'TV_pwL'
            for i = 1:d1
                param_2 = linspace(a1,b1,d1);
    
                %do_minimization2 and calculate ssims
                params.gamma = param_2(i);
                u_TV_pwL = do_minimization(s, params, lambda);
                ssim_new(i) = ssim(s_true,u_TV_pwL');
            end
        case 'TGV2'
            for i = 1:d1
                param_2 = linspace(a1,b1,d1);
    
                %do_minimization2 and calculate ssims
                params.beta = param_2(i);
                u_TGV = do_minimization(s, params, lambda);
                ssim_new(i) = ssim(s_true,u_TGV');
            end
        case 'TVLp'
            for i = 1:d1
                param_2 = linspace(a1,b1,d1);
    
                %do_minimization2 and calculate ssims
                params.beta = param_2(i);
                u_TVLp = do_minimization(s, params, lambda);
                ssim_new(i) = ssim(s_true,u_TVLp');
            end
    end
    
    %calculate difference between old maximal ssim and new maximal ssim
    [argvalue_old, argmax_old] = max(ssim_old(:));
    [argvalue_new, argmax_new] = max(ssim_new(:));
    j = abs(argvalue_old-argvalue_new);
    
    %choose new interval for optimization
    switch regularizer
        case 'TV'

        case {'TV_pwL', 'TGV2', 'TVLp'}
            c = b1-a1;
            a1 = param_2(argmax_new)-(c)/d1;
            if a1 < 0
                a1 = 0;
            end
            b1 = param_2(argmax_new)+(c)/d1;
    end
    
    ssim_old = ssim_new;
end

[argvalue, argmax] = max(ssim_old(:));

%set optimal parameter values
switch regularizer
    case 'TV'
        params_opt.lambda = params.opt
    case 'TV_pwL'
        params_opt.gamma = param_2(argmax);
    case {'TGV2', 'TVLp'}
        params_opt.beta = param_2(argmax);
end

%do_minimization2 and plot noisy data and calculated optimal solution
switch regularizer
    case 'TV_pwL'
        params.gamma = params_opt.gamma;
    case {'TGV2', 'TVLp'}
        params.beta = params_opt.beta;
    case 'TV'
end
u = do_minimization(s, params, lambda);
plot(xx,s,'b',xx,u,'r','LineWidth',2);
axis([a b min(s) max(s)])
title(num2str(regularizer))
SSIM = ssim(s_true,u');