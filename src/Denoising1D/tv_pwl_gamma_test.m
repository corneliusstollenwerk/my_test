clear all
clc
rng(456123789)


name = 'cos';
delta = 0.1;
params.N = 1000;
params.gamma = 1;
params.fidelity = 'L2 Squared';

%calculating optimal lambda for initial gamma value 1
[u1,params_opt1,SSIM1] = ssim_optimisation_tvpwl ( name, delta, params);
lambda_opt1 = params_opt1.lambda;

%calculating optimal gamma for obtained lambda
params.lambda = lambda_opt1;
[u2,gamma_opt,SSIM2] = ssim_optimisation_tvpwl ( name, delta, params);

%calculating optimal lambda for adjusted gamma
params.gamma = gamma_opt;
[u3,params_opt3,SSIM3] = ssim_optimisation_tvpwl ( name, delta, params);
lambda_opt3 = params_opt3.lambda;

%calculating and plotting solution
% params.lambda = lambda_opt3;
% subplot(1,2,1)
% [u_tvpwl,gamma_opt,SSIM2] = ssim_optimisation_tvpwl_gamma ( name, delta, params);
% title('TV_{pwL}')
% 
% subplot(1,2,2) = plot(xx,gamma_opt)
% title('Gamma')

N = params.N;
params.reg = 'TV_pwL';
[s,s_true,a,b,xx,d]= testfun(name,N,delta);
params.stepsize = d;

lambda = lambda_opt3;
u_TV_pwl = do_minimization2(s, params, lambda);
subplot(1,2,1)
plot(xx,s,'b',xx,u_TV_pwl,'r','LineWidth',2);
axis([a b min(s) max(s)])
title(['TV_{pwL} with lambda =', num2str(lambda)])
subplot(1,2,2)
plot(xx,gamma_opt)
axis([a b min(gamma_opt)-0.2 0])
ssim_TV_pwl = ssim(s_true,u_TV_pwl')
title('adjusted gamma')