clear all
clc
close all
rng(23454)

%fun=@(x) cos(x).*(x<=0) -cos(x).*(x>0);
N = 1000;
delta = 0;
name = 'cos';
[s,s_true,a,b,xx,d]= testfun(name,N,delta);
params.stepsize = d;
params.N = N;
type = 'Gaussian';
params.fidelity = 'L2 Squared';

% subplot(2,2,1)
% plot(xx,s,'b','LineWidth',1.5);
lambda = 0.1;

%%
%do_minimization2 for TV_pwL

params.reg = 'TV_pwL';
params.gamma = 1;

u_TVpwl = do_minimization(s, params, lambda);
f1=figure('pos',[0 0 900 900]);
subplot(4,1,1)
plot(xx,s,'b',xx,u_TVpwl,'r','LineWidth',1.5);
L=legend('f','TV_{pwL}^1')
set (L,'FontSize',7)
set (L,'Location','northwest')
pbaspect([2*pi 1.2 1 ])
axis([a b min(s)-0.2 max(s)+0.2])
%title('TV_{pwL}')
ssim_0 = ssim(s_true,u_TVpwl')

%%
lambda= 5;

params.reg = 'TVLp';
params.p = inf;
params.beta = 5;

u_TVLp = do_minimization(s, params, lambda);
subplot(4,1,2)
plot(xx,s,'b',xx,u_TVLp,'r','LineWidth',1.5);
L=legend('f','TVL^{\infty}')
set (L,'FontSize',7)
set (L,'Location','northwest')
pbaspect([2*pi 1.2 1 ])
axis([a b min(s)-0.2 max(s)+0.2])
%title('TV_{pwL}')
ssim_02 = ssim(s_true,u_TVLp')


%%
lambda = 1;

params.reg = 'TVLp';
params.p = 2;
params.beta = 1;

%f2=figure('pos',[0 0 900 300]);
u_TVLp = do_minimization(s, params, lambda);
subplot(4,1,3)
plot(xx,s,'b',xx,u_TVLp,'r','LineWidth',1.5);
L=legend('f','TVL^{2}')
set (L,'FontSize',7)
set (L,'Location','northwest')
pbaspect([2*pi 1.2 1 ])
axis([a b min(s)-0.2 max(s)+0.2])
%title('TV_{pwL}')
ssim_02 = ssim(s_true,u_TVLp')


%%
lambda = 0.1;

params.reg = 'TGV2';
params.beta = 0.1;

u_TVLp = do_minimization(s, params, lambda);
subplot(4,1,4)
plot(xx,s,'b',xx,u_TVLp,'r','LineWidth',1.5);
L=legend('f','TGV^2')
set (L,'FontSize',7)
set (L,'Location','northwest')
pbaspect([2*pi 1.2 1 ])
axis([a b min(s)-0.2 max(s)+0.2])
%title('TV_{pwL}')
ssim_02 = ssim(s_true,u_TVLp')

%print(f1,'images_nonoise/comp_cos1','-dpdf')