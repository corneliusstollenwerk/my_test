clear all
clc
close all
rng(23454)

%fun=@(x) cos(x).*(x<=0) -cos(x).*(x>0);
N = 1000;
delta = 0.05;
name = 'quadratic';
params.reg = 'TV_pwL';
[s,s_true,a,b,xx,d]= testfun(name,N,delta);
params.stepsize = d;
params.N = N;
type = 'Gaussian';
params.fidelity = 'L2 Squared';

% subplot(2,2,1)
% plot(xx,s,'b','LineWidth',1.5);
lambda = 0.01;

%%
% %do_minimization2 for TV_pwL
% 
% params.gamma = 0;
% 
% u_TV_pwL = do_minimization(s, params, lambda);
% f1=figure('pos',[0 0 900 300]);
% subplot(1,3,1)
% plot(xx,s,'b',xx,u_TV_pwL,'r','LineWidth',1.2);
% L=legend('f','TV_{pwL}^0')
% set (L,'FontSize',7)
% set (L,'Location','northwest')
% pbaspect([2 1.4 1 ])
% axis([a b min(s)-0.2 max(s)+0.2])
% %title('TV_{pwL}')
% ssim_0 = ssim(s_true,u_TV_pwL')

%%
params.gamma = 0.3;
f1=figure('pos',[0 0 900 300]);

u_TV_pwL = do_minimization(s, params, lambda);
subplot(1,2,1)
plot(xx,s,'b',xx,u_TV_pwL,'r','LineWidth',1.2);
L=legend('f','TV_{pwL}^{0.3}')
set (L,'FontSize',7)
set (L,'Location','northwest')
pbaspect([2 1.4 1 ])
axis([a b min(s)-0.2 max(s)+0.2])
%title('TV_{pwL}')
ssim_02 = ssim(s_true,u_TV_pwL')


%%
params.gamma = 0.6;

u_TV_pwL = do_minimization(s, params, lambda);
subplot(1,2,2)
plot(xx,s,'b',xx,u_TV_pwL,'r','LineWidth',1.2);
L=legend('f','TV_{pwL}^{0.6}')
set (L,'FontSize',7)
set (L,'Location','northwest')
pbaspect([2 1.4 1 ])
axis([a b min(s)-0.2 max(s)+0.2])
%title('TV_{pwL}')
ssim_1 = ssim(s_true,u_TV_pwL')
%print(f1,'images_noise/tvpwl_noise_quadratic1','-dpdf')