clear all
clc
close all
rng(23454)

%fun=@(x) cos(x).*(x<=0) -cos(x).*(x>0);
N = 1000;
delta = 0.05;
name = 'hat';
params.reg = 'TV_pwL';
[s,s_true,a,b,xx,d]= testfun(name,N,delta);
params.stepsize = d;
params.N = N;
type = 'Gaussian';
params.fidelity = 'L2 Squared';

% subplot(2,2,1)
% plot(xx,s,'b','LineWidth',1.5);
lambda = 0.01;

%%
%do_minimization2 for TV_pwL

params.gamma = 0;

u_TV_pwL = do_minimization(s, params, lambda);
f1=figure('pos',[0 0 900 300]);
subplot(1,4,1)
plot(xx,s,'b',xx,u_TV_pwL,'r','LineWidth',1.2);
L=legend('f','TV_{pwL}^0')
set (L,'FontSize',7)
set (L,'Location','northwest')
pbaspect([1 1.6 1 ])
axis([a b min(s)-0.2 max(s)+0.4])
%title('TV_{pwL}')
ssim1 = ssim(s_true,u_TV_pwL')

%%
params.gamma = 5/3;

u_TV_pwL = do_minimization(s, params, lambda);
subplot(1,4,2)
plot(xx,s,'b',xx,u_TV_pwL,'r','LineWidth',1.2);
L=legend('f','TV_{pwL}^{5/3}')
set (L,'FontSize',7)
set (L,'Location','northwest')
pbaspect([1 1.6 1 ])
axis([a b min(s)-0.2 max(s)+0.4])
%title('TV_{pwL}')
ssim2 = ssim(s_true,u_TV_pwL')


%%
params.gamma = 10/3;

u_TV_pwL = do_minimization(s, params, lambda);
%f2=figure('pos',[0 0 900 300]);
subplot(1,4,3)
plot(xx,s,'b',xx,u_TV_pwL,'r','LineWidth',1.2);
L=legend('f','TV_{pwL}^{10/3}')
set (L,'FontSize',7)
set (L,'Location','northwest')
pbaspect([1 1.6 1 ])
axis([a b min(s)-0.2 max(s)+0.4])
%title('TV_{pwL}')
ssim3 = ssim(s_true,u_TV_pwL')
%%
params.gamma = 20/3;

u_TV_pwL = do_minimization(s, params, lambda);
%f2=figure('pos',[0 0 900 300]);
subplot(1,4,4)
plot(xx,s,'b',xx,u_TV_pwL,'r','LineWidth',1.2);
L=legend('f','TV_{pwL}^{20/3}')
set (L,'FontSize',7)
set (L,'Location','northwest')
pbaspect([1 1.6 1 ])
axis([a b min(s)-0.2 max(s)+0.4])
%title('TV_{pwL}')
ssim4 = ssim(s_true,u_TV_pwL')

print(f1,'images_noise/tvpwl_noise_hat3','-dpdf')
