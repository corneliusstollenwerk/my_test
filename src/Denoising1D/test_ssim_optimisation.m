clear all
clc
rng(234589)

name = 'cos';
delta = 0.1;
params.N = 1000;

%parameter optimization for TV
start_TV = tic;
params.reg = 'TV';
subplot(2,3,1);
[u_TV, params_opt_TV, SSIM_TV] = ssim_optimisation(name,delta,params)
title('TV')
time_TV = toc(start_TV);

%parameter optimization for TV_pwL
start_TV_pwL = tic;
params.reg = 'TV_pwL';
subplot(2,3,2);
[u_TV_pwL, params_opt_TV_pwL, SSIM_TV_pwL] = ssim_optimisation(name,delta,params)
title('TV_{pwL}')
time_TV_pwL = toc(start_TV_pwL);

%parameter optimization for TGV2
start_TGV2 = tic;
params.reg = 'TGV2';
subplot(2,3,3);
[u_TGV2, params_opt_TGV2, SSIM_TGV2] = ssim_optimisation(name,delta,params)
title('TGV2')
time_TGV2 = toc(start_TGV2);


%parameter optimization for TVL2
start_TVL2 = tic;
params.reg = 'TVLp';
params.p = 2;
subplot(2,3,4);
[u_TVL2, params_opt_TVL2, SSIM_TVL2] = ssim_optimisation(name,delta,params)
title('TVL2')
time_TVL2 = toc(start_TVL2);

%parameter optimization for TVL5
start_TVL5 = tic;
params.reg = 'TVLp';
params.p = 5;
subplot(2,3,5);
[u_TVL5, params_opt_TVL5, SSIM_TVL5] = ssim_optimisation(name,delta,params)
title('TVL5')
time_TVL5 = toc(start_TVL5);

%parameter optimization for TVLinf
start_TVLinf = tic;
params.reg = 'TVLp';
params.p = inf;
subplot(2,3,6);
[u_TVLinf, params_opt_TVLinf, SSIM_TVLinf] = ssim_optimisation(name,delta,params)
title('TVL^{\infty}')
time_TVLinf = toc(start_TVLinf);





