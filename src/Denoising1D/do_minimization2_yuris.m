function [u] = do_minimization2_yuris (s, params, lambda)

regularizer = params.reg;

d = params.stepsize;
m = length(s);

% generate differential opertators
D = diffopn(m,1,'forward','neumann');
D = D/d;

E = diffopn(m,1,'backward','neumann');
E = E/d;

cvx_begin quiet
%    cvx_precision best

    cvx_solver Mosek

    variables u(m,1)
    variables g(m,1)

    % fidelty term
    fid = 0;
    norm( u(:) - s(:), 2 ) <= params.bound;
%    fid = 0.5 * sum((u(:) - s(:)).^2);

    switch regularizer
        case 'TV'    % solve optimisation problem with TV using CVX
            % regularisation term
            reg = sum(abs(g(:)));
            D * u == g
            minimize fid + lambda * reg
            
    
        case 'TV_pwL'  % solve optimisation problem with TV_pwL using CVX   
            % set allowed aplitude of gradient in kernel of the regulariser
            gamma = params.gamma;
            % regularisation term
            reg = sum(abs(D * u - g));
            -gamma(:) <= g(:) <= gamma(:)  
            minimize fid + lambda * reg
        
      
        case 'TGV2'
            beta = params.beta;
            if lambda == 0
                reg =  beta * norm( E*g, 1 );
                minimize fid + reg
            else
                reg = norm( D*u-g, 1 )...
                      + beta/lambda * norm( E*g , 1 );
                minimize fid + lambda * reg
            end
                      
               
        case 'TVLp'     % ||Du-g||_1 + b/a* ||g||_p
            p = params.p;
            beta = params.beta;
            if lambda == 0
               reg = beta * norm( g , p ); 
               minimize fid + reg
               
            else
               reg = norm( D*u-g , 1 )...
                     + beta/lambda * norm( g , p );
               minimize fid + lambda * reg
            end
              
                   
    end

cvx_end 

if ~strcmp(cvx_status, 'Solved')
    warning('\tProblem not solved.\tCVX Status: %s', cvx_status);
end