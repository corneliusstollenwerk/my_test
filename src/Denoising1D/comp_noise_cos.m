clear all
clc
close all
rng(23454)

%fun=@(x) cos(x).*(x<=0) -cos(x).*(x>0);
N = 1000;delta = 0.2;
name = 'cos';
[s,s_true,a,b,xx,d]= testfun(name,N,delta);
params.stepsize = d;
params.N = N;

type = 'Gaussian_Bound';
params.fidelity = 'Bound';
if delta == 0
    params.bound = sqrt(N) * max(abs(s_true)) * 0.01;
else 
    params.bound = sqrt(N) * max(abs(s_true)) * delta;
end
lambda = 1;

%%
%do_minimization2 for TV_pwL

params.reg = 'TV';

u_TV = do_minimization(s, params, lambda);
f1=figure('pos',[0 0 900 900]);
subplot(5,1,1)
plot(xx,s,'b',xx,u_TV,'r','LineWidth',1.2);
L=legend('f','TV')
set (L,'FontSize',7)
set (L,'Location','northwest')
pbaspect([2*pi 1.4 1 ])
axis([a b min(s)-0.2 max(s)+0.2])
%title('TV_{pwL}')
ssim1 = ssim(s_true,u_TV')

%%
params.reg = 'TV_pwL';
params.gamma = 1;

u_TV_pwL = do_minimization(s, params, lambda);
subplot(5,1,2)
plot(xx,s,'b',xx,u_TV_pwL,'r','LineWidth',1.2);
L=legend('f','TV_{pwL}^1')
set (L,'FontSize',7)
set (L,'Location','northwest')
pbaspect([2*pi 1.4 1 ])
axis([a b min(s)-0.2 max(s)+0.2])
%title('TV_{pwL}')
ssim2 = ssim(s_true,u_TV_pwL')

%%

params.reg = 'TVLp';
params.p = 2;
params.beta = 18.5043;
u_TVLp = do_minimization(s, params, lambda);
subplot(5,1,3)
plot(xx,s,'b',xx,u_TVLp,'r','LineWidth',1.2);
L=legend('f','TVL^2')
set (L,'FontSize',7)
set (L,'Location','northwest')
pbaspect([2*pi 1.4 1 ])
axis([a b min(s)-0.2 max(s)+0.2])
%title('TV_{pwL}')
ssim3 = ssim(s_true,u_TVLp')

%%

params.reg = 'TVLp';
params.p = inf;
params.beta = 319.8901;
u_TVLp = do_minimization(s, params, lambda);
subplot(5,1,4)
plot(xx,s,'b',xx,u_TVLp,'r','LineWidth',1.2);
L=legend('f','TVL^{\infty}')
set (L,'FontSize',7)
set (L,'Location','northwest')
pbaspect([2*pi 1.4 1 ])
axis([a b min(s)-0.2 max(s)+0.2])
%title('TV_{pwL}')
ssim4 = ssim(s_true,u_TVLp')



%%
params.reg = 'TGV2';
params.beta = 0.3223;
u_TGV = do_minimization(s, params, lambda);
subplot(5,1,5)
plot(xx,s,'b',xx,u_TGV,'r','LineWidth',1.2);
L=legend('f','TGV^2')
set (L,'FontSize',7)
set (L,'Location','northwest')
pbaspect([2*pi 1.4 1 ])
axis([a b min(s)-0.2 max(s)+0.2])
%title('TV_{pwL}')
ssim5 = ssim(s_true,u_TGV')
%print(f1,'images_noise/comp_cos2','-dpdf')