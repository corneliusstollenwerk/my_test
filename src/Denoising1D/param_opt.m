%parameter optimization in 1D

function [param , SSIM] = param_opt ( name, type ,  delta , reg , alpha0 , vararg )

N = 1000;
params.reg = reg;
params.delta = delta;

%determining data from given function, stepsize and noise level
[s,s_true,a,b,xx,d]= testfun(name,N,delta);
params.stepsize = d;


switch type
    case 'Gaussian'
        params.fidelity = 'L2 Squared';
        
        switch reg
            case 'TV'
                alphaend = vararg;
                ssim_neg = @(alpha) - ssim( do_minimization (s , params , alpha),s_true');
                [param, SSIM_neg] = fminbnd( ssim_neg , alpha0 , alphaend );

            case {'TV_pwL','TVLp','TGV2'}
                if isequal(reg,'TVLp') == 1
                    params.p = vararg;
                end
                ssim_neg = @(paramreg) - ssim( do_minimization2 (s , params , paramreg(1) ,...
                    paramreg(2)),s_true');
                A = -eye(2);
                b = [0,0];
    
                [param, SSIM_neg] = fmincon( ssim_neg , alpha0 , A , b);

        end
        SSIM = - SSIM_neg;
        
    case 'Uniform'
        params.fidelity = 'Indicator';
        switch reg
            case 'TV'
                param = 1;
                SSIM = ssim( minimization (noisy , params , param),img);
            case {'TV_pwL','TVLp','TGV2'}
                if isequal(reg,'TVLp') == 1
                    params.p = vararg;
                end
                alpha = 1;
                ssim_neg = @(paramreg) - ssim( minimization2 (noisy , params , alpha ,...
                    paramreg),img);
                [param, SSIM_neg] = fminbnd( ssim_neg , alpha0(1) , alpha0(2) );
                SSIM = -SSIM_neg;
        end
    case 'Gaussian_Bound'
        params.fidelity = 'Bound';
        params.bound = sqrt(N) * max(abs(s_true)) * delta;
        switch reg
            case 'TV'
                param = 1;
                SSIM = ssim( do_minimization2 (s , params , param),s_true');
            case {'TV_pwL','TVLp','TGV2'}
                if isequal(reg,'TVLp') == 1
                    params.p = vararg;
                end
                alpha = 1;
                ssim_neg = @(paramreg) - ssim( do_minimization2 (s , params , alpha ,...
                    paramreg),s_true');
                [param, SSIM_neg] = fminbnd( ssim_neg , alpha0(1) , alpha0(2) );
                SSIM = -SSIM_neg;
        end
end

