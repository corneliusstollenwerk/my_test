%Regularisation with TV_pwL for different noise intensities

clear all
clc
rng(23454)

name = 'cos';

[s,s_true,a,b,xx,d]= testfun(name,0.1,0);
params.stepsize = d;


%do_minimization2 for zero noise
params.reg = 'TV_pwL';
params.gamma = 1;
lambda = 0.1;
u_TV_pwL = do_minimization2(s, params, lambda);
subplot(2,2,1)
plot(xx,s,'b',xx,u_TV_pwL,'r','LineWidth',2);
axis([a b min(s) max(s)])
title('TV_{pwL} for zero noise')
ssim_clean = ssim(s_true,u_TV_pwL')

[s,s_true,a,b,xx,d]= testfun(name,0.1,0.1);

%do_minimization2 for noise=0.1
params.reg = 'TV_pwL';
params.gamma = 1;
lambda = 0.1;
u_TV_pwL = do_minimization2(s, params, lambda);
subplot(2,2,2)
plot(xx,s,'b',xx,u_TV_pwL,'r','LineWidth',2);
axis([a b min(s) max(s)])
title('TV_{pwL} for noise=10%')
ssim_10 = ssim(s_true,u_TV_pwL')


[s,s_true,a,b,xx,d]= testfun(name,0.1,0.25);

%do_minimization2 for noise=0.25
params.reg = 'TV_pwL';
params.gamma = 1;
lambda = 0.1;
u_TV_pwL = do_minimization2(s, params, lambda);
subplot(2,2,3)
plot(xx,s,'b',xx,u_TV_pwL,'r','LineWidth',2);
axis([a b min(s) max(s)])
title('TV_{pwL} for noise=25%')
ssim_25 = ssim(s_true,u_TV_pwL')

[s,s_true,a,b,xx,d]= testfun(name,0.1,0.5);

%do_minimization2 for noise=0.5
params.reg = 'TV_pwL';
params.gamma = 1;
lambda = 0.1;
u_TV_pwL = do_minimization2(s, params, lambda);
subplot(2,2,4)
plot(xx,s,'b',xx,u_TV_pwL,'r','LineWidth',2);
axis([a b min(s) max(s)])
title('TV_{pwL} for noise=50%')
ssim_50 = ssim(s_true,u_TV_pwL')

suptitle('Reconstruction with TV_{pwL} for different noise intensities')

