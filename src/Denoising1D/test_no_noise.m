clear all
clc
rng(23454)

%fun=@(x) cos(x).*(x<=0) -cos(x).*(x>0);
N = 1000;
delta = 0;
name = 'ramp';
[s,s_true,a,b,xx,d]= testfun(name,N,delta);
params.stepsize = d;
params.N = N;
type = 'Gaussian';
params.fidelity = 'L2 Squared';

subplot(3,2,1)
plot(xx,s,'b','LineWidth',2);

%%
%do_minimization2 for TV
params.reg = 'TV';
switch name
    case 'affine'
        lambda = 1;
    case 'constant'
        lambda = 1;
    case 'cos'
        lambda = 1;
    case 'hat'
        lambda = 1;
    case 'quadratic'
        lambda = 1;
    case 'ramp'
        lambda = 0.1;
end   
        
u_TV = do_minimization(s, params, lambda);
subplot(3,2,2)
plot(xx,s,'b',xx,u_TV,'r','LineWidth',2);
axis([a b min(s) max(s)])
title('TV')
ssim_TV = ssim(s_true,u_TV')

%%
%do_minimization2 for TV_pwL
params.reg = 'TV_pwL';
switch name
    case 'affine'
        lambda = 1;
        params.gamma = 1;
    case 'constant'
        lambda = 1;
        params.gamma = 1.0795e+03;
    case 'cos'
        lambda = 1;
        params.gamma = 1;
    case 'hat'
        lambda = 1;
        params.gamma = 1;
    case 'quadratic'
        lambda = 1;
        params.gamma = 1;
    case 'ramp'
        lambda = 0.1;
        params.gamma = 5/2;
end   
u_TV_pwL = do_minimization(s, params, lambda);
subplot(3,2,3)
plot(xx,s,'b',xx,u_TV_pwL,'r','LineWidth',2);
axis([a b min(s) max(s)])
title('TV_{pwL}')
ssim_TV_pwL = ssim(s_true,u_TV_pwL')

%%

%do_minimization2 for TGV2
params.reg = 'TGV2';
switch name
    case 'affine'
        lambda = 1;
        params.beta = 1;
    case 'constant'
        lambda = 1;
        params.beta = 1;
    case 'cos'
        lambda = 1;
        params.beta = 1;
    case 'hat'
        lambda = 1;
        params.beta = 1;
    case 'quadratic'
        lambda = 1;
        params.beta = 1;
    case 'ramp'
        lambda = 0.1;
        params.beta = 1;
end   
u_TGV = do_minimization(s, params, lambda);
subplot(3,2,4)
plot(xx,s,'b',xx,u_TGV,'r','LineWidth',2);
axis([a b min(s) max(s)])
title('TGV')
ssim_TGV = ssim(s_true,u_TGV')

%%

%do_minimization2 for TVLp
params.reg = 'TVLp';
params.p = 2;
switch name
    case 'affine'
        lambda = 1;
        params.beta = 1;
    case 'constant'
        lambda = 1;
        params.beta = 1;
    case 'cos'
        lambda = 1;
        params.beta = 1;
    case 'hat'
        lambda = 1;
        params.beta = 1;
    case 'quadratic'
        lambda = 1;
        params.beta = 1;
    case 'ramp'
        lambda = 0.1;
        params.beta = 1;
end   
u_TVLp = do_minimization(s, params, lambda);
subplot(3,2,5)
plot(xx,s,'b',xx,u_TVLp,'r','LineWidth',2);
axis([a b min(s) max(s)])
title('TVL_3')
ssim_TVL3 = ssim(s_true,u_TVLp')

%%
%do_minimization2 for TVLinfty
params.reg = 'TVLp';
params.p = inf;
switch name
    case 'affine'
        lambda = 1;
        params.beta = 1;
    case 'constant'
        lambda = 1;
        params.beta = 1;
    case 'cos'
        lambda = 1;
        params.beta = 1;
    case 'hat'
        lambda = 1;
        params.beta = 1;
    case 'quadratic'
        lambda = 1;
        params.beta = 1;
    case 'ramp'
        lambda = 0.1;
        params.beta = 1;
end   
u_TVL_infty = do_minimization(s, params, lambda);
subplot(3,2,6)
plot(xx,s,'b',xx,u_TVL_infty,'r','LineWidth',2);
axis([a b min(s) max(s)])
title('TVL_{\infty}')
ssim_TVL_infty = ssim(s_true,u_TVL_infty')