clear all
clc
close all
rng(23454)

%fun=@(x) cos(x).*(x<=0) -cos(x).*(x>0);
N = 1000;
delta = 0;
name = 'cos';
params.reg = 'TV_pwL';
[s,s_true,a,b,xx,d]= testfun(name,N,delta);
params.stepsize = d;
params.N = N;
type = 'Gaussian';
params.fidelity = 'L2 Squared';

m = length(s_true);
D = diffopn(m,1,'forward','neumann');
D = D/d;

params.gamma = norms(D*s_true',2,2);

% subplot(2,2,1)
% plot(xx,s,'b','LineWidth',1.5);
lambda = 0.1;

%%
%do_minimization2 for TV_pwL

delta = 0;
[s,s_true,a,b,xx,d]= testfun(name,N,delta);

u_TV_pwL = do_minimization(s, params, lambda);
f1=figure('pos',[0 0 900 600]);
subplot(2,2,1)
plot(xx,s,'b',xx,u_TV_pwL,'r','LineWidth',1.2);
L=legend('f','TV_{pwL}^{1}')
set (L,'FontSize',7)
set (L,'Location','northwest')
pbaspect([2*pi 4 1 ])
axis([a b -4 4])
%title('TV_{pwL}')
ssim_1 = ssim(s_true,u_TV_pwL')
%%
%do_minimization2 for TV_pwL

delta = 0.2;
[s,s_true,a,b,xx,d]= testfun(name,N,delta);

u_TV_pwL = do_minimization(s, params, lambda);
subplot(2,2,2)
plot(xx,s,'b',xx,u_TV_pwL,'r','LineWidth',1.2);
L=legend('f','TV_{pwL}^{1}')
set (L,'FontSize',7)
set (L,'Location','northwest')
pbaspect([2*pi 4 1 ])
axis([a b -4 4])
%title('TV_{pwL}')
ssim_2 = ssim(s_true,u_TV_pwL')

%%
delta = 0.4;
[s,s_true,a,b,xx,d]= testfun(name,N,delta);

u_TV_pwL = do_minimization(s, params, lambda);
subplot(2,2,3)
plot(xx,s,'b',xx,u_TV_pwL,'r','LineWidth',1.2);
L=legend('f','TV_{pwL}^1')
set (L,'FontSize',7)
set (L,'Location','northwest')
pbaspect([2*pi 4 1 ])
axis([a b -4 4])
%title('TV_{pwL}')
ssim_3 = ssim(s_true,u_TV_pwL')

%%
delta = 1;
[s,s_true,a,b,xx,d]= testfun(name,N,delta);

u_TV_pwL = do_minimization(s, params, lambda);
subplot(2,2,4)
plot(xx,s,'b',xx,u_TV_pwL,'r','LineWidth',1.2);
L=legend('f','TV_{pwL}^{1}')
set (L,'FontSize',7)
set (L,'Location','northwest')
pbaspect([2*pi 4 1 ])
axis([a b -4 4])
%title('TV_{pwL}')
ssim_4 = ssim(s_true,u_TV_pwL')
%print(f1,'images_noise/tvpwl_noise_cos_exactgrad','-dpdf')