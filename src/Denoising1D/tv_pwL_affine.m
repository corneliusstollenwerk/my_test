clear all
clc

params.reg = 'TV_pwL';

grad = 0.8;
jump = 1 - grad;

data = affine ( grad , jump );
delta = 0.025;
s = size(data);
params.stepsize = 10/(s(2)-1);
xx = 0:(10/(s(2)-1)):10;
noisy = data + max(abs(data)) * delta * randn(s);

alpha0 = 0;
alphaend = 1;

%%
params.gamma = 0.5*grad;
ssim_neg = @(lambda) - ssim( do_minimization2 (noisy , params , lambda),data');
[lambda1, SSIM_neg] = fminbnd( ssim_neg , alpha0 , alphaend );
u_TV_pwL = do_minimization2(noisy, params, lambda1);
subplot(2,3,1)
plot(xx',noisy,'b',xx',u_TV_pwL,'r','LineWidth',2);
axis([0 10 min(noisy) max(noisy)])
title('TV_{pwL} with jump 1, \gamma = 0.5*grad')
ssim1 = ssim(data,u_TV_pwL')

%%
params.gamma = grad;
ssim_neg = @(lambda) - ssim( do_minimization2 (noisy , params , lambda),data');
[lambda2, SSIM_neg] = fminbnd( ssim_neg , alpha0 , alphaend );
u_TV_pwL = do_minimization2(noisy, params, lambda2);
subplot(2,3,2)
plot(xx',noisy,'b',xx',u_TV_pwL,'r',xx',data,'y','LineWidth',1);
axis([0 10 min(noisy) max(noisy)])
title('TV_{pwL} with jump 1, \gamma = grad')
ssim2 = ssim(data,u_TV_pwL')

%%
params.gamma = 2*grad;
ssim_neg = @(lambda) - ssim( do_minimization2 (noisy , params , lambda),data');
[lambda3, SSIM_neg] = fminbnd( ssim_neg , alpha0 , alphaend );
u_TV_pwL = do_minimization2(noisy, params, lambda3);
subplot(2,3,3)
plot(xx',noisy,'b',xx',u_TV_pwL,'r','LineWidth',2);
axis([0 10 min(noisy) max(noisy)])
title('TV_{pwL} with jump 1, \gamma = 2*grad')
ssim3 = ssim(data,u_TV_pwL')

%%
grad = 0.6;
jump = 1 - grad;

data = affine ( grad , jump );
s = size(data);
params.stepsize = 10/(s(2)-1);
xx = 0:(10/(s(2)-1)):10;
noisy = data + max(abs(data)) * delta * randn(s);

%%
params.gamma = 0.5*grad;
ssim_neg = @(lambda) - ssim( do_minimization2 (noisy , params , lambda),data');
[lambda4, SSIM_neg] = fminbnd( ssim_neg , alpha0 , alphaend );
u_TV_pwL = do_minimization2(noisy, params, lambda4);
subplot(2,3,4)
plot(xx',noisy,'b',xx',u_TV_pwL,'r','LineWidth',2);
axis([0 10 min(noisy) max(noisy)])
title('TV_{pwL} with jump 2, \gamma = 0.5*grad')
ssim4 = ssim(data,u_TV_pwL')

%%
params.gamma = grad;
ssim_neg = @(lambda) - ssim( do_minimization2 (noisy , params , lambda),data');
[lambda5, SSIM_neg] = fminbnd( ssim_neg , alpha0 , alphaend );
u_TV_pwL = do_minimization2(noisy, params, lambda5);
subplot(2,3,5)
plot(xx',noisy,'b',xx',u_TV_pwL,'r','LineWidth',2);
axis([0 10 min(noisy) max(noisy)])
title('TV_{pwL} with jump 2, \gamma = grad')
ssim5 = ssim(data,u_TV_pwL')

%%
params.gamma = 2*grad;
ssim_neg = @(lambda) - ssim( do_minimization2 (noisy , params , lambda),data');
[lambda6, SSIM_neg] = fminbnd( ssim_neg , alpha0 , alphaend );
u_TV_pwL = do_minimization2(noisy, params, lambda6);
subplot(2,3,6)
plot(xx',noisy,'b',xx',u_TV_pwL,'r','LineWidth',2);
axis([0 10 min(noisy) max(noisy)])
title('TV_{pwL} with jump 2, \gamma = 2*grad')
ssim6 = ssim(data,u_TV_pwL')