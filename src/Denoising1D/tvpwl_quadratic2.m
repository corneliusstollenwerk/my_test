clear all
clc
close all
rng(23454)

%fun=@(x) cos(x).*(x<=0) -cos(x).*(x>0);
N = 1000;
delta = 0;
name = 'quadratic2';
params.reg = 'TV_pwL';
[s,s_true,a,b,xx,d]= testfun(name,N,delta);
params.stepsize = d;
params.N = N;
type = 'Gaussian';
params.fidelity = 'L2 Squared';

% subplot(2,2,1)
% plot(xx,s,'b','LineWidth',1.5);
lambda = 0.01;

%%
%do_minimization2 for TV_pwL

params.gamma = 0;

u_TV_pwL = do_minimization(s, params, lambda);
f1=figure('pos',[0 0 900 300]);
subplot(1,3,1)
plot(xx,s,'b',xx,u_TV_pwL,'r','LineWidth',1.5);
L=legend('f','TV_{pwL}^0')
set (L,'FontSize',7)
set (L,'Location','northwest')
pbaspect([1 1.4 1 ])
axis([a b min(s)-0.2 max(s)+0.2])
%title('TV_{pwL}')
ssim_0 = ssim(s_true,u_TV_pwL')

%%
params.gamma = 0.8;

u_TV_pwL = do_minimization(s, params, lambda);
subplot(1,3,2)
plot(xx,s,'b',xx,u_TV_pwL,'r','LineWidth',1.5);
L=legend('f','TV_{pwL}^{0.8}')
set (L,'FontSize',7)
set (L,'Location','northwest')
pbaspect([1 1.4 1 ])
axis([a b min(s)-0.2 max(s)+0.2])
%title('TV_{pwL}')
ssim_02 = ssim(s_true,u_TV_pwL')


%%
params.gamma = 1.6;

u_TV_pwL = do_minimization(s, params, lambda);
subplot(1,3,3)
plot(xx,s,'b',xx,u_TV_pwL,'r','LineWidth',1.5);
L=legend('f','TV_{pwL}^{1.6}')
set (L,'FontSize',7)
set (L,'Location','northwest')
pbaspect([1 1.4 1 ])
axis([a b min(s)-0.2 max(s)+0.2])
%title('TV_{pwL}')
ssim_1 = ssim(s_true,u_TV_pwL')
%print(f1,'images_nonoise/tvpwl_quadratic3','-dpdf')

% %%
% params.gamma = 10;
% 
% u_TV_pwL = do_minimization(s, params, lambda);
% subplot(1,2,2)
% plot(xx,s,'b',xx,u_TV_pwL,'r','LineWidth',1.5);
% pbaspect([2 1.4 1 ])
% axis([a b min(s)-0.2 max(s)+0.2])
% %title('TV_{pwL}')
% ssim_10 = ssim(s_true,u_TV_pwL')
% %print(f2,'images_nonoise/tvpwl_affine2','-dpdf')