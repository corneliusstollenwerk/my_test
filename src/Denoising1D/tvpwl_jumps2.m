clear all
clc
close all
rng(23454)

%fun=@(x) cos(x).*(x<=0) -cos(x).*(x>0);
N = 1000;
delta = 0;
name = 'constant';
params.reg = 'TV_pwL';
[s,s_true,a,b,xx,d]= testfun(name,N,delta);
params.stepsize = d;
params.N = N;
type = 'Gaussian';
params.fidelity = 'L2 Squared';

% subplot(2,2,1)
% plot(xx,s,'b','LineWidth',1.5);


%%
%do_minimization2 for TV_pwL

params.gamma = 0;
f1=figure('pos',[0 0 900 300]);

lambda = 1;
u_TV_pwL = do_minimization(s, params, lambda);
subplot(1,2,1)
plot(xx,s,'b',xx,u_TV_pwL,'r','LineWidth',1.5);
L=legend('f','TV_{pwL}^0, \alpha=1')
set (L,'FontSize',7)
set (L,'Location','northwest')
pbaspect([2 1.4 1 ])
axis([a b min(s)-0.2 max(s)+0.2])
%title('TV_{pwL}')
ssim1 = ssim(s_true,u_TV_pwL')

lambda = 0.01;
u_TV_pwL = do_minimization(s, params, lambda);
subplot(1,2,2)
plot(xx,s,'b',xx,u_TV_pwL,'r','LineWidth',1.5);
L=legend('f','TV_{pwL}^0, \alpha=0.01')
set (L,'FontSize',7)
set (L,'Location','northwest')
pbaspect([2 1.4 1 ])
axis([a b min(s)-0.2 max(s)+0.2])
%title('TV_{pwL}')
ssim2 = ssim(s_true,u_TV_pwL')
print(f1,'images_nonoise/tvpwl_jump3','-dpdf')

%%
%do_minimization2 for TV_pwL

params.gamma = 1;
f2=figure('pos',[0 0 900 300]);

lambda = 1;
u_TV_pwL = do_minimization(s, params, lambda);
subplot(1,2,1)
plot(xx,s,'b',xx,u_TV_pwL,'r','LineWidth',1.5);
L=legend('f','TV_{pwL}^1, \alpha=1')
set (L,'FontSize',7)
set (L,'Location','northwest')
pbaspect([2 1.4 1 ])
axis([a b min(s)-0.2 max(s)+0.2])
%title('TV_{pwL}')
ssim3 = ssim(s_true,u_TV_pwL')

lambda = 0.01;
u_TV_pwL = do_minimization(s, params, lambda);
subplot(1,2,2)
plot(xx,s,'b',xx,u_TV_pwL,'r','LineWidth',1.5);
L=legend('f','TV_{pwL}^1, \alpha=0.01')
set (L,'FontSize',7)
set (L,'Location','northwest')
pbaspect([2 1.4 1 ])
axis([a b min(s)-0.2 max(s)+0.2])
%title('TV_{pwL}')
ssim4 = ssim(s_true,u_TV_pwL')
print(f2,'images_nonoise/tvpwl_jump4','-dpdf')

%%
%do_minimization2 for TV_pwL

params.gamma = 10;
f3=figure('pos',[0 0 900 300]);

lambda = 1;
u_TV_pwL = do_minimization(s, params, lambda);
subplot(1,2,1)
plot(xx,s,'b',xx,u_TV_pwL,'r','LineWidth',1.5);
L=legend('f','TV_{pwL}^{10}, \alpha=1')
set (L,'FontSize',7)
set (L,'Location','northwest')
pbaspect([2 1.4 1 ])
axis([a b min(s)-0.2 max(s)+0.2])
%title('TV_{pwL}')
ssim5 = ssim(s_true,u_TV_pwL')

lambda = 0.01;
u_TV_pwL = do_minimization(s, params, lambda);
subplot(1,2,2)
plot(xx,s,'b',xx,u_TV_pwL,'r','LineWidth',1.5);
L=legend('f','TV_{pwL}^{10}, \alpha=0.01')
set (L,'FontSize',7)
set (L,'Location','northwest')
pbaspect([2 1.4 1 ])
axis([a b min(s)-0.2 max(s)+0.2])
%title('TV_{pwL}')
ssim6 = ssim(s_true,u_TV_pwL')
print(f3,'images_nonoise/tvpwl_jump5','-dpdf')

