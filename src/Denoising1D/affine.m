function [data] = affine ( grad , jump )

a = grad;
b = jump;

fun= @(x) ( a.*x ) .* ( x<=0.5 ) +( 0.5.*a + b + a.*(x-0.5) ) .* ( x>0.5 );

d = 1/(1000-1);
xx = 0:d:1;

data = fun( xx );