function [s,s_true,a,b,xx,d] = testfun(name,N,delta)
rng(23454)

switch name
    case 'simplehat'
        fun=@(x) 2.*x.*(x<=0.5) +(2-2.*x).*(x>0.5);
        a = 0;
        b = 1;
        
    case 'hat'
        fun=@(x) 0.*(x<=0.2) + ((10/3).*x-(2/3)).*(0.2<x).*(x<=0.5) + ((-10/3).*x+(8/3)).*(0.5<x).*(x<=0.8) + 0.*(0.8<x);
        a = 0;
        b = 1;
       
    case 'constant'
        fun=@(x) 0.*(x<=1) +1.*(x>1);
        a = 0;
        b = 2;
        
    case 'plateau'
        fun= @(x) 0.*(x<=0.3) + 1.*(0.3<x).*(x<=0.7) + 0.*(0.7<x);
        a = 0;
        b = 1;
        
    case 'affine'
        fun=@(x) (0.3.*x).*(x<=1) +(0.7.*x).*(x>1);
        a = 0;
        b = 2;

    case 'affine2'
        fun=@(x) (0.4.*x).*(x<=0.5) +(0.7.*x+0.3).*(x>0.5).*(x<=1) + (-0.6*(x-1)+0.8).*(x>1).*(x<=1.5)+(-0.6*(x-1)+0.6).*(x>1.5);
        a = 0;
        b = 2;
        
    case 'ramp'
        fun=@(x) 0.*(x<=0.8) + ((5/2).*(x-0.5)-(3/4)).*(0.8<x).*(x<=1.2) + 1.*(1.2<x);
        a = 0;
        b = 2;
        
    case 'quadratic'
        fun=@(x) (0.15.*x.^2).*(x<=1) +(0.15.*x.^2+0.4).*(x>1);
        a = 0;
        b = 2;        

    case 'quadratic2'
        fun=@(x) (0.8.*x.^2).*(x<=0.5) +(0.8.*x.^2+0.2).*(x>0.5);
        a = 0;
        b = 1;
        
    case 'quadratic3'
        fun=@(x) (1/4)*x.^2;
        a = 0;
        b = 2;
        
    case 'cos01'
        fun=@(x) (cos(4*pi*x-2*pi)./2 +0.5).*(x<=0.5) +(-cos(4*pi*x-2*pi)./2 +0.5).*(x>0.5);
        a = 0;
        b = 1;
        
    case 'cos'
        fun=@(x) cos(x).*(x<=0) - cos(x).*(x>0);
        a = -2*pi;
        b = 2*pi;
        
    case 'all'
        fun=@(x) (0<=x).*(x<1).*(2*x)...
             +(1<=x).*(x<2).*((-1)*x+2)...
             +(2<=x).*(x<3).*4 ...
             +(3<=x).*(x<4).*2 ...
             +(4<=x).*(x<5).*((2*(x-4.5)).^2) ...
             +(5<=x).*(x<7).*(-1*(x-6).^2+3) ...
             +(7<x).*(-x+8);
            a = 0;
        b = 8;
end
d = (b-a)/(N-1);
xx=a:d:b;

% generate true signal
s = fun(xx);
s_true = s;
    
% generate noise
noise = max(abs(s)) * delta * randn(size(s));
%    noise(1:(m-1)/4) = 0;
   
% add noise
s = s + noise;

