clear all
clc
close all
rng(23454)

N = 1000;
delta = 0.1;
name = 'all';
[s,s_true,a,b,xx,d]= testfun(name,N,delta);
params.stepsize = d;
params.N = N;

type = 'Gaussian_Bound';
params.fidelity = 'Bound';
if delta == 0
    params.bound = sqrt(N) * max(abs(s_true)) * 0.01;
else 
    params.bound = sqrt(N) * max(abs(s_true)) * delta;
end
lambda = 1;

%%
%do_minimization2 for TV

params.reg = 'TV';

u_TV = do_minimization(s, params, lambda);
f1=figure('pos',[0 0 900 280]);
subplot(1,2,1)
plot(xx,s,'b',xx,u_TV,'r','LineWidth',1.2);
L=legend('f','TV')
set (L,'FontSize',7)
set (L,'Location','northeast')
pbaspect([8 6 1 ])
axis([a b min(s)-0.2 max(s)+0.2])
%title('TV_{pwL}')
ssim1 = ssim(s_true,u_TV')

%%
params.reg = 'TV_pwL';
params.gamma = 0.8985;

u_TV_pwL = do_minimization(s, params, lambda);
subplot(1,2,2)
plot(xx,s,'b',xx,u_TV_pwL,'r','LineWidth',1.2);
L=legend('f','TV_{pwL}^{\gamma}')
set (L,'FontSize',7)
set (L,'Location','northeast')
pbaspect([8 6 1 ])
axis([a b min(s)-0.2 max(s)+0.2])
%title('TV_{pwL}')
ssim2 = ssim(s_true,u_TV_pwL')
%print(f1,'images_noise/comp_all1','-dpdf')

%%
f2=figure('pos',[0 0 800 280]);
params.reg = 'TVLp';
params.p = 2;
params.beta = 16.4832;
u_TVLp = do_minimization(s, params, lambda);
subplot(1,2,1)
plot(xx,s,'b',xx,u_TVLp,'r','LineWidth',1.2);
L=legend('f','TVL^2')
set (L,'FontSize',7)
set (L,'Location','northeast')
pbaspect([8 6 1 ])
axis([a b min(s)-0.2 max(s)+0.2])
%title('TV_{pwL}')
ssim3 = ssim(s_true,u_TVLp')

%%

params.reg = 'TVLp';
params.p = inf;
params.beta = 490.1291;
u_TVLp = do_minimization(s, params, lambda);
subplot(1,2,2)
plot(xx,s,'b',xx,u_TVLp,'r','LineWidth',1.2);
L=legend('f','TVL^{\infty}')
set (L,'FontSize',7)
set (L,'Location','northeast')
pbaspect([8 6 1 ])
axis([a b min(s)-0.2 max(s)+0.2])
%title('TV_{pwL}')
ssim4 = ssim(s_true,u_TVLp')
%print(f2,'images_noise/comp_all2','-dpdf')

%%
f3=figure('pos',[0 0 900 280]);
params.reg = 'TGV2';
params.beta = 0.1861;
u_TGV = do_minimization(s, params, lambda);
subplot(1,1,1)
plot(xx,s,'b',xx,u_TGV,'r','LineWidth',1.2);
L=legend('f','TGV^2')
set (L,'FontSize',7)
set (L,'Location','northeast')
pbaspect([8 6 1 ])
axis([a b min(s)-0.2 max(s)+0.2])
%title('TV_{pwL}')
ssim5 = ssim(s_true,u_TGV')
%print(f3,'images_noise/comp_all3','-dpdf')