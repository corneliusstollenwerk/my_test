clear all
clc
rng(23454)

%fun=@(x) cos(x).*(x<=0) -cos(x).*(x>0);
N = 1000;
delta = 0.05;
[s,s_true,a,b,xx,d]= testfun('hat',N,delta);
params.stepsize = d;
params.N = N;
sigma = max(abs(s_true)) * delta;
params.sigma = sigma;
params.bound = params.sigma * sqrt(N);

%% do_minimization2 for TV
params.reg = 'TV';
lambda = 1;
u_TV = do_minimization2_yuris(s, params, lambda);
subplot(3,2,1)
plot(xx,s,'b',xx,u_TV,'r','LineWidth',2);
axis([a b min(s) max(s)])
title('TV')
ssim_TV = ssim(s_true,u_TV')

%% do_minimization2 for TV_pwL
params.reg = 'TV_pwL';
params.gamma = 1;
lambda = 1;
u_TV_pwL = do_minimization2_yuris(s, params, lambda);
subplot(3,2,2)
plot(xx,s,'b',xx,u_TV_pwL,'r','LineWidth',2);
axis([a b min(s) max(s)])
title('TV_{pwL}')
ssim_TV_pwL = ssim(s_true,u_TV_pwL')


%% do_minimization2 for TGV2
params.reg = 'TGV2';
params.beta = 0.01;
lambda = 0.1;
u_TGV = do_minimization2_yuris(s, params, lambda);
subplot(3,2,3)
plot(xx,s,'b',xx,u_TGV,'r','LineWidth',2);
axis([a b min(s) max(s)])
title('TGV')
ssim_TGV = ssim(s_true,u_TGV')

%% do_minimization2 for TVLp
params.reg = 'TVLp';
params.p = 3;
params.beta = 10;
lambda = 0.1;
u_TVLp = do_minimization2_yuris(s, params, lambda);
subplot(3,2,4)
plot(xx,s,'b',xx,u_TVLp,'r','LineWidth',2);
axis([a b min(s) max(s)])
title('TVL_3')
ssim_TVL3 = ssim(s_true,u_TVLp')

%% do_minimization2 for TVLp
params.reg = 'TVLp';
params.p = 5;
params.beta = 10;
lambda = 0.1;
u_TVLp = do_minimization2_yuris(s, params, lambda);
subplot(3,2,5)
plot(xx,s,'b',xx,u_TVLp,'r','LineWidth',2);
axis([a b min(s) max(s)])
title('TVL_5')
ssim_TVL5 = ssim(s_true,u_TVLp')

%% do_minimization2 for TVLinfty
params.reg = 'TVLp';
params.p = inf;
params.beta = 1500;
lambda = 1500;
u_TVL_infty = do_minimization2_yuris(s, params, lambda);
subplot(3,2,6)
plot(xx,s,'b',xx,u_TVL_infty,'r','LineWidth',2);
axis([a b min(s) max(s)])
title('TVL_{\infty}')
ssim_TVL_infty = ssim(s_true,u_TVL_infty')

