clc
clear all
close all


load('synthetic3_0.05.mat');

f1 = figure
imshow(uint8(orig))
print(f1,'images/synthetic305/orig','-deps')

f2= figure
imshow(uint8(noisy))
print(f2,'images/synthetic305/noisy','-deps')

f3= figure
imshow(uint8(x_tv))
print(f3,'images/synthetic305/tv','-deps')

f4= figure
imshow(uint8(x_tv_pwl))
print(f4,'images/synthetic305/tvpwl','-deps')

f5= figure
imshow(uint8(x_tvlp))
print(f5,'images/synthetic305/tvl2','-deps')

f6= figure
imshow(uint8(x_tvlinf))
print(f6,'images/synthetic305/tvlinf','-deps')

f7= figure
imshow(uint8(x_tgv2))
print(f7,'images/synthetic305/tgv','-deps')