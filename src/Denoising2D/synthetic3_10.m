clc
clear all
close all


load('synthetic3_0.1.mat');

f1 = figure
imshow(uint8(orig))
print(f1,'images/synthetic310/orig','-deps')

f2= figure
imshow(uint8(noisy))
print(f2,'images/synthetic310/noisy','-deps')

f3= figure
imshow(uint8(x_tv))
print(f3,'images/synthetic310/tv','-deps')

f4= figure
imshow(uint8(x_tv_pwl))
print(f4,'images/synthetic310/tvpwl','-deps')

f5= figure
imshow(uint8(x_tvlp))
print(f5,'images/synthetic310/tvl2','-deps')

f6= figure
imshow(uint8(x_tvlinf))
print(f6,'images/synthetic310/tvlinf','-deps')

f7= figure
imshow(uint8(x_tgv2))
print(f7,'images/synthetic310/tgv','-deps')