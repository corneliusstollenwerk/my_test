%parameter optimization in 2D

function [param , SSIM] = param_opt ( img , type, delta , reg , alpha0 , vararg )

seed = 5849774;
rng(seed)
%add gaussian noise
noisy = add_noise( img , type , delta );
noisy = double(noisy);

sz = numel(img);
sz = sqrt(sz);

params.regularizer = reg;
params.delta = delta;

switch type
    case 'Gaussian'
        params.fidelity = 'L2 squared';
        switch reg
            case 'TV'
                alphaend = vararg;
                ssim_neg = @(alpha) - ssim( minimization (noisy , params , alpha),img);
                [param, SSIM_neg] = fminbnd( ssim_neg , alpha0 , alphaend );

            case {'TV_pwL','TVLp','TGV2'}
                if isequal(reg,'TVLp') == 1
                    params.p = vararg;
                end
                ssim_neg = @(paramreg) - ssim( minimization2 (noisy , params , paramreg(1) ,...
                    paramreg(2)),img);
                A = -eye(2);
                b = [0,0];
    
                [param, SSIM_neg] = fmincon( ssim_neg , alpha0 , A , b);

        end
        SSIM = - SSIM_neg;
        
    case 'Uniform'
        params.fidelity = 'Indicator';
        switch reg
            case 'TV'
                param = 1;
                SSIM = ssim( minimization (noisy , params , param),img);
            case {'TV_pwL','TVLp','TGV2'}
                if isequal(reg,'TVLp') == 1
                    params.p = vararg;
                end
                alpha = 1;
                ssim_neg = @(paramreg) - ssim( minimization2 (noisy , params , alpha ,...
                    paramreg),img);
                [param, SSIM_neg] = fminbnd( ssim_neg , alpha0(1) , alpha0(2) );
                SSIM = -SSIM_neg;
        end
    case 'Gaussian_Bound'
        params.fidelity = 'Bound';
        params.bound = sz * 255 * delta;
        switch reg
            case 'TV'
                param = 1;
                SSIM = ssim( minimization (noisy , params , param),img);
            case {'TV_pwL','TVLp','TGV2'}
                if isequal(reg,'TVLp') == 1
                    params.p = vararg;
                end
                alpha = 1;
                ssim_neg = @(paramreg) - ssim( minimization2 (noisy , params , alpha ,...
                    paramreg),img);
                [param, SSIM_neg] = fminbnd( ssim_neg , alpha0(1) , alpha0(2) );
                SSIM = -SSIM_neg;
        end
end

