clear all
 clc

seed = 58497174;
rng(seed)

image = 'lmb1';
type = 'Gaussian';

switch image
    
    case 'lmb1'
        load('testimages/LMB/proj190.mat')
        orig = double(gt);
        noisy = double(noisy);
        
    case 'lmb2'
        load('testimages/LMB/proj302.mat')
        orig = double(gt);
        noisy = double(noisy);
        
    case 'lmb3'
        load('testimages/LMB/proj712.mat')
        orig = double(gt);
        noisy = double(noisy);
        
    case 'lmb4'
        load('testimages/LMB/proj2519.mat')
        orig = double(gt);
        noisy = double(noisy);
        
    case 'lmb5'
        load('testimages/LMB/proj3485.mat')
        orig = double(gt);
        noisy = double(noisy);
        
    case 'lmb6'
        load('testimages/LMB/proj10392.mat')
        orig = double(gt);
        noisy = double(noisy);
        
end

SSIM_noisy = ssim(noisy,orig)

figure
subplot(2,3,1)
imshow(orig)
title('original')
subplot(2,3,2)
imshow(noisy)
title('noisy image')

params.fidelity = 'L2 squared';

%%
% generate differential operators
sg = size(orig);
D = diffopn(sg,1,'forward','neumann');
Dgt = reshape( D * orig(:) , prod(sg), 2 );


%%
switch image
    case 'lmb1'
        alpha = 0.6;
    case 'lmb2'
        alpha = 1;
    case 'lmb3'
        alpha = 1;
    case 'lmb4'
        alpha = 1;
    case 'lmb5'
        alpha = 1;
    case 'lmb6'
        alpha = 1;
end

params.gamma = Dgt;
[x_tv_pwl] = minimization_tv_pwl(noisy,params,alpha);
subplot(2,3,3)
imshow(x_tv_pwl)
title('denoised image')
SSIM1 = ssim(x_tv_pwl,orig)

%%
switch image
    case 'lmb1'
        alpha = 1;
    case 'lmb2'
        alpha = 1;
    case 'lmb3'
        alpha =1;
    case 'lmb4'
        alpha = 1;
    case 'lmb5'
        alpha = 1;
    case 'lmb6'
        alpha = 1;
end

gamma = max(abs(norms(Dgt,1,2)));
params.gamma = gamma;
params.regularizer = 'TV_pwL';
[x_tv_pwl2] = minimization(noisy,params,alpha);
subplot(2,3,4)
imshow(x_tv_pwl2)
title('denoised image')
SSIM2 = ssim(orig,x_tv_pwl2)

%%
switch image
    case 'lmb1'
        alpha = 0.01;
    case 'lmb2'
        alpha = 1;
    case 'lmb3'
        alpha =1;
    case 'lmb4'
        alpha = 1;
    case 'lmb5'
        alpha = 1;
    case 'lmb6'
        alpha = 1;
end
G = zeros(24,24);
gamma = reshape (abs(norms(Dgt,1,2)), sg(1),sg(2));
for i = 1:24
    for j = 1:24
        G(i,j) = max(max(gamma(i*16-15:i*16,j*16-15:j*16)));
    end
end
for i = 1:24
    for j = 1:24
        gamma( i*16-15:i*16,j*16-15:j*16) = ones(16,16)*G(i,j);
    end
end
params.gamma = reshape (gamma, prod(sg),1);
params.gamma = [params.gamma params.gamma];
[x_tv_pwl3] = minimization_tv_pwl(noisy,params,alpha);
subplot(2,3,5)
imshow(x_tv_pwl3)
title('denoised image')
SSIM3 = ssim(x_tv_pwl3,orig)