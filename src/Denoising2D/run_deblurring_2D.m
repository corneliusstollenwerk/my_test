clear all
clc

%addpath ../../_Library/tools

seed = 5849774;
%seed = 6584564; %(squares,horiz)
rng(seed)

cameraman128 = imread('testimages/128x128/camera_128.tiff');
bird128 = imread('testimages/128x128/bird_128.tif');
circles128 = imread('testimages/128x128/circles_128.tif');
horiz128 = imread('testimages/128x128/horiz_128.tif');
lena128 = imread('testimages/128x128/lena_128.tif');
squares128 = imread('testimages/128x128/squares_128.tif');

orig = double(cameraman128);
figure
subplot(2,3,1);
imshow(uint8(orig))
title('original')

n = size(orig,1);
% variance of the Gaussian blur
sigma = 1;

if n == 100
    if sigma == 1
        load oper100_1.mat
        oper = oper100_1;
    elseif sigma == 5
        load oper100_5.mat
        oper = oper100_5;
    end
elseif n == 128
    if sigma == 1
        load oper128_1.mat
        oper = oper128_1;
    elseif sigma == 5
        load oper128_5.mat
        oper = oper128_5;
    end
end

% noise
c = 10;
noisy = addNoise(orig, oper, 'Uniform',-c,c);
% subplot(2,3,2);
% imshow(uint8(noisy))
% title('blurred and noisy')
% pause(0.01)

% noisy operator
d = 0.025;
if n == 128 && sigma == 1 && d == 0.025
    load noisy_op_128_0.025.mat
else      
    noisy_op = max(oper + (1-2*rand(size(oper)))*d*norm(oper(:),'inf'),0);
    noisy_op(noisy_op < d*norm(oper(:),'inf')) = 0;
    noisy_op = ndSparse(noisy_op,[n*n,n,n]);
    for i = 1:n
        for j = 1:n
            noisy_op(:,i,j) = noisy_op(:,i,j)/sum(noisy_op(:,i,j));
        end
    end
    noisy_op = ndSparse(noisy_op,[n*n,n*n]);
    noisy_op = sparse(noisy_op);
end

% operator bounds
Al = sparse(max(noisy_op - d*norm(noisy_op(:),'inf'),0));
Au = noisy_op;
Au(noisy_op > 0) = Au(noisy_op > 0) + d*norm(noisy_op(:),'inf');

% data bounds
f_l = noisy - c;
f_u = noisy + c;

params.fidelity = 'Operatorbounds';
params.regularizer = 'TV-anisotr';
params.gamma = 5;
params.lowerbounds = f_l; %lower bounds for data f^l
params.upperbounds = f_u; %upper bounds for data f^u

% exact operator
params.loweperator = oper; %lower b. operator A^l
params.upperoperator = oper; %upper b. operator A^u
[recon_exact,p_ex,g_ex] = do_minimization(noisy, params);
subplot(2,3,2);
imshow(uint8(recon_exact))
title('exact operator')
pause(0.01)

% debiasing for exact operator
params.eps = 1e-6;
debiased_exact = debiasing_2d(f_l,f_u,oper,oper,oper,noisy,recon_exact,p_ex,g_ex,params);
pause(0.01)
subplot(2,3,3);
imshow(uint8(debiased_exact))
title('exact debiased')
pause(0.01)

% wrong operator
params.loweperator = noisy_op; %lower b. operator A^l
params.upperoperator = noisy_op; %upper b. operator A^u
recon_wrong = do_minimization(noisy, params);
subplot(2,3,4);
imshow(uint8(recon_wrong))
title('wrong operator')
pause(0.01)

% interval operator
params.loweperator = Al; %lower b. operator A^l
params.upperoperator = Au; %upper b. operator A^u
[recon_int,p,g] = do_minimization(noisy, params);
subplot(2,3,5);
imshow(uint8(recon_int))
title('interval operator')
pause(0.01)

params.eps = 3e+4;
% debiasing for interval reconstruction
debiased_int = debiasing_2d(f_l,f_u,Al,Au,noisy_op,noisy,recon_int,p,g,params);
subplot(2,3,6);
imshow(uint8(debiased_int))
title('debiased')
pause(0.01)

% plots for slides
% figure
% imshow(uint8(debiased_int))
% set(gcf, 'PaperSize', [6.25 7.5]);
% set(gcf, 'PaperPositionMode', 'manual');
% set(gcf, 'PaperPosition', [0 0 6.25 7.5]);
% 
% set(gcf, 'PaperUnits', 'inches');
% set(gcf, 'PaperSize', [6.25 7.5]);
% set(gcf, 'PaperPositionMode', 'manual');
% set(gcf, 'PaperPosition', [0 0 6.25 7.5]);
% 
% set(gcf, 'renderer', 'painters');
% print(gcf, '-depsc2', 'cameraman_debiased_big_eps.eps');

PSNR_exact = psnr(recon_exact,orig,255)
PSNR_exact_debiased = psnr(debiased_exact,orig,255)
PSNR_wrong = psnr(recon_wrong,orig,255)
PSNR_int = psnr(recon_int,orig,255)
PSNR_debiased = psnr(debiased_int,orig,255)
SSIM_exact = ssim(recon_exact,orig)
SSIM_exact_debiased = ssim(debiased_exact,orig)
SSIM_wrong = ssim(recon_wrong,orig)
SSIM_int = ssim(recon_int,orig)
SSIM_debiased = ssim(debiased_int,orig)