clc
clear all
close all


load('owl_0.2.mat');

f1 = figure
imshow(uint8(orig))
print(f1,'images/owl20/orig','-deps')

f2= figure
imshow(uint8(noisy))
print(f2,'images/owl20/noisy','-deps')

f3= figure
imshow(uint8(x_tv))
print(f3,'images/owl20/tv','-deps')

f4= figure
imshow(uint8(x_tv_pwl))
print(f4,'images/owl20/tvpwl','-deps')

f5= figure
imshow(uint8(x_tvlp))
print(f5,'images/owl20/tvl2','-deps')

f6= figure
imshow(uint8(x_tvlinf))
print(f6,'images/owl20/tvlinf','-deps')

f7= figure
imshow(uint8(x_tgv2))
print(f7,'images/owl20/tgv','-deps')