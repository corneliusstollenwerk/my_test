function [param,SSIM] = param_opt_tv ( img , noise , alpha0 , alphaend )

%add gaussian noise
noisy = add_noise( img , noise );
noisy = double(noisy);
 
params.regularizer = 'TV';
ssim_neg = @(alpha) - ssim( minimization (noisy , params , alpha),img)

[param, SSIM_neg] = fminbnd( ssim_neg , alpha0 , alphaend );
SSIM = -SSIM_neg;

end