 clear all
 clc

seed = 5849774;
%seed = 6584564; %(squares,horiz)
rng(seed)

image = 'lena';
sz = 128;
type = 'Gaussian';


switch image
    case 'barbara'
        if sz == 128
            barbara128 = imread('testimages/128x128/barbara_128.tif');
            orig = double(barbara128);
        elseif sz == 256
            barbara256 = imread('testimages/256x256/barbara_256.tif');
            orig = double(barbara256);
        end
    case 'cameraman'
        if sz == 128
            cameraman128 = imread('testimages/128x128/camera_128.tif');
            orig = double(cameraman128);
        elseif sz == 256
            cameraman256 = imread('testimages/256x256/camera_256.tif');
            orig = double(cameraman256);
        end
    case 'gray'
        if sz == 128
            gray128 = imread('testimages/128x128/grayscale_128.tif');
            orig = double(gray128);
        elseif sz == 256
            gray256 = imread('testimages/256x256/grayscale_256.tif');
            orig = double(gray256(:,:,1));
        end
    case 'lena'
        if sz == 128
            lena128 = imread('testimages/128x128/lena_128.tif');
            orig = double(lena128);
        elseif sz == 256
            lena256 = imread('testimages/256x256/lena_256.tif');
            orig = double(lena256);
        end
    case 'butterfly'
        if sz == 128
            butterfly128 = imread('testimages/128x128/butterfly_128.tif');
            orig = double(butterfly128);
        elseif sz == 256
            butterfly256 = imread('testimages/256x256/butterfly_256.tif');
            orig = double(butterfly256);
        end

end



figure
subplot(2,2,1);
imshow(uint8(orig))
title('original')

%add gaussian noise
delta = 0.05;
noisy = add_noise(orig , type , delta);
noisy = double(noisy);
subplot(2,2,2);
imshow(uint8(noisy))
title('noisy image')

%fidelity term
params.fidelity = 'L2 squared';
% params.operator = 1;

%%

%denoising using TVLp
params.regularizer = 'TVLp';
params.p = 2;
switch image
    case 'barbara'
        if sz == 128
            alpha = 7.8644;
            params.beta = 0.9962;
        elseif sz == 256
            alpha = 8.2154;
            params.beta = 1.2253;
        end
    case 'butterfly'
        if sz == 128
            alpha = 6.4562;
            params.beta = 0.4840;
        elseif sz == 256
            alpha = 8.3688;
            params.beta = 1.0559;
        end
    case 'cameraman'
        if sz == 128
            alpha = 8.4816;
            params.beta = 3.0705;
        elseif sz == 256
            alpha = 6.5223;
            params.beta = 9.7184;
        end
    case 'gray'
        if sz == 128
            alpha = 64.3544;
            params.beta = 9.1003;
        elseif sz == 256
            alpha = 141.0835;
            params.beta = 23.8741;
        end
    case 'lena'
        if sz == 128
            alpha= 8.5421;
            params.beta = 1.7958;
        elseif sz == 256
            alpha = 9.2742;
            params.beta = 1.5495;
        end
end

% alpha = 7.7;
% params.beta = 2;
[x_tvlp] = minimization(noisy,params,alpha);
subplot(2,2,3)
imshow(uint8(x_tvlp))
title('TVL^2')
SSIM_tvl_2 = ssim(x_tvlp,orig)

%%

%denoising using TVLp
params.regularizer = 'TVLp';
params.p = 'inf';
switch image
    case 'barbara'
        if sz == 128
            alpha = 1;
            params.beta = 1;
        elseif sz == 256
            alpha = 1;
            params.beta = 1;
        end
    case 'butterfly'
        if sz == 128
            alpha = 1;
            params.beta = 1;
        elseif sz == 256
            alpha = 1;
            params.beta = 1;
        end
    case 'cameraman'
        if sz == 128
            alpha = 1;
            params.beta = 1;
        elseif sz == 256
            alpha = 1;
            params.beta = 1;
        end
    case 'gray'
        if sz == 128
            alpha = 1;
            params.beta = 1;
        elseif sz == 256
            alpha = 141.0835;
            params.beta = 23.8741;
        end
    case 'lena'
        if sz == 128
            alpha= 8.6939;
            params.beta = 8.8802;
        elseif sz == 256
            alpha = 10;
            params.beta = 2;
        end
end

[x_tvlp] = minimization(noisy,params,alpha);
subplot(2,2,4)
imshow(uint8(x_tvlp))
title('TVL^{\infty}')
SSIM_tvl_inf = ssim(x_tvlp,orig)