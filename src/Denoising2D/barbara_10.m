clc
clear all
close all


load('barbara_0.1.mat');

f1 = figure
imshow(uint8(orig))
print(f1,'images/barbara10/orig','-deps')

f2= figure
imshow(uint8(noisy))
print(f2,'images/barbara10/noisy','-deps')

f3= figure
imshow(uint8(x_tv))
print(f3,'images/barbara10/tv','-deps')

f4= figure
imshow(uint8(x_tv_pwl))
print(f4,'images/barbara10/tvpwl','-deps')

f5= figure
imshow(uint8(x_tvlp))
print(f5,'images/barbara10/tvl2','-deps')

f6= figure
imshow(uint8(x_tvlinf))
print(f6,'images/barbara10/tvlinf','-deps')

f7= figure
imshow(uint8(x_tgv2))
print(f7,'images/barbara10/tgv','-deps')