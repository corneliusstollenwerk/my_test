clc
clear all
close all


load('comp_tvlinf_owl.mat');

f1 = figure
imshow(uint8(orig))
print(f1,'images/comp_tvlinf_owl/orig','-deps')

f2= figure
imshow(uint8(noisy))
print(f2,'images/comp_tvlinf_owl/noisy','-deps')

f3= figure
imshow(uint8(x_tv1))
print(f3,'images/comp_tvlinf_owl/tv1','-deps')

f4= figure
imshow(uint8(x_tv2))
print(f4,'images/comp_tvlinf_owl/tv2','-deps')

f5= figure
imshow(uint8(x_tv3))
print(f5,'images/comp_tvlinf_owl/tv3','-deps')

f6= figure
imshow(uint8(x_tv_pwl))
print(f6,'images/comp_tvlinf_owl/tvpwl','-deps')