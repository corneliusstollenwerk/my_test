clc
clear all
close all


load('synthetic4_0.05.mat');

f1 = figure
imshow(uint8(orig))
print(f1,'images/synthetic405/orig','-dpdf')

f2= figure
imshow(uint8(noisy))
print(f2,'images/synthetic405/noisy','-dpdf')

f3= figure
imshow(uint8(x_tv))
print(f3,'images/synthetic405/tv','-dpdf')

f4= figure
imshow(uint8(x_tv_pwl))
print(f4,'images/synthetic405/tvpwl','-dpdf')

f5= figure
imshow(uint8(x_tvlp))
print(f5,'images/synthetic405/tvl2','-dpdf')

f6= figure
imshow(uint8(x_tvlinf))
print(f6,'images/synthetic405/tvlinf','-dpdf')

f7= figure
imshow(uint8(x_tgv2))
print(f7,'images/synthetic405/tgv','-dpdf')