clear all
close all
 clc

seed = 5849774;
%seed = 6584564; %(squares,horiz)
rng(seed)
%%

image = 'barbara';
sz = 256;
type = 'Gaussian_Bound';


switch image
    case 'barbara'
           barbara256 = imread('testimages/256x256/barbara_256.tif');
           orig = double(barbara256);
    case 'brickwall'
           brickwall256 = imread('testimages/256x256/brickwall_256.tif');
           orig = double(brickwall256(:,:,1));
    case 'cameraman'
           cameraman256 = imread('testimages/256x256/camera_256.tif');
           orig = double(cameraman256);
    case 'fish'
           fish256 = imread('testimages/256x256/fish_256.tif');
           orig = double(fish256(:,:,1));
    case 'gull'
           gull256 = imread('testimages/256x256/gull_256.tif');
           orig = double(gull256(:,:,1));
    case 'house'
           house256 = imread('testimages/256x256/house_256.tif');
           orig = double(house256(:,:,1));
    case 'butterfly'
           butterfly256 = imread('testimages/256x256/butterfly_256.tif');
           orig = double(butterfly256);
     case 'owl'
           owl256 = imread('testimages/256x256/owl_256.tif');
           orig = double(owl256);
    case 'synthetic1'
           synthetic1256 = imread('testimages/256x256/synthetic1_256.tif');
           orig = double(synthetic1256);
    case 'synthetic3'
           synthetic3256 = imread('testimages/256x256/synthetic3_256.tif');
           orig = double(synthetic3256(:,:,1));
    case 'synthetic4'
           synthetic4256 = imread('testimages/256x256/synthetic4_256.tif');
           orig = double(synthetic4256(:,:,1));
end

sg = size(orig);
D = diffopn(sg,1,'forward','neumann');
Dorig = reshape( D * orig(:) , prod(sg), 2 );

figure

%add gaussian noise
delta = 0.1; %standard deviation delta*255
noisy = add_noise(orig , type , delta);
noisy = double(noisy);

%fidelity term
params.fidelity = 'Bound';
params.bound = sz * 255 * delta;

%denoising using TV_pwL
params.regularizer = 'TV_pwL';
alpha = 1;
params.gamma = norms ( Dorig , 2 , 2 );

[x_tv_pwl1] = minimization(noisy,params,alpha);
subplot(2,2,1)
imshow(uint8(x_tv_pwl1))
title('TV_{pwL} for barbara')
SSIM_tv_pwl1 = ssim(x_tv_pwl1,orig)
SSIM_tv_pwl1_dr = ssim(x_tv_pwl1,orig,'DynamicRange',255)

%%

image = 'butterfly';
sz = 256;
type = 'Gaussian_Bound';


switch image
    case 'barbara'
           barbara256 = imread('testimages/256x256/barbara_256.tif');
           orig = double(barbara256);
    case 'brickwall'
           brickwall256 = imread('testimages/256x256/brickwall_256.tif');
           orig = double(brickwall256(:,:,1));
    case 'cameraman'
           cameraman256 = imread('testimages/256x256/camera_256.tif');
           orig = double(cameraman256);
    case 'fish'
           fish256 = imread('testimages/256x256/fish_256.tif');
           orig = double(fish256(:,:,1));
    case 'gull'
           gull256 = imread('testimages/256x256/gull_256.tif');
           orig = double(gull256(:,:,1));
    case 'house'
           house256 = imread('testimages/256x256/house_256.tif');
           orig = double(house256(:,:,1));
    case 'butterfly'
           butterfly256 = imread('testimages/256x256/butterfly_256.tif');
           orig = double(butterfly256);
     case 'owl'
           owl256 = imread('testimages/256x256/owl_256.tif');
           orig = double(owl256);
    case 'synthetic1'
           synthetic1256 = imread('testimages/256x256/synthetic1_256.tif');
           orig = double(synthetic1256);
    case 'synthetic3'
           synthetic3256 = imread('testimages/256x256/synthetic3_256.tif');
           orig = double(synthetic3256(:,:,1));
    case 'synthetic4'
           synthetic4256 = imread('testimages/256x256/synthetic4_256.tif');
           orig = double(synthetic4256(:,:,1));
end

sg = size(orig);
D = diffopn(sg,1,'forward','neumann');
Dorig = reshape( D * orig(:) , prod(sg), 2 );

%add gaussian noise
delta = 0.1; %standard deviation delta*255
noisy = add_noise(orig , type , delta);
noisy = double(noisy);

%fidelity term
params.fidelity = 'Bound';
params.bound = sz * 255 * delta;

%denoising using TV_pwL
params.regularizer = 'TV_pwL';
alpha = 1;
params.gamma = norms ( Dorig , 2 , 2 );

[x_tv_pwl2] = minimization(noisy,params,alpha);
subplot(2,2,2)
imshow(uint8(x_tv_pwl2))
title('TV_{pwL} for butterfly')
SSIM_tv_pwl2 = ssim(x_tv_pwl2,orig)
SSIM_tv_pwl2_dr = ssim(x_tv_pwl2,orig,'DynamicRange',255)

%%

image = 'fish';
sz = 256;
type = 'Gaussian_Bound';


switch image
    case 'barbara'
           barbara256 = imread('testimages/256x256/barbara_256.tif');
           orig = double(barbara256);
    case 'brickwall'
           brickwall256 = imread('testimages/256x256/brickwall_256.tif');
           orig = double(brickwall256(:,:,1));
    case 'cameraman'
           cameraman256 = imread('testimages/256x256/camera_256.tif');
           orig = double(cameraman256);
    case 'fish'
           fish256 = imread('testimages/256x256/fish_256.tif');
           orig = double(fish256(:,:,1));
    case 'gull'
           gull256 = imread('testimages/256x256/gull_256.tif');
           orig = double(gull256(:,:,1));
    case 'house'
           house256 = imread('testimages/256x256/house_256.tif');
           orig = double(house256(:,:,1));
    case 'butterfly'
           butterfly256 = imread('testimages/256x256/butterfly_256.tif');
           orig = double(butterfly256);
     case 'owl'
           owl256 = imread('testimages/256x256/owl_256.tif');
           orig = double(owl256);
    case 'synthetic1'
           synthetic1256 = imread('testimages/256x256/synthetic1_256.tif');
           orig = double(synthetic1256);
    case 'synthetic3'
           synthetic3256 = imread('testimages/256x256/synthetic3_256.tif');
           orig = double(synthetic3256(:,:,1));
    case 'synthetic4'
           synthetic4256 = imread('testimages/256x256/synthetic4_256.tif');
           orig = double(synthetic4256(:,:,1));
end

sg = size(orig);
D = diffopn(sg,1,'forward','neumann');
Dorig = reshape( D * orig(:) , prod(sg), 2 );

%add gaussian noise
delta = 0.1; %standard deviation delta*255
noisy = add_noise(orig , type , delta);
noisy = double(noisy);

%fidelity term
params.fidelity = 'Bound';
params.bound = sz * 255 * delta;

%denoising using TV_pwL
params.regularizer = 'TV_pwL';
alpha = 1;
params.gamma = norms ( Dorig , 2 , 2 );

[x_tv_pwl3] = minimization(noisy,params,alpha);
subplot(2,2,3)
imshow(uint8(x_tv_pwl3))
title('TV_{pwL} for fish')
SSIM_tv_pwl3 = ssim(x_tv_pwl3,orig)
SSIM_tv_pwl3_dr = ssim(x_tv_pwl3,orig,'DynamicRange',255)

%%

image = 'owl';
sz = 256;
type = 'Gaussian_Bound';


switch image
    case 'barbara'
           barbara256 = imread('testimages/256x256/barbara_256.tif');
           orig = double(barbara256);
    case 'brickwall'
           brickwall256 = imread('testimages/256x256/brickwall_256.tif');
           orig = double(brickwall256(:,:,1));
    case 'cameraman'
           cameraman256 = imread('testimages/256x256/camera_256.tif');
           orig = double(cameraman256);
    case 'fish'
           fish256 = imread('testimages/256x256/fish_256.tif');
           orig = double(fish256(:,:,1));
    case 'gull'
           gull256 = imread('testimages/256x256/gull_256.tif');
           orig = double(gull256(:,:,1));
    case 'house'
           house256 = imread('testimages/256x256/house_256.tif');
           orig = double(house256(:,:,1));
    case 'butterfly'
           butterfly256 = imread('testimages/256x256/butterfly_256.tif');
           orig = double(butterfly256);
     case 'owl'
           owl256 = imread('testimages/256x256/owl_256.tif');
           orig = double(owl256);
    case 'synthetic1'
           synthetic1256 = imread('testimages/256x256/synthetic1_256.tif');
           orig = double(synthetic1256);
    case 'synthetic3'
           synthetic3256 = imread('testimages/256x256/synthetic3_256.tif');
           orig = double(synthetic3256(:,:,1));
    case 'synthetic4'
           synthetic4256 = imread('testimages/256x256/synthetic4_256.tif');
           orig = double(synthetic4256(:,:,1));
end

sg = size(orig);
D = diffopn(sg,1,'forward','neumann');
Dorig = reshape( D * orig(:) , prod(sg), 2 );

%add gaussian noise
delta = 0.1; %standard deviation delta*255
noisy = add_noise(orig , type , delta);
noisy = double(noisy);

%fidelity term
params.fidelity = 'Bound';
params.bound = sz * 255 * delta;

%denoising using TV_pwL
params.regularizer = 'TV_pwL';
alpha = 1;
params.gamma = norms ( Dorig , 2 , 2 );

[x_tv_pwl4] = minimization(noisy,params,alpha);
subplot(2,2,4)
imshow(uint8(x_tv_pwl4))
title('TV_{pwL} for owl')
SSIM_tv_pwl4 = ssim(x_tv_pwl4,orig)
SSIM_tv_pwl4_dr = ssim(x_tv_pwl4,orig,'DynamicRange',255)