%  clear all
%  clc

seed = 5849724;
rng(seed)

sz = 128;
type = 'Uniform';
params.fidelity = 'Indicator';

if sz == 128
        owl128 = imread('testimages/128x128/owl_128.tif');
        orig = double(owl128);
    elseif sz == 256
        owl256 = imread('testimages/256x256/owl_256.tif');
        orig = double(owl256);
end

%add gaussian noise
delta = 0.25;
params.delta = delta;
noisy = add_noise(orig , type , delta);
noisy = double(noisy);

param = (210:10:300)';

%denoising using TGV2
params.regularizer = 'TGV2';
alpha = 1;

for i = 1:10
    params.beta = param(i);
    u_tv_pwl = minimization(noisy,params,alpha);
    param(i,2) = ssim(u_tv_pwl,orig);
end
