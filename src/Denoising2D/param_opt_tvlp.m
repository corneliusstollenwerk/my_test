function [param , SSIM] = param_opt_tvlp ( img , noise , alpha0 , p )

%add gaussian noise
noisy = add_noise( img , noise );
noisy = double(noisy);

params.regularizer = 'TVLp';
params.p = p;

ssim_neg = @(paramreg) - ssim( minimization2 (noisy , params , paramreg(1) , paramreg(2)),img)

A = -eye(2);
b = [0,0];

[param, SSIM_neg] = fmincon( ssim_neg , alpha0 , A , b);
SSIM = - SSIM_neg;


end
 

