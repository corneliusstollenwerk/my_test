function [ x, p, g ] = minimization_tv_pwl( img, params, alpha )
%DO_MINIMIZATION Summary of this function goes here
%   Detailed explanation goes here

% Set default values
if nargin < 3
    if isfield(params, 'alpha')
        alpha = params.alpha;
    else
        alpha = 1;
    end
end

fidelity = params.fidelity;
s = size(img);
n_ghosts = 0; % Number of ghost cells at each boundary
sg = s + (2*n_ghosts); % Size including ghost cells


cvx_begin quiet
    
    cvx_solver Mosek
    cvx_precision best

    % Set up variables
    variables u(sg)             % including ghost cells
    variables g(prod(sg),2)
    
    u(:) >= 0;
    
    data = u(1+n_ghosts:end-n_ghosts,...
             1+n_ghosts:end-n_ghosts);  % excluding ghost cells
         
    % generate differential operators
    D = diffopn(sg,1,'forward','neumann');
    Du = reshape( D * u(:) , prod(sg), 2 );
    
    % Define fidelity term
    switch fidelity
        case 'L2 squared'
            fid = 0.5 * sum((data(:) - img(:)).^2);
        case 'Indicator'
            delta = params.delta;
            fid = 0;
            %fid = max(abs(data(:)-img(:)));
            img(:) - 0.5 * 255 * delta * ones( prod(sg),1 ) <= data(:) ...
                <= img(:) + 0.5 * 255 * delta * ones( prod(sg),1 );      
    end
    
    % Define regularization term
    if alpha == 0
        reg = 0;
    else
                              
        gamma = params.gamma;
        gamma <= g <= gamma;
        reg = sum( norms( Du-g , 1 , 2 ) );
                                     
    end
    
    minimize fid + alpha * reg

cvx_end

if ~strcmp(cvx_status, 'Solved')
    warning('\tProblem not solved.\tCVX Status: %s', cvx_status);
end

if not(exist('g'))
    g = [];
end

x = full(data);

end