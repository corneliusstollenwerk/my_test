clear all
 clc

seed = 5849774;
%seed = 6584564; %(squares,horiz)
rng(seed)

image = 'owl';
sz = 256;
type = 'Gaussian_Bound';


switch image
    case 'barbara'
           barbara256 = imread('testimages/256x256/barbara_256.tif');
           orig = double(barbara256);
    case 'brickwall'
           brickwall256 = imread('testimages/256x256/brickwall_256.tif');
           orig = double(brickwall256(:,:,1));
    case 'cameraman'
           cameraman256 = imread('testimages/256x256/camera_256.tif');
           orig = double(cameraman256);
    case 'fish'
           fish256 = imread('testimages/256x256/fish_256.tif');
           orig = double(fish256(:,:,1));
    case 'gull'
           gull256 = imread('testimages/256x256/gull_256.tif');
           orig = double(gull256(:,:,1));
    case 'house'
           house256 = imread('testimages/256x256/house_256.tif');
           orig = double(house256(:,:,1));
    case 'butterfly'
           butterfly256 = imread('testimages/256x256/butterfly_256.tif');
           orig = double(butterfly256);
     case 'owl'
           owl256 = imread('testimages/256x256/owl_256.tif');
           orig = double(owl256);
    case 'synthetic1'
           synthetic1256 = imread('testimages/256x256/synthetic1_256.tif');
           orig = double(synthetic1256);
    case 'synthetic3'
           synthetic3256 = imread('testimages/256x256/synthetic3_256.tif');
           orig = double(synthetic3256(:,:,1));
    case 'synthetic4'
           synthetic4256 = imread('testimages/256x256/synthetic4_256.tif');
           orig = double(synthetic4256(:,:,1));
end



figure
subplot(2,3,1);
imshow(uint8(orig))
title('original')

%add gaussian noise
if isequal(image,'owl')
    delta = 0.15; %standard deviation delta*255
else
    delta = 0.1;
end
noisy = add_noise(orig , type , delta);
noisy = double(noisy);
subplot(2,3,2);
imshow(uint8(noisy))
title('noisy image')

%fidelity term
params.fidelity = 'Bound';
params.bound = sz * 255 * delta;


%%
%denoising using TV_pwL
params.regularizer = 'TV_pwL';
alpha = 1;
switch image
    case 'barbara'
        if delta == 0.05
            params.gamma = 61.8161;
        elseif delta == 0.1
            params.gamma = 29.8687;
        elseif delta == 0.25;
            params.gamma =16.4029;
        end
    case 'cameraman'
        if delta == 0.05
            params.gamma = 73.4641;
        elseif delta == 0.25
            params.gamma = 1;
        end
     case 'fish'
        if delta == 0.05
            params.gamma = 76.8930;
        elseif delta == 0.1
            params.gamma = 4.1111;%45.2079;
        elseif delta == 0.15
            params.gamma = 39.9483;
        elseif delta == 0.25
            params.gamma = 1;
        end
    case 'gull'
        if delta == 0.05
           params.gamma = 37.9741;
        elseif delta == 0.25
            params.gamma = 1;
        end
    case 'butterfly'
        if delta == 0.05
            params.gamma = 39.1323;
        elseif delta == 0.1
            params.gamma = 24.6667;
        elseif delta == 0.25
            params.gamma = 1;
        end
     case 'owl'
         if delta == 0.05
             params.gamma = 40.8771;
         elseif delta == 0.15
             params.gamma = 34.4444;
         elseif delta == 0.2
             params.gamma = 25.4136;
         end
end

[x_tv_pwl] = minimization(noisy,params,alpha);
subplot(2,3,3)
imshow(uint8(x_tv_pwl))
title('TV_{pwL}')
SSIM_tv_pwl = ssim(x_tv_pwl,orig)


%%

%denoising using TVL_inf
params.regularizer = 'TVLp';
params.p = 2;
alpha = 1;
switch image
    case 'barbara'
        params.beta = 1;
    case 'brickwall'
        params.beta = 1;
    case 'cameraman'
        params.beta = 1;
    case 'gray'
        params.beta = 1;
    case 'gull'
        params.beta = 1;
    case 'fish'
        params.beta = 1;
    case 'butterfly'
        params.beta = 1;
    case 'owl'
        params.beta = 0;
    case 'synthetic1'
        params.beta = 1;
end
[x_tv1] = minimization(noisy,params,alpha);
subplot(2,3,4)
imshow(uint8(x_tv1))
title('TVL^2,\beta=0')
SSIM_tv1 = ssim(x_tv1,orig)

%%
%denoising using TV
params.regularizer = 'TVLp';
params.p = 2;
alpha = 1;
switch image
    case 'barbara'
        params.beta = 1;
    case 'brickwall'
        params.beta = 1;
    case 'cameraman'
        params.beta = 1;
    case 'gray'
        params.beta = 1;
    case 'gull'
        params.beta = 1;
    case 'fish'
        params.beta = 1;
    case 'butterfly'
        params.beta = 140;
    case 'owl'
        params.beta = 27.7778;
    case 'synthetic1'
        params.beta = 1;
end
[x_tv2] = minimization(noisy,params,alpha);
subplot(2,3,5)
imshow(uint8(x_tv2))
title('TVL^2,\beta=27.7778')
SSIM_tv2 = ssim(x_tv2,orig)

%%
%denoising using TV
params.regularizer = 'TVLp';
params.p = 2;
alpha = 1;
switch image
    case 'barbara'
        params.beta = 1;
    case 'brickwall'
        params.beta = 1;
    case 'cameraman'
        params.beta = 1;
    case 'gray'
        params.beta = 1;
    case 'gull'
        params.beta = 1;
    case 'fish'
        params.beta = 1;
    case 'butterfly'
        params.beta = 166.4444;
    case 'owl'
        params.beta = 500;
    case 'synthetic1'
        params.beta = 1;
end
[x_tv3] = minimization(noisy,params,alpha);
subplot(2,3,6)
imshow(uint8(x_tv3))
title('TVL^2,\beta=500')
SSIM_tv3 = ssim(x_tv3,orig)