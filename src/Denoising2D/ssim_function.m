function [SSIM] = ssim_function ( img , params , alpha , noise)

%add gaussian noise
[noisy] = add_noise(img,noise);
noisy = double(noisy);

[ x ] = minimization ( noisy , params , alpha );
SSIM = ssim ( x , img )