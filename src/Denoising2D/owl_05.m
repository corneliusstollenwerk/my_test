clc
clear all
close all


load('owl_0.05.mat');

f1 = figure
imshow(uint8(orig))
print(f1,'images/owl05/orig','-dpdf')

f2= figure
imshow(uint8(noisy))
print(f2,'images/owl05/noisy','-dpdf')

f3= figure
imshow(uint8(x_tv))
print(f3,'images/owl05/tv','-dpdf')

f4= figure
imshow(uint8(x_tv_pwl))
print(f4,'images/owl05/tvpwl','-dpdf')

f5= figure
imshow(uint8(x_tvlp))
print(f5,'images/owl05/tvl2','-dpdf')

f6= figure
imshow(uint8(x_tvlinf))
print(f6,'images/owl05/tvlinf','-dpdf')

f7= figure
imshow(uint8(x_tgv2))
print(f7,'images/owl05/tgv','-dpdf')