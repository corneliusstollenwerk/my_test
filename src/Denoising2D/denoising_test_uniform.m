clear all
clc

seed = 5849774;
%seed = 6584564; %(squares,horiz)
rng(seed)

image = 'owl';
sz = 256;
type = 'Uniform';


switch image
    case 'barbara'
           barbara256 = imread('testimages/256x256/barbara_256.tif');
           orig = double(barbara256);
    case 'cameraman'
           cameraman256 = imread('testimages/256x256/camera_256.tif');
           orig = double(cameraman256);
    case 'gull'
           gull256 = imread('testimages/256x256/gull_256.tif');
           orig = double(gull256(:,:,1));
    case 'house'
           house256 = imread('testimages/256x256/house_256.tif');
           orig = double(house256(:,:,1));
    case 'butterfly'
           butterfly256 = imread('testimages/256x256/butterfly_256.tif');
           orig = double(butterfly256);
    case 'owl'
           owl256 = imread('testimages/256x256/owl_256.tif');
           orig = double(owl256);
    case 'synthetic1'
           synthetic1256 = imread('testimages/256x256/synthetic1_256.tif');
           orig = double(synthetic1256);
end



figure
subplot(2,3,1);
imshow(uint8(orig))
title('original')

%add uniform noise
params.delta = 0.25;
delta = params.delta;
noisy = add_noise(orig , type , delta);
noisy = double(noisy);
subplot(2,3,2);
imshow(uint8(noisy))
title('noisy image')

%fidelity term
params.fidelity = 'Indicator';
% params.operator = 1;

%%

%denoising using TV
params.regularizer = 'TV';
alpha = 1;

[x_tv] = minimization(noisy,params,alpha);
subplot(2,3,3)
imshow(uint8(x_tv))
title('TV')
SSIM_tv = ssim(x_tv,orig)

%%
%denoising using TV_pwL
params.regularizer = 'TV_pwL';
alpha =1;
switch image
    case 'barbara'
           params.gamma = 26.3734;
    case 'cameraman'
           params.gamma = 9.7600;
    case 'gull'
           params.gamma = 7.8707;
    case 'house'
           params.gamma = 50.1161;
    case 'butterfly'
           params.gamma = 21.6240;
    case 'owl'
           params.gamma = 49.2775;
    case 'synthetic1'
           params.gamma = 0.9683;
end

[x_tv_pwl] = minimization(noisy,params,alpha);
subplot(2,3,4)
imshow(uint8(x_tv_pwl))
title('TV_{pwL}')
SSIM_tv_pwl = ssim(x_tv_pwl,orig)

%%
%denoising using TVLp
params.regularizer = 'TVLp';
alpha = 1;
params.p = 2;
switch image
    case 'barbara'
           params.beta = 0.0875;
    case 'butterfly'
           params.beta = 0.0031;
    case 'cameraman'
           params.beta = 0.2193;
    case 'gull'
           params.beta = 0.0223;
    case 'house'
           params.beta = 0.0045;
    case 'owl'
           params.beta = 0.0036;
    case 'synthetic1'
           params.beta = 0.2389;
end

[x_tvlp] = minimization(noisy,params,alpha);
subplot(2,3,5)
imshow(uint8(x_tvlp))
title('TVL^p')
SSIM_tvlp = ssim(x_tvlp,orig)

%%

%denoising using TGV2
params.regularizer = 'TGV2';
alpha = 1;

switch image
    case 'barbara'
           params.beta = 219.0539;
    case 'butterfly'
           params.beta = 0.0183;
    case 'cameraman'
           params.beta = 7.6622;
    case 'gull'
           params.beta = 460.9697;
    case 'house'
           params.beta = 355.6335;
    case 'owl'
           params.beta = 77.3277;
    case 'synthetic1'
            params.beta = 309.0281;
end

[x_tgv2] = minimization(noisy,params,alpha);
subplot(2,3,6)
imshow(uint8(x_tgv2))
title('TGV^2')
SSIM_tgv2 = ssim(x_tgv2,orig)