clc
clear all
close all


load('owl_0.2.comp.mat');

f1 = figure
imshow(uint8(orig))
print(f1,'images/owl20comp/orig','-deps')

f2= figure
imshow(uint8(noisy))
print(f2,'images/owl20comp/noisy','-deps')

f3= figure
imshow(uint8(x_tv_pwl1))
print(f3,'images/owl20comp/tvpwlmax','-deps')

f4= figure
imshow(uint8(x_tv_pwl2))
print(f4,'images/owl20comp/tvpwlmean','-deps')

f5= figure
imshow(uint8(x_tv_pwl3))
print(f5,'images/owl20comp/tvpwl5','-deps')