clc
clear all
close all


load('owl_0.15.mat');

f1 = figure
imshow(uint8(orig))
print(f1,'images/owl15/orig','-deps')

f2= figure
imshow(uint8(noisy))
print(f2,'images/owl15/noisy','-deps')

f3= figure
imshow(uint8(x_tv))
print(f3,'images/owl15/tv','-deps')

f4= figure
imshow(uint8(x_tv_pwl))
print(f4,'images/owl15/tvpwl','-deps')

f5= figure
imshow(uint8(x_tvlp))
print(f5,'images/owl15/tvl2','-deps')

f6= figure
imshow(uint8(x_tvlinf))
print(f6,'images/owl15/tvlinf','-deps')

f7= figure
imshow(uint8(x_tgv2))
print(f7,'images/owl15/tgv','-deps')