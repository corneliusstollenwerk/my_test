clear all
clc

seed = 5849774;
%seed = 6584564; %(squares,horiz)
rng(seed)

image = 'barbara';
sz = 128;
type = 'Uniform';


switch image
    case 'barbara'
        if sz == 128
            barbara128 = imread('testimages/128x128/barbara_128.tif');
            orig = double(barbara128);
        elseif sz == 256
            barbara256 = imread('testimages/256x256/barbara_256.tif');
            orig = double(barbara256);
        end
    case 'brickwall'
        if sz == 128
            brickwall128 = imread('testimages/128x128/brickwall_128.tif');
            orig = double(brickwall128(:,:,1));
        elseif sz == 256
            brickwall256 = imread('testimages/256x256/brickwall_256.tif');
            orig = double(brickwall256(:,:,1));
        end
    case 'cameraman'
        if sz == 128
            cameraman128 = imread('testimages/128x128/camera_128.tif');
            orig = double(cameraman128);
        elseif sz == 256
            cameraman256 = imread('testimages/256x256/camera_256.tif');
            orig = double(cameraman256);
        end
    case 'gray'
        if sz == 128
            gray128 = imread('testimages/128x128/grayscale_128.tif');
            orig = double(gray128);
        elseif sz == 256
            gray256 = imread('testimages/256x256/grayscale_256.tif');
            orig = double(gray256(:,:,1));
        end
    case 'gull'
        if sz == 128
            gull128 = imread('testimages/128x128/gull_128.tif');
            orig = double(gull128(:,:,1));
        elseif sz == 256
            gull256 = imread('testimages/256x256/gull_256.tif');
            orig = double(gull256(:,:,1));
        end
    case 'house'
        if sz == 128
            house128 = imread('testimages/128x128/house_128.tif');
            orig = double(house128(:,:,1));
        elseif sz == 256
            house256 = imread('testimages/256x256/house_256.tif');
            orig = double(house256(:,:,1));
        end
    case 'lena'
        if sz == 128
            lena128 = imread('testimages/128x128/lena_128.tif');
            orig = double(lena128);
        elseif sz == 256
            lena256 = imread('testimages/256x256/lena_256.tif');
            orig = double(lena256);
        end
    case 'butterfly'
        if sz == 128
            butterfly128 = imread('testimages/128x128/butterfly_128.tif');
            orig = double(butterfly128);
        elseif sz == 256
            butterfly256 = imread('testimages/256x256/butterfly_256.tif');
            orig = double(butterfly256);
        end
    case 'owl'
        if sz == 128
            owl128 = imread('testimages/128x128/owl_128.tif');
            orig = double(owl128);
        elseif sz == 256
            owl256 = imread('testimages/256x256/owl_256.tif');
            orig = double(owl256);
        end
    case 'synthetic1'
        if sz == 128
            synthetic1128 = imread('testimages/128x128/synthetic1_128.tif');
            orig = double(synthetic1128);
        elseif sz == 256
            synthetic1256 = imread('testimages/256x256/synthetic1_256.tif');
            orig = double(synthetic1256);
        end
     case 'synthetic2'
        if sz == 128
            synthetic2128 = imread('testimages/128x128/synthetic2_128.tif');
            orig = double(synthetic2128);
        elseif sz == 256
            synthetic2256 = imread('testimages/256x256/synthetic2_256.tif');
            orig = double(synthetic2256);
        end
end



figure
subplot(2,3,1);
imshow(uint8(orig))
title('original')

%add uniform noise
params.delta = 0.25;
delta = params.delta;
noisy = add_noise(orig , type , delta);
noisy = double(noisy);
subplot(2,3,2);
imshow(uint8(noisy))
title('noisy image')

%fidelity term
params.fidelity = 'Indicator';
% params.operator = 1;

%%

%denoising using TV
params.regularizer = 'TV';
alpha = 1;

[x_tv] = minimization(noisy,params,alpha);
subplot(2,3,3)
imshow(uint8(x_tv))
title('TV')
SSIM_tv = ssim(x_tv,orig)

%%
%denoising using TV_pwL
params.regularizer = 'TV_pwL';
alpha =1;
switch image
    case 'barbara'
        if sz == 128
            %params.gamma = 28.5118;
            params.gamma = 42.6559;
        elseif sz == 256
            %params.gamma = 7.0109;
            params.gamma =35.6896; %28.2016;
        end
    case 'brickwall'
        if sz == 128
            params.gamma = 89.2261;
        elseif sz == 256
            params.gamma = 74.5478;
        end
    case 'cameraman'
        if sz == 128
            %params.gamma = 4.4129;
            params.gamma = 4.3504;
        elseif sz == 256
            %params.gamma = 7.2246;
            params.gamma = 9.4272;
        end
    case 'gray'
        if sz == 128
            %params.gamma = 2.3660;
            params.gamma = 1.8373;
        elseif sz == 256
            %params.gamma = 1.1503;
            params.gamma = 0.9731;
        end
    case 'gull'
        if sz == 128
            params.gamma = 0.2833;
        elseif sz == 256
            params.gamma = 7.4910;
        end
    case 'house'
        if sz == 128
            params.gamma = 43.9104;
        elseif sz == 256
            params.gamma = 52.5707;
        end
    case 'lena'
        if sz == 128
            %params.gamma = 9.1843;
            params.gamma = 30.9055;
        elseif sz == 256
            %params.gamma = 7.0109;
            params.gamma = 9.5416;
        end
    case 'butterfly'
        if sz == 128
            %params.gamma = 28.5757;
            params.gamma = 42.5958;
        elseif sz == 256
            %params.gamma = 13.9699;
            params.gamma = 30.7332;%19.9337;
        end
     case 'owl'
        if sz == 128
            params.gamma = 84.6281;%71.6602;
        elseif sz == 256
            params.gamma = 67.7444;%54.2101;
        end
     case 'synthetic1'
        if sz == 128
            params.gamma = 1.9785;
        elseif sz == 256
            params.gamma = 0.9657;
        end
     case 'synthetic2'
        if sz == 128
            params.gamma = 15.1976;
        elseif sz == 256
            params.gamma = 7.7308;
        end
end

[x_tv_pwl] = minimization(noisy,params,alpha);
subplot(2,3,4)
imshow(uint8(x_tv_pwl))
title('TV_{pwL}')
SSIM_tv_pwl = ssim(x_tv_pwl,orig)

%%
%denoising using TVLp
params.regularizer = 'TVLp';
alpha = 1;
params.p = 2;
switch image
    case 'barbara'
        if sz == 128
            params.beta = 13.3327;
        elseif sz == 256
            params.beta = 19.3612;
        end
    case 'brickwall'
        if sz == 128
            params.beta = 84.8135;
        elseif sz == 256
            params.beta = 52.5550;
        end
    case 'butterfly'
        if sz == 128
            params.beta = 16.0478;
        elseif sz == 256
            params.beta = 16.5077;
        end
    case 'cameraman'
        if sz == 128
            params.beta = 0.5000;
        elseif sz == 256
            params.beta = 17.0904;
        end
    case 'gray'
        if sz == 128
            params.beta = 0.2920;
        elseif sz == 256
            params.beta = 0.1696;
        end
    case 'gull'
        if sz == 128
            params.beta = 17.4471;
        elseif sz == 256
            params.beta = 72.8678;
        end
    case 'house'
        if sz == 128
            params.beta = 47.4783;
        elseif sz == 256
            params.beta = 75.6436;
        end
    case 'lena'
        if sz == 128
            params.beta = 15.9691;
        elseif sz == 256
            params.beta = 22.5090;
        end
    case 'owl'
        if sz == 128
            params.beta = 70.2214;
        elseif sz == 256
            params.beta = 77.4153;
        end
    case 'synthetic1'
        if sz == 128
            params.beta = 95.4759;
        elseif sz == 256
            params.beta = 99.2341;
        end
    case 'synthetic2'
        if sz == 128
            params.beta = 19.3093;
        elseif sz == 256
            params.beta = 130.2730;
        end
end

[x_tvlp] = minimization(noisy,params,alpha);
subplot(2,3,5)
imshow(uint8(x_tvlp))
title('TVL^p')
SSIM_tvlp = ssim(x_tvlp,orig)

%%

%denoising using TGV2
params.regularizer = 'TGV2';
alpha = 1;

switch image
    case 'barbara'
        if sz == 128
            params.beta = 8.3150;
        elseif sz == 256
            params.beta = 0.1529;
        end
    case 'brickwall'
        if sz == 128
            params.beta = 84.3685;
        elseif sz == 256
            params.beta = 37.5005;
        end
    case 'butterfly'
        if sz == 128
            params.beta = 15.1986;
        elseif sz == 256
            params.beta = 12.4525;
        end
    case 'cameraman'
        if sz == 128
            params.beta = 3.5081;
        elseif sz == 256
            params.beta = 3.9815;
        end
    case 'gray'
        if sz == 128
            params.beta = 10.3288;
        elseif sz == 256
            params.beta = 6.0842;
        end
    case 'gull'
        if sz == 128
            params.beta = 6.7210;
        elseif sz == 256
            params.beta = 11.3568;
        end
    case 'house'
        if sz == 128
            params.beta = 88.9956;
        elseif sz == 256
            params.beta = 29.8778;
        end
    case 'lena'
        if sz == 128
            params.beta = 17.0901;
        elseif sz == 256
            params.beta = 0.1936;
        end
    case 'owl'
        if sz == 128
            params.beta = 1.4545e+03;%89.9179;
        elseif sz == 256
            params.beta = 13.9539;
        end
    case 'synthetic1'
        if sz == 128
            params.beta = 86.1249;
        elseif sz == 256
            params.beta = 1;
        end
    case 'synthetic2'
        if sz == 128
            params.beta = 1.7086;
        elseif sz == 256
            params.beta = 2.3868;
        end
end

[x_tgv2] = minimization(noisy,params,alpha);
subplot(2,3,6)
imshow(uint8(x_tgv2))
title('TGV^2')
SSIM_tgv2 = ssim(x_tgv2,orig);