clear all
clc

seed = 5842774;
%seed = 6584564; %(squares,horiz)
rng(seed)

gray128 = imread('testimages/128x128/grayscale_128.tif');


orig = double(gray128);
figure
subplot(1,3,1);
imshow(uint8(orig))
title('original')

s = size(orig);

D = diffopn(s,1,'forward','neumann');
Du = reshape( D * orig(:) , prod(s), 2 );

D2 = diffopn(prod(s), 2, 'forward', 'neumann');
D2u = reshape( D2*Du(:) , prod(s) , 2 );

xx = [1:s(1)];
subplot(1,3,3)
plot(xx , D2u(1:s(1),1) ,xx, D2u(1:s(1),2),'LineWidth',1.5)
title('Differential of first row and first column of the image')
axis([0 128 -3 3])

subplot(1,3,2)
plot( xx , orig(:,1), xx , orig(1,:),'LineWidth',1.5)
title('First row and first column of the image')
axis([0 128 0 256])