clear all
 clc

seed = 5849774;
%seed = 6584564; %(squares,horiz)
rng(seed)

image = 'barbara';
sz = 256;
type = 'Gaussian_Bound';


switch image
    case 'barbara'
           barbara256 = imread('testimages/256x256/barbara_256.tif');
           orig = double(barbara256);
    case 'brickwall'
           brickwall256 = imread('testimages/256x256/brickwall_256.tif');
           orig = double(brickwall256(:,:,1));
    case 'cameraman'
           cameraman256 = imread('testimages/256x256/camera_256.tif');
           orig = double(cameraman256);
    case 'fish'
           fish256 = imread('testimages/256x256/fish_256.tif');
           orig = double(fish256(:,:,1));
    case 'gull'
           gull256 = imread('testimages/256x256/gull_256.tif');
           orig = double(gull256(:,:,1));
    case 'house'
           house256 = imread('testimages/256x256/house_256.tif');
           orig = double(house256(:,:,1));
    case 'butterfly'
           butterfly256 = imread('testimages/256x256/butterfly_256.tif');
           orig = double(butterfly256);
     case 'owl'
           owl256 = imread('testimages/256x256/owl_256.tif');
           orig = double(owl256);
    case 'synthetic1'
           synthetic1256 = imread('testimages/256x256/synthetic1_256.tif');
           orig = double(synthetic1256);
    case 'synthetic3'
           synthetic3256 = imread('testimages/256x256/synthetic3_256.tif');
           orig = double(synthetic3256(:,:,1));
    case 'synthetic4'
           synthetic4256 = imread('testimages/256x256/synthetic4_256.tif');
           orig = double(synthetic4256(:,:,1));
end



figure
subplot(3,3,1);
imshow(uint8(orig))
title('original')

%add gaussian noise
delta = 0.1; %standard deviation delta*255
noisy = add_noise(orig , type , delta);
noisy = double(noisy);
subplot(3,3,2);
imshow(uint8(noisy))
title('noisy image')
SSIM_noisy_dr = ssim(noisy,orig,'DynamicRange',255)

%fidelity term
params.fidelity = 'Bound';
params.bound = sz * 255 * delta;
%%

%denoising using TV
params.regularizer = 'TV';
alpha = 1;

[x_tv] = minimization(noisy,params,alpha);
subplot(3,3,3)
imshow(uint8(x_tv))
title('TV')
SSIM_tv = ssim(x_tv,orig)
SSIM_tv_dr = ssim(x_tv,orig,'DynamicRange',255)

%%
%denoising using TV_pwL
params.regularizer = 'TV_pwL';
alpha = 1;
switch image
    case 'barbara'
        if delta == 0.05
            params.gamma = 61.8161;
        elseif delta == 0.1
            params.gamma =  1.2222;%2.3333;%29.8687;
        elseif delta == 0.25;
            params.gamma =16.4029;%
        end
    case 'brickwall'
        if delta == 0.05
           params.gamma = 54.7790;
        elseif delta == 0.25
            params.gamma = 1;
        end
    case 'cameraman'
        if delta == 0.05
            params.gamma = 73.4641;
        elseif delta == 0.25
            params.gamma = 1;
        end
    case 'fish'
        if delta == 0.05
            params.gamma = 76.8930;
        elseif delta == 0.1
            params.gamma = 5;%0.0022;%4.1111;%45.2079;
        elseif delta == 0.15
            params.gamma = 39.9483;
        elseif delta == 0.25
            params.gamma = 1;
        end
    case 'gull'
        if delta == 0.05
           params.gamma = 37.9741;
        elseif delta == 0.25
            params.gamma = 1;
        end
    case 'house'
        if delta == 0.05
            params.gamma = 50.2410;
        elseif delta == 0.25
            params.gamma = 1;
        end
    case 'butterfly'
        if delta == 0.05
            params.gamma = 39.1323;
        elseif delta == 0.1
            params.gamma = 2.3333;%24.6667;%24.6831;
        elseif delta == 0.25
            params.gamma = 1;
        end
     case 'owl'
         if delta == 0.05
             params.gamma = 40.8771;
         elseif delta == 0.1
             params.gamma = 111.4222;
         elseif delta == 0.15
             params.gamma = 34.4444;%33.9544;
         elseif delta == 0.2
             params.gamma = 25.4136;
         end
    case 'synthetic1'
        if delta == 0.05
           params.gamma = 1; %35.8134;
        elseif delta == 0.1
            params.gamma= 2;%0.9822;%1;%18.8418;
        elseif delta == 0.25
            params.gamma = 1;
        end
    case 'synthetic3'
        if delta == 0.05
           params.gamma = 0.1;%87.3807;
        elseif delta == 0.1
            params.gamma = 0;%62.9863;
        elseif delta == 0.25
            params.gamma = 1;
        end
    case 'synthetic4'
        if delta == 0.05
           params.gamma = 99.9999;
        elseif delta == 0.25
            params.gamma = 1;
        end
end

[x_tv_pwl] = minimization(noisy,params,alpha);
subplot(3,3,4)
imshow(uint8(x_tv_pwl))
title('TV_{pwL}')
SSIM_tv_pwl = ssim(x_tv_pwl,orig)
SSIM_tv_pwl_dr = ssim(x_tv_pwl,orig,'DynamicRange',255)

%%

%denoising using TVLp for p=2
params.regularizer = 'TVLp';
params.p = 2;
alpha = 1;
switch image
    case 'barbara'
        if delta == 0.05
            params.beta = 188.5459;
        elseif delta == 0.1
            params.beta = 199.3333;%179.3333;%179.2785;
        elseif delta == 0.25
            params.beta = 160.2380;%
        end
    case 'brickwall'
        if delta == 0.05
           params.beta = 340.9900;
        elseif delta == 0.25
            params.beta = 1;
        end
    case 'butterfly'
        if delta == 0.05
            params.beta = 185.14931;
        elseif delta == 0.1
            params.beta = 186.6667;%166.4444;%166.4492;
        elseif delta == 0.25
            params.beta = 1;
        end
    case 'cameraman'
        if delta == 0.05
           params.beta = 192.4291;
        elseif delta == 0.25
            params.beta = 1;
        end
    case 'fish'
        if delta == 0.05
            params.beta = 27.2220;
        elseif delta == 0.1
            params.beta = 450;%16.6667;%46.9115;
        elseif delta == 0.15
            params.beta = 25.3686;
        elseif delta == 0.25
            params.beta = 1;
        end
    case 'gull'
        if delta == 0.05
           params.beta = 182.7048;
        elseif delta == 0.25
            params.beta = 1;
        end
    case 'house'
        if delta == 0.05
            params.beta = 42.8643;
        elseif delta == 0.25
            params.beta = 1;
        end
     case 'owl'
         if delta == 0.05
             params.beta = 24.46001;
         elseif delta == 0.1
             params.beta = 50;
         elseif delta == 0.15
             params.beta = 27.7778;%0.4572;
         elseif delta == 0.2
             params.beta = 54.2103;
         end
    case 'synthetic1'
        if delta == 0.05
           params.beta = 163.1868;
        elseif delta == 0.1
            params.beta = 172.2222;%167.9745;
        elseif delta == 0.25
            params.beta = 1;
        end
    case 'synthetic3'
        if delta == 0.05
           params.beta = 190.9358;
        elseif delta == 0.1
            params.beta = 505.5556;%345.5069;
        elseif delta == 0.25
            params.beta = 1;
        end
    case 'synthetic4'
        if delta == 0.05
           params.beta = 380.1404;
        elseif delta == 0.25
            params.beta = 1;
        end
end

[x_tvlp] = minimization(noisy,params,alpha);
subplot(3,3,5)
imshow(uint8(x_tvlp))
title('TVL^2')
SSIM_tvlp = ssim(x_tvlp,orig)
SSIM_tvlp_dr = ssim(x_tvlp,orig,'DynamicRange',255)

%%

%denoising using TVLp for p=inf
params.regularizer = 'TVLp';
params.p = 'inf';
alpha = 1;
switch image
    case 'barbara'
        if delta == 0.05
            params.beta = 4.5791e+04;
        elseif delta == 0.1
            params.beta = 4.8444e+04;%4.3111e+04;%4.3005e+04;
        elseif delta == 0.25
            params.beta = 1;
        end
    case 'brickwall'
        if delta == 0.05
           params.beta = 7.3680e+04;
        elseif delta == 0.25
            params.beta = 1;
        end
    case 'butterfly'
        if delta == 0.05
            params.beta = 4.6283e+04;
        elseif delta == 0.1
            params.beta = 4.5556e+04;%4.0889e+04;%4.0688e+04;
        elseif delta == 0.15
            params.beta = 3.7949e+04;
        elseif delta == 0.25
            params.beta = 1;
        end
    case 'cameraman'
        if delta == 0.05
           params.beta = 4.5304e+04;
        elseif delta == 0.25
            params.beta = 1;
        end
    case 'fish'
        if delta == 0.05
            params.beta = 7.8742e+04;
        elseif delta == 0.1
            params.beta = 9.6667e+04;%4.0444e+04;%4.0322e+04;
        elseif delta == 0.15
            params.beta = 3.9653e+04;
        elseif delta == 0.25
            params.beta = 1;
        end
    case 'gull'
        if delta == 0.05
           params.beta = 4.2968e+04;
        elseif delta == 0.25
            params.beta = 1;
        end
    case 'house'
        if delta == 0.05
            params.beta = 3.0136e+04;
        elseif delta == 0.25
            params.beta = 1;
        end
     case 'owl'
         if delta == 0.05
             params.beta = 2.9571e+04;
         elseif delta == 0.1
             params.beta = 1;
         elseif delta == 0.15
             params.beta = 1;%2.0827e+03;
         elseif delta == 0.2
             params.beta = 2.9497e+03;
         end
    case 'synthetic1'
        if delta == 0.05
           params.beta = 4.4776e+04;% 214.6628;
        elseif delta == 0.1
            params.beta = 4.3111e+04;%4.3281e+04;
        elseif delta == 0.25
            params.beta = 1;
        end
    case 'synthetic3'
        if delta == 0.05
           params.beta = 4.4391e+04;
        elseif delta == 0.1
            params.beta = 70000;%4.5349e+04;
        elseif delta == 0.25
            params.beta = 1;
        end
    case 'synthetic4'
        if delta == 0.05
           params.beta = 6.8817e+04;
        elseif delta == 0.25
            params.beta = 1;
        end
end

[x_tvlinf] = minimization(noisy,params,alpha);
subplot(3,3,6)
imshow(uint8(x_tvlinf))
title('TVL^{\infty}')
SSIM_tvlinf = ssim(x_tvlinf,orig)
SSIM_tvlinf_dr = ssim(x_tvlinf,orig,'DynamicRange',255)


%%

%denoising using TGV2
params.regularizer = 'TGV2';
alpha = 1;
switch image
    case 'barbara'
        if delta == 0.05
            params.beta = 191.6001;
        elseif delta == 0.1
            params.beta = 0.2111;%766.6667;%98.9209; %660.0722;
        elseif delta == 0.25
            params.beta = 191.0915;
        end
    case 'brickwall'
        if delta == 0.05
           params.beta = 191.8933;
        elseif delta == 0.25
            params.beta = 1;
        end
    case 'butterfly'
        if delta == 0.05
            params.beta = 0.0973;
        elseif delta == 0.1
            params.beta =  0.2111;%0.3444;%366.5778;%379.0810;
        elseif delta == 0.25
            params.beta = 1;
        end
    case 'cameraman'
        if delta == 0.05
           params.beta = 134.7667;
        elseif delta == 0.25
            params.beta = 1;
        end
    case 'fish'
        if delta == 0.05
            params.beta = 0.1426;%415.7834;
        elseif delta == 0.1
            params.beta = 0.1000;%33.3333;%13.7487;
        elseif delta == 0.15
            params.beta = 15.2479;
        elseif delta == 0.25
            params.beta = 1;
        end
    case 'gull'
        if delta == 0.05
           params.beta = 328.19611;
        elseif delta == 0.25
            params.bete = 1;
        end
    case 'house'
        if delta == 0.05
            params.beta = 303.9612;
        elseif delta == 0.25
            params.beta = 1;
        end
    case 'owl'
        if delta == 0.05
            params.beta =  308.8563;
        elseif delta == 0.1
            params.beta = 0.3000;%300;
        elseif delta == 0.15;
            params.beta = 0.0789;%0.2778;%366.6667;%61.8034;
        elseif delta == 0.2
            params.beta = 853.3580;
        elseif delta == 0.25
            params.beta = 420.2929;
        end
    case 'synthetic1'
        if delta == 0.05
           params.beta = 308.0027;
        elseif delta == 0.1
            params.beta = 1.1248;%500;%1;%382.0272;
        elseif delta == 0.25
            params.beta = 1;
        end
    case 'synthetic3'
        if delta == 0.05
           params.beta = 76.2258;
        elseif delta == 0.1
            params.beta = 655.5556;%763.9348;
        elseif delta == 0.25
            params.beta = 1;
        end
    case 'synthetic4'
        if delta == 0.05
           params.beta = 308.9834;
        elseif delta == 0.25
            params.beta = 1;
        end
end

[x_tgv2] = minimization(noisy,params,alpha);
subplot(3,3,7)
imshow(uint8(x_tgv2))
title('TGV^2')
SSIM_tgv2 = ssim(x_tgv2,orig)
SSIM_tgv2_dr = ssim(x_tgv2,orig,'DynamicRange',255)