function [param,SSIM] = param_opt_tvpwl ( img , noise , alpha0 )

%add gaussian noise
noisy = add_noise( img , noise );
noisy = double(noisy);
 
params.regularizer = 'TV_pwL';

ssim_neg = @(paramreg) - ssim( minimization2 (noisy , params , paramreg(1) , paramreg(2)),img)
%ssim_neg = @(alpha) - ssim( minimization (noisy , params , alpha),img)

A = -eye(2);
b = [0,0];

[param, SSIM_neg] = fmincon( ssim_neg , alpha0 , A , b);


%[param , SSIM_neg ] = fminbnd( ssim_neg , alpha0 , alphaend );

SSIM = - SSIM_neg;

end