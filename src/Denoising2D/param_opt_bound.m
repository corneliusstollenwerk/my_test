function [param_opt,SSIM] = param_opt_bound ( img , delta , reg , alpha0 , vararg )

seed = 5849774;
rng(seed)

figure
subplot(1,3,1);
imshow(uint8(img))
title('original')

%add gaussian noise
type = 'Gaussian_Bound';
noisy = add_noise( img , type , delta );
noisy = double(noisy);
subplot(1,3,2);
imshow(uint8(noisy))
title('noisy image')

sz = numel(img);
sz = sqrt(sz);

params.regularizer = reg;
params.delta = delta;

%fidelity term
params.fidelity = 'Bound';
params.bound = sz * 255 * delta;
alpha = 1;

if isequal(reg,'TV')
    param_opt = 1;
    u = minimization( noisy, params, alpha );
    SSIM = ssim(img,u,'DynamicRange',255);
    subplot(1,3,3)
    imshow(uint8(u))
    title('denoised image')
    return
end


%number of steps
d1 = 10;



%initialising ssim and intervals for second parameter
a1 = alpha0(1);
b1 = alpha0(2);
ssim_old = zeros(d1,1);
ssim_new = zeros(d1,1);

j = 1;
iteration = 0;

while j > 10^(-4)
    iteration = iteration +1;
    disp(['Iteration ',num2str(iteration)])
    %calculating ssims for initial interval
    switch reg
        case 'TV_pwL'
            for i = 1:d1
                paramv = linspace(a1,b1,d1);
    
                %do_minimization2 and calculate ssims
                params.gamma = paramv(i);
                u_TV_pwL = minimization( noisy, params, alpha );
                ssim_new(i) = ssim(img,u_TV_pwL,'DynamicRange',255);
            end
        case 'TGV2'
            for i = 1:d1
                paramv = linspace(a1,b1,d1);
    
                %do_minimization2 and calculate ssims
                params.beta = paramv(i);
                u_TGV2 = minimization( noisy, params, alpha );
                ssim_new(i) = ssim(img,u_TGV2,'DynamicRange',255);
            end
        case 'TVLp'
            for i = 1:d1
                params.p = vararg;
                paramv = linspace(a1,b1,d1);
    
                %do_minimization2 and calculate ssims
                params.beta = paramv(i);
                u_TVLp = minimization( noisy, params, alpha );
                ssim_new(i) = ssim(img,u_TVLp,'DynamicRange',255);
            end
    end
    
    %calculate difference between old maximal ssim and new maximal ssim
    [argvalue_old, argmax_old] = max(ssim_old(:));
    [argvalue_new, argmax_new] = max(ssim_new(:));
    j = abs(argvalue_old-argvalue_new);
    
    %choose new interval for optimization
    switch reg  
        case {'TV_pwL', 'TGV2', 'TVLp'}
            c = b1-a1;
            a1 = paramv(argmax_new)-(c)/d1;
            if a1 < 0
                a1 = 0;
            end
            b1 = paramv(argmax_new)+(c)/d1;
    end
    
    ssim_old = ssim_new;
end

[argvalue, argmax] = max(ssim_old(:));

%set optimal parameter values
param_opt = paramv(argmax);

%minimization and plot noisy data and calculated optimal solution
switch reg
    case 'TV_pwL'
        params.gamma = param_opt;
    case {'TGV2', 'TVLp'}
        params.beta = param_opt;
end
u = minimization( noisy, params, alpha );
subplot(1,3,3);
imshow(uint8(u))
title('reconstruction')

SSIM = ssim(img,u,'DynamicRange',255);