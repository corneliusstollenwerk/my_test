function [u_opt,iteration,param_opt,SSIM] = ssim_optimization ( img , type, delta , reg , param , vararg )

%add gaussian noise
noisy = add_noise( img , type , delta );
noisy = double(noisy);

params.regularizer = reg;
params.delta = delta;

param0 = param(1);
paramend = param(2);

%number of steps
d1 = 11;

switch type
    case 'Uniform'
        params.fidelity = 'Indicator';
        alpha = 1;
        ssim_old = zeros(d1,1);
        ssim_new = zeros(d1,1);
        
        j = 1;
        iteration = 0;
        
        if isequal(reg,'TV') == 1
            param_opt = 0;
        else
        while j > 10^(-4)
            iteration = iteration +1;
            disp(['Iteration ',num2str(iteration)])
            %calculating ssims for initial interval
            switch reg
                case 'TV_pwL'
                    for i = 1:d1
                        paramv = linspace(param0,paramend,d1);
              
                        %minimization and calculate ssims
                        params.gamma = paramv(i);
                        u_TV_pwL = minimization(noisy, params, alpha);
                        ssim_new(i) = ssim(img,u_TV_pwL);
                    end
                case 'TGV2'
                    for i = 1:d1
                        paramv = linspace(param0,paramend,d1);
    
                        %minimization and calculate ssims
                        params.beta = paramv(i);
                        u_TGV = minimization(noisy, params, alpha);
                        ssim_new(i) = ssim(img,u_TGV);
                    end
                case 'TVLp'
                    params.p = vararg;
                    for i = 1:d1
                        paramv = linspace(param0,paramend,d1);
    
                        %minimization and calculate ssims
                        params.beta = paramv(i);
                        u_TVLp = minimization(img, params, alpha);
                        ssim_new(i) = ssim(img,u_TVLp);
                    end
            end
                %calculate difference between old maximal ssim and new maximal ssim
                [argvalue_old, argmax_old] = max(ssim_old(:));
                [argvalue_new, argmax_new] = max(ssim_new(:));
                j = abs(argvalue_old-argvalue_new);
                
                %choose new interval for optimization
                c = paramend-param0;
                param0 = paramv(argmax_new)-c/d1;
                if param0 < 0
                    param0 = 0;
                end
                paramend = paramv(argmax_new)+c/d1;
                
                ssim_old = ssim_new;
        end
        end
        [argvalue, argmax] = max(ssim_old(:));
        switch reg
            case 'TV_pwL'
                param_opt = paramv(argmax);
                params.gamma = param_opt;
            case {'TVLp','TGV2'}
                param_opt = paramv(argmax);
                params.beta = param_opt;
            case 'TV'
               
        end
        u_opt = minimization(noisy, params, alpha);
        SSIM = ssim(img,u_opt);
        
    case 'Gaussian'
        params.fidelity = 'L2 Squared';
        param_opt = 1;
        
    end
        
end
