clc
clear all
close all


load('denoising_exactgrad.mat');

f1 = figure
imshow(uint8(x_tv_pwl1))
print(f1,'images/denoising_exactgrad/barb','-deps')

f2= figure
imshow(uint8(x_tv_pwl2))
print(f2,'images/denoising_exactgrad/butterfly','-deps')

f3= figure
imshow(uint8(x_tv_pwl3))
print(f3,'images/denoising_exactgrad/fish','-deps')

f4= figure
imshow(uint8(x_tv_pwl4))
print(f4,'images/denoising_exactgrad/owl','-deps')