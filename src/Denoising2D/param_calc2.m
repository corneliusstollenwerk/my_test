clc
clear all

seed = 5849774;
rng(seed)


%%
tic
image = 'barbara';
sz = 256;
type = 'Gaussian_Bound';


switch image
    case 'barbara'
           barbara256 = imread('testimages/256x256/barbara_256.tif');
           orig = double(barbara256);
    case 'brickwall'
           brickwall256 = imread('testimages/256x256/brickwall_256.tif');
           orig = double(brickwall256(:,:,1));
    case 'cameraman'
           cameraman256 = imread('testimages/256x256/camera_256.tif');
           orig = double(cameraman256);
    case 'fish'
           fish256 = imread('testimages/256x256/fish_256.tif');
           orig = double(fish256(:,:,1));
    case 'gull'
           gull256 = imread('testimages/256x256/gull_256.tif');
           orig = double(gull256(:,:,1));
    case 'house'
           house256 = imread('testimages/256x256/house_256.tif');
           orig = double(house256(:,:,1));
    case 'butterfly'
           butterfly256 = imread('testimages/256x256/butterfly_256.tif');
           orig = double(butterfly256);
    case 'owl'
           owl256 = imread('testimages/256x256/owl_256.tif');
           orig = double(owl256);
    case 'synthetic1'
           synthetic1256 = imread('testimages/256x256/synthetic1_256.tif');
           orig = double(synthetic1256);
    case 'synthetic3'
           synthetic3256 = imread('testimages/256x256/synthetic3_256.tif');
           orig = double(synthetic3256(:,:,1));
    case 'synthetic4'
           synthetic4256 = imread('testimages/256x256/synthetic4_256.tif');
           orig = double(synthetic4256(:,:,1));
end

%add uniform noise
params.delta = 0.1;
delta = params.delta;
noisy = add_noise(orig , type , delta);
noisy = double(noisy);

%fidelity term
params.fidelity = 'Bound';
params.bound = sz * 255 * delta;

img=orig;
reg = 'TGV2';
alpha0=[0,1];

[param_barb_tgv , SSIM_barb_tgv] = param_opt_bound ( img , delta , reg , alpha0  )

time1=toc
%%
tic
image = 'barbara';
sz = 256;
type = 'Gaussian_Bound';


switch image
    case 'barbara'
           barbara256 = imread('testimages/256x256/barbara_256.tif');
           orig = double(barbara256);
    case 'brickwall'
           brickwall256 = imread('testimages/256x256/brickwall_256.tif');
           orig = double(brickwall256(:,:,1));
    case 'cameraman'
           cameraman256 = imread('testimages/256x256/camera_256.tif');
           orig = double(cameraman256);
    case 'fish'
           fish256 = imread('testimages/256x256/fish_256.tif');
           orig = double(fish256(:,:,1));
    case 'gull'
           gull256 = imread('testimages/256x256/gull_256.tif');
           orig = double(gull256(:,:,1));
    case 'house'
           house256 = imread('testimages/256x256/house_256.tif');
           orig = double(house256(:,:,1));
    case 'butterfly'
           butterfly256 = imread('testimages/256x256/butterfly_256.tif');
           orig = double(butterfly256);
    case 'owl'
           owl256 = imread('testimages/256x256/owl_256.tif');
           orig = double(owl256);
    case 'synthetic1'
           synthetic1256 = imread('testimages/256x256/synthetic1_256.tif');
           orig = double(synthetic1256);
    case 'synthetic3'
           synthetic3256 = imread('testimages/256x256/synthetic3_256.tif');
           orig = double(synthetic3256(:,:,1));
    case 'synthetic4'
           synthetic4256 = imread('testimages/256x256/synthetic4_256.tif');
           orig = double(synthetic4256(:,:,1));
end

%add uniform noise
params.delta = 0.1;
delta = params.delta;
noisy = add_noise(orig , type , delta);
noisy = double(noisy);

%fidelity term
params.fidelity = 'Bound';
params.bound = sz * 255 * delta;

img=orig;
reg = 'TVLp';
vararg = 'inf';
alpha0=[0,100000];

[param_barb_tvlinf , SSIM_barb_tvlinf] = param_opt_bound ( img , delta , reg , alpha0, vararg )

time2=toc
%%
tic
image = 'barbara';
sz = 256;
type = 'Gaussian_Bound';


switch image
    case 'barbara'
           barbara256 = imread('testimages/256x256/barbara_256.tif');
           orig = double(barbara256);
    case 'brickwall'
           brickwall256 = imread('testimages/256x256/brickwall_256.tif');
           orig = double(brickwall256(:,:,1));
    case 'cameraman'
           cameraman256 = imread('testimages/256x256/camera_256.tif');
           orig = double(cameraman256);
    case 'gull'
           gull256 = imread('testimages/256x256/gull_256.tif');
           orig = double(gull256(:,:,1));
    case 'fish'
           fish256 = imread('testimages/256x256/fish_256.tif');
           orig = double(fish256(:,:,1));
    case 'house'
           house256 = imread('testimages/256x256/house_256.tif');
           orig = double(house256(:,:,1));
    case 'butterfly'
           butterfly256 = imread('testimages/256x256/butterfly_256.tif');
           orig = double(butterfly256);
    case 'owl'
           owl256 = imread('testimages/256x256/owl_256.tif');
           orig = double(owl256);
    case 'synthetic1'
           synthetic1256 = imread('testimages/256x256/synthetic1_256.tif');
           orig = double(synthetic1256);
    case 'synthetic3'
           synthetic3256 = imread('testimages/256x256/synthetic3_256.tif');
           orig = double(synthetic3256(:,:,1));
    case 'synthetic4'
           synthetic4256 = imread('testimages/256x256/synthetic4_256.tif');
           orig = double(synthetic4256(:,:,1));
end

%add uniform noise
params.delta = 0.1;
delta = params.delta;
noisy = add_noise(orig , type , delta);
noisy = double(noisy);

%fidelity term
params.fidelity = 'Bound';
params.bound = sz * 255 * delta;

img=orig;
reg = 'TVLp';
vararg = 2;
alpha0=[0,500];

[param_barb_tvl2 , SSIM_barb_tvl2] = param_opt_bound ( img , delta , reg , alpha0, vararg )
time3=toc

%%
tic
image = 'barbara';
sz = 256;
type = 'Gaussian_Bound';


switch image
    case 'barbara'
           barbara256 = imread('testimages/256x256/barbara_256.tif');
           orig = double(barbara256);
    case 'brickwall'
           brickwall256 = imread('testimages/256x256/brickwall_256.tif');
           orig = double(brickwall256(:,:,1));
    case 'cameraman'
           cameraman256 = imread('testimages/256x256/camera_256.tif');
           orig = double(cameraman256);
    case 'fish'
           fish256 = imread('testimages/256x256/fish_256.tif');
           orig = double(fish256(:,:,1));
    case 'gull'
           gull256 = imread('testimages/256x256/gull_256.tif');
           orig = double(gull256(:,:,1));
    case 'house'
           house256 = imread('testimages/256x256/house_256.tif');
           orig = double(house256(:,:,1));
    case 'butterfly'
           butterfly256 = imread('testimages/256x256/butterfly_256.tif');
           orig = double(butterfly256);
    case 'owl'
           owl256 = imread('testimages/256x256/owl_256.tif');
           orig = double(owl256);
    case 'synthetic1'
           synthetic1256 = imread('testimages/256x256/synthetic1_256.tif');
           orig = double(synthetic1256);
    case 'synthetic3'
           synthetic3256 = imread('testimages/256x256/synthetic3_256.tif');
           orig = double(synthetic3256(:,:,1));
    case 'synthetic4'
           synthetic4256 = imread('testimages/256x256/synthetic4_256.tif');
           orig = double(synthetic4256(:,:,1));
end

%add uniform noise
params.delta = 0.1;
delta = params.delta;
noisy = add_noise(orig , type , delta);
noisy = double(noisy);

%fidelity term
params.fidelity = 'Bound';
params.bound = sz * 255 * delta;

img=orig;
reg = 'TV_pwL';
alpha0=[0,100];

[param_barb_tvpwl , SSIM_barb_tvpwl] = param_opt_bound ( img , delta , reg , alpha0  )

time4=toc
% %%
% tic
% image = 'fish';
% sz = 256;
% type = 'Gaussian_Bound';
% 
% 
% switch image
%     case 'barbara'
%            barbara256 = imread('testimages/256x256/barbara_256.tif');
%            orig = double(barbara256);
%     case 'brickwall'
%            brickwall256 = imread('testimages/256x256/brickwall_256.tif');
%            orig = double(brickwall256(:,:,1));
%     case 'cameraman'
%            cameraman256 = imread('testimages/256x256/camera_256.tif');
%            orig = double(cameraman256);
%     case 'fish'
%            fish256 = imread('testimages/256x256/fish_256.tif');
%            orig = double(fish256(:,:,1));
%     case 'gull'
%            gull256 = imread('testimages/256x256/gull_256.tif');
%            orig = double(gull256(:,:,1));
%     case 'house'
%            house256 = imread('testimages/256x256/house_256.tif');
%            orig = double(house256(:,:,1));
%     case 'butterfly'
%            butterfly256 = imread('testimages/256x256/butterfly_256.tif');
%            orig = double(butterfly256);
%     case 'owl'
%            owl256 = imread('testimages/256x256/owl_256.tif');
%            orig = double(owl256);
%     case 'synthetic1'
%            synthetic1256 = imread('testimages/256x256/synthetic1_256.tif');
%            orig = double(synthetic1256);
%     case 'synthetic3'
%            synthetic3256 = imread('testimages/256x256/synthetic3_256.tif');
%            orig = double(synthetic3256(:,:,1));
%     case 'synthetic4'
%            synthetic4256 = imread('testimages/256x256/synthetic4_256.tif');
%            orig = double(synthetic4256(:,:,1));
% end
% 
% %add uniform noise
% params.delta = 0.1;
% delta = params.delta;
% noisy = add_noise(orig , type , delta);
% noisy = double(noisy);
% 
% %fidelity term
% params.fidelity = 'Bound';
% params.bound = sz * 255 * delta;
% 
% img=orig;
% reg = 'TGV2';
% alpha0=[0,1];
% 
% [param_fish , SSIM_fish] = param_opt_bound ( img , delta , reg , alpha0)
% 
% time5=toc
% %%
% tic
% image = 'owl';
% sz = 256;
% type = 'Gaussian_Bound';
% 
% 
% switch image
%     case 'barbara'
%            barbara256 = imread('testimages/256x256/barbara_256.tif');
%            orig = double(barbara256);
%     case 'brickwall'
%            brickwall256 = imread('testimages/256x256/brickwall_256.tif');
%            orig = double(brickwall256(:,:,1));
%     case 'cameraman'
%            cameraman256 = imread('testimages/256x256/camera_256.tif');
%            orig = double(cameraman256);
%     case 'fish'
%            fish256 = imread('testimages/256x256/fish_256.tif');
%            orig = double(fish256(:,:,1));
%     case 'gull'
%            gull256 = imread('testimages/256x256/gull_256.tif');
%            orig = double(gull256(:,:,1));
%     case 'house'
%            house256 = imread('testimages/256x256/house_256.tif');
%            orig = double(house256(:,:,1));
%     case 'butterfly'
%            butterfly256 = imread('testimages/256x256/butterfly_256.tif');
%            orig = double(butterfly256);
%     case 'owl'
%            owl256 = imread('testimages/256x256/owl_256.tif');
%            orig = double(owl256);
%     case 'synthetic1'
%            synthetic1256 = imread('testimages/256x256/synthetic1_256.tif');
%            orig = double(synthetic1256);
%     case 'synthetic3'
%            synthetic3256 = imread('testimages/256x256/synthetic3_256.tif');
%            orig = double(synthetic3256(:,:,1));
%     case 'synthetic4'
%            synthetic4256 = imread('testimages/256x256/synthetic4_256.tif');
%            orig = double(synthetic4256(:,:,1));
% end
% 
% %add uniform noise
% params.delta = 0.15;
% delta = params.delta;
% noisy = add_noise(orig , type , delta);
% noisy = double(noisy);
% 
% %fidelity term
% params.fidelity = 'Bound';
% params.bound = sz * 255 * delta;
% 
% img=orig;
% reg = 'TGV2';
% alpha0=[0,0.1];
% 
% [param_owl , SSIM_owl] = param_opt_bound ( img , delta , reg , alpha0 )
% time6=toc