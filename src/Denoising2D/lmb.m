 clear all
 clc

seed = 58497174;
rng(seed)

image = 'lmb3';
type = 'Gaussian';

switch image
    
    case 'lmb1'
        load('testimages/LMB/proj190.mat')
        orig = double(gt);
        noisy = double(noisy);
        
    case 'lmb2'
        load('testimages/LMB/proj302.mat')
        orig = double(gt);
        noisy = double(noisy);
        
    case 'lmb3'
        load('testimages/LMB/proj712.mat')
        orig = double(gt);
        noisy = double(noisy);
        
    case 'lmb4'
        load('testimages/LMB/proj2519.mat')
        orig = double(gt);
        noisy = double(noisy);
        
    case 'lmb5'
        load('testimages/LMB/proj3485.mat')
        orig = double(gt);
        noisy = double(noisy);
        
    case 'lmb6'
        load('testimages/LMB/proj10392.mat')
        orig = double(gt);
        noisy = double(noisy);
        
end

MIN = min(min(noisy));
MAX = max(max(noisy));
orig = mat2gray(orig,[MIN MAX]);
noisy = mat2gray(noisy, [MIN MAX]);
SSIM_noisy = ssim(noisy,orig)

figure
subplot(2,3,1)
imshow(orig)
title('original')
subplot(2,3,2)
imshow(noisy)
title('noisy image')

params.fidelity = 'L2 squared';

% generate differential operators
sg = size(orig);
D = diffopn(sg,1,'forward','neumann');
Dgt = reshape( D * orig(:) , prod(sg), 2 );


%%
switch image
    case 'lmb1'
        alpha = 663.6364;
    case 'lmb2'
        alpha = 800;
    case 'lmb3'
        alpha = 727.2727;
    case 'lmb4'
        alpha = 845.4545;
    case 'lmb5'
        alpha = 700;
    case 'lmb6'
        alpha = 700;
end

params.gamma = Dgt;
[x_tv_pwl_exact] = minimization_tv_pwl(noisy,params,alpha);
subplot(2,3,3)
imshow(x_tv_pwl_exact)
title('denoised image with exact gradient')
SSIM1 = ssim(x_tv_pwl_exact,orig)

%%
switch image
    case 'lmb1'
        alpha = 0.1454;
    case 'lmb2'
        alpha = 0.1589;
    case 'lmb3'
        alpha =0.1655;
    case 'lmb4'
        alpha = 0.1475;
    case 'lmb5'
        alpha = 0.1193;
    case 'lmb6'
        alpha = 0.1508;
end

gamma = max(abs(norms(Dgt,1,2)));
params.gamma = gamma;
params.regularizer = 'TV_pwL';
[x_tv_pwl_max] = minimization(noisy,params,alpha);
subplot(2,3,4)
imshow(x_tv_pwl_max)
title('denoised image with maximal gradient')
SSIM2 = ssim(orig,x_tv_pwl_max)

%%
switch image
    case 'lmb1'
        alpha = 1.3917;
    case 'lmb2'
        alpha = 0.9952;
    case 'lmb3'
        alpha =1.5457;
    case 'lmb4'
        alpha = 1.6304;
    case 'lmb5'
        alpha = 1.4610;
    case 'lmb6'
        alpha = 0.7834;
end
G = zeros(24,24);
gamma = reshape (abs(norms(Dgt,1,2)), sg(1),sg(2));
for i = 1:24
    for j = 1:24
        G(i,j) = max(max(gamma(i*16-15:i*16,j*16-15:j*16)));
    end
end
for i = 1:24
    for j = 1:24
        gamma( i*16-15:i*16,j*16-15:j*16) = ones(16,16)*G(i,j);
    end
end
params.gamma = reshape (gamma, prod(sg),1);
params.gamma = [params.gamma params.gamma];
[x_tv_pwl_sp] = minimization_tv_pwl(noisy,params,alpha);
subplot(2,3,6)
imshow(x_tv_pwl_sp)
title('denoised image with locally addapted gradient')
SSIM3 = ssim(x_tv_pwl_sp,orig)

%%
int = MAX - MIN;
original = orig*int + MIN;
noisyim = noisy*int + MIN;
x_exact = x_tv_pwl_exact*int + MIN;
x_max = x_tv_pwl_max*int +MIN;
x_sp = x_tv_pwl_sp*int + MIN;
figure
subplot(1,5,1)
imshow(original)
title ('Original image')
subplot(1,5,2)
imshow(noisyim)
title('Noisy image')
subplot(1,5,3)
imshow(x_exact)
title (' Reconstruction with \newline exact gradient')
subplot(1,5,4)
imshow(x_max)
title (' Reconstruction with \newline maximal gradient')
subplot(1,5,5)
imshow(x_sp)
title (' Reconstruction with \newline spatially adapted \newline gradient')