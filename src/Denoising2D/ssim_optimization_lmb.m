function [u_opt,iteration,param_opt,SSIM] = ssim_optimization_lmb ( img , noisy , param , gamma )

params.regularizer = 'TV_pwL';

param0 = param(1);
paramend = param(2);

%number of steps
d1 = 11;

params.fidelity = 'L2 squared';
param_opt = 1;
params.gamma = gamma;
ssim_old = zeros(d1,1);
ssim_new = zeros(d1,1);
j = 1;
iteration = 0;
                
while j > 10^(-4)
   iteration = iteration +1;
   disp(['Iteration ',num2str(iteration)])
   %calculating ssims for initial interval
                 
   for i = 1:d1
       paramv = linspace(param0,paramend,d1);
        
       %minimization and calculate ssims
       alpha = paramv(i);
       u_TV_pwL = minimization_tv_pwl(noisy, params, alpha);
       ssim_new(i) = ssim(img,u_TV_pwL);
   end
                    
   %calculate difference between old maximal ssim and new maximal ssim
   [argvalue_old, argmax_old] = max(ssim_old(:));
   [argvalue_new, argmax_new] = max(ssim_new(:));
   j = abs(argvalue_old-argvalue_new);
                
   %choose new interval for optimization
   c = paramend-param0;
   param0 = paramv(argmax_new)-c/d1;
   if param0 < 0
       param0 = 0;
   end
   paramend = paramv(argmax_new)+c/d1;
                
   ssim_old = ssim_new;
end
   [argvalue, argmax] = max(ssim_old(:));
   param_opt = paramv(argmax);
   alpha = param_opt;
   u_opt = minimization_tv_pwl(noisy, params, alpha);
   SSIM = ssim(img,u_opt);
end