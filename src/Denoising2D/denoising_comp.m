clear all
 clc

seed = 5849774;
%seed = 6584564; %(squares,horiz)
rng(seed)

image = 'synthetic1';
sz = 256;
type = 'Gaussian_Bound';


switch image
    case 'barbara'
           barbara256 = imread('testimages/256x256/barbara_256.tif');
           orig = double(barbara256);
    case 'brickwall'
           brickwall256 = imread('testimages/256x256/brickwall_256.tif');
           orig = double(brickwall256(:,:,1));
    case 'cameraman'
           cameraman256 = imread('testimages/256x256/camera_256.tif');
           orig = double(cameraman256);
    case 'fish'
           fish256 = imread('testimages/256x256/fish_256.tif');
           orig = double(fish256(:,:,1));
    case 'gull'
           gull256 = imread('testimages/256x256/gull_256.tif');
           orig = double(gull256(:,:,1));
    case 'house'
           house256 = imread('testimages/256x256/house_256.tif');
           orig = double(house256(:,:,1));
    case 'butterfly'
           butterfly256 = imread('testimages/256x256/butterfly_256.tif');
           orig = double(butterfly256);
     case 'owl'
           owl256 = imread('testimages/256x256/owl_256.tif');
           orig = double(owl256);
    case 'synthetic1'
           synthetic1256 = imread('testimages/256x256/synthetic1_256.tif');
           orig = double(synthetic1256);
    case 'synthetic3'
           synthetic3256 = imread('testimages/256x256/synthetic3_256.tif');
           orig = double(synthetic3256(:,:,1));
    case 'synthetic4'
           synthetic4256 = imread('testimages/256x256/synthetic4_256.tif');
           orig = double(synthetic4256(:,:,1));
end



figure
% subplot(3,3,1);
% imshow(uint8(orig))
% title('original')

%add gaussian noise
delta = 0.1; %standard deviation delta*255
noisy = add_noise(orig , type , delta);
noisy = double(noisy);
subplot(2,2,1);
imshow(uint8(noisy))
title('noisy image')

%fidelity term
params.fidelity = 'Bound';
params.bound = sz * 255 * delta;

%%
%denoising using TV_pwL
params.regularizer = 'TV_pwL';
alpha = 1;

params.gamma= 0.1;

[x_tv_pwl1] = minimization(noisy,params,alpha);
subplot(2,2,2)
imshow(uint8(x_tv_pwl1))
title('TV_{pwL},\gamma=0.1')
SSIM_tv_pwl1 = ssim(x_tv_pwl1,orig)

%%
%denoising using TV_pwL
params.regularizer = 'TV_pwL';
alpha = 1;

params.gamma= 1;

[x_tv_pwl2] = minimization(noisy,params,alpha);
subplot(2,2,3)
imshow(uint8(x_tv_pwl2))
title('TV_{pwL},\gamma=1')
SSIM_tv_pwl2 = ssim(x_tv_pwl2,orig)

%%
%denoising using TV_pwL
params.regularizer = 'TV_pwL';
alpha = 1;

params.gamma= 5;

[x_tv_pwl3] = minimization(noisy,params,alpha);
subplot(2,2,4)
imshow(uint8(x_tv_pwl3))
title('TV_{pwL},\gamma=10')
SSIM_tv_pwl3 = ssim(x_tv_pwl3,orig)