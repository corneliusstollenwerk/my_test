function [ x, p, g ] = do_minimization( img, params, alpha )
%DO_MINIMIZATION Summary of this function goes here
%   Detailed explanation goes here

% Set default values
if nargin < 3
    if isfield(params, 'alpha')
        alpha = params.alpha;
    else
        alpha = 1;
    end
end

fidelity = params.fidelity;
regularizer = params.regularizer;
gamma = params.gamma;
s = size(img);
n_ghosts = 0; % Number of ghost cells at each boundary
sg = s + (2*n_ghosts); % Size including ghost cells


cvx_begin quiet
    
    cvx_solver Mosek
    cvx_precision best

    % Set up variables
    variable u(sg)             % including ghost cells
    
    u(:) >= 0;
    
    data = u(1+n_ghosts:end-n_ghosts,...
             1+n_ghosts:end-n_ghosts);  % excluding ghost cells
    

    % Define fidelity term
    switch fidelity
        case 'Operatorbounds'       % 0 s.t. A^l u <= fu, A^u u >= fl
            dataterm = 0;
            f_lower = params.lowerbounds;
            f_upper = params.upperbounds;
            A_lower = params.loweperator;
            A_upper = params.upperoperator;
            dual variable mu1
            dual variable mu2
            mu1: A_lower * data(:) <= f_upper(:);
            mu2: -A_upper * data(:) <= -f_lower(:);           

        case 'L1'           % ||u-image||_1
            oper = params.operator;
            dataterm = norm( oper*data(:)-img(:) , 1 );

        case 'L2'           % ||u-image||_2
            dataterm = norm( data(:)-img(:) , 2 );

        case 'L2 squared'   % ||u-image||_2^2
            oper = params.operator;
            dataterm = sum_square( oper*data(:) - img(:) );

        case 'Residual'     % 0 s.t. ||u-image||_2 <= delta
            dataterm = 0;
            delta = params.delta;
            norm( data(:)-img(:), 2 ) <= 1.1 * delta;

        case 'Bounds'       % 0 s.t. fl <= u <= fu
            dataterm = 0;
            f_lower = params.lowerbounds;
            f_upper = params.upperbounds;
            f_lower(:) <= data(:) <= f_upper(:);
            
        case 'Relaxed Bounds L1'   % ||(fl-u)+||_1 + ||(u-fu)+||_1
            f_lower = params.lowerbounds;
            f_upper = params.upperbounds;
            relaxed_lower = sum(pos(f_lower(:) - data(:)));
            relaxed_upper = sum(pos(data(:) - f_upper(:)));
            dataterm = (relaxed_lower) + (relaxed_upper);
            
        case 'Relaxed Bounds Residual' % 0 s.t. ||(fl-u)+||_1 + ||(u-fu)+||_1 <= ??
            dataterm = 0;
            f_lower = params.lowerbounds;
            f_upper = params.upperbounds;
            relaxed_lower = sum(pos(f_lower(:) - data(:)));
            relaxed_upper = sum(pos(data(:) - f_upper(:)));
            relaxed_lower + relaxed_upper <= 50000;
            
        case 'Relaxed Bounds L2sq'   % ||(fl-u)+||_2^2 + ||(u-fu)+||_2^2
            f_lower = params.lowerbounds;
            f_upper = params.upperbounds;
            relaxed_lower = sum_square_pos(f_lower(:) - data(:));
            relaxed_upper = sum_square_pos(data(:) - f_upper(:));
            dataterm = (relaxed_lower) + (relaxed_upper);
    end

    
    % Define regularization term
    if alpha == 0
        reg = 0;
    else
        switch regularizer
            case 'L1'           % ||u||_1
                reg = norm( u(:) , 1 );

            case 'L2'           % ||u||_2
                reg = norm( u(:) , 2 );
                
            case 'L2_grad'      % ||Du||_2^2
                D = diffopn(sg,1,'forward','neumann');
                Du = D * u(:);
                reg = sum_square( Du );

            case 'TV-isotr'           % ||Du||_1
                D = diffopn(sg,1,'forward','neumann');
                Du = reshape( D * u(:) , prod(sg), 2 );
                reg = sum( norms( Du , 2 , 2 ) );
                
            case 'TV-anisotr'           % ||Du||_1
                D = diffopn(sg,1,'forward','neumann');
                Du = reshape( D * u(:) , prod(sg), 2 );
                reg = sum( norms( Du , 1 , 2 ) );
                
            case 'TV-isotr+L1'           % ||Du||_1
                D = diffopn(sg,1,'forward','neumann');
                Du = reshape( D * u(:) , prod(sg), 2 );
                reg = sum( norms( Du , 2 , 2 ) ) + 1e-4*norm(u,1);
                
            case 'TV-anisotr+L1'           % ||Du||_1
                D = diffopn(sg,1,'forward','neumann');
                Du = reshape( D * u(:) , prod(sg), 2 );
                reg = sum( norms( Du , 1 , 2 ) ) + 1e-4*norm(u,1);

            case 'TV-anisotr-pwL'
                D = diffopn(sg,1,'forward','neumann');
                Du = reshape( D * u(:) , prod(sg), 2 );
                variable g(size(Du))
                -gamma <= g <= gamma;
                reg = sum( norms( Du-g , 1 , 2 ) );
                   
            case 'TV-isotr-pwL'
                D = diffopn(sg,1,'forward','neumann');
                Du = reshape( D * u(:) , prod(sg), 2 );
                variable g(size(Du))
                -gamma <= g <= gamma;
                reg = sum( norms( Du-g , 2 , 2 ) );
                   
            case 'TV+L2'        % ||Du||_1 + 1e-4* ||u-u0||_2
                u0 = mean(img(:));
                D = diffopn(sg,1,'forward','neumann');
                Du = reshape( D * u(:) , prod(sg), 2 );
                reg = sum( norms( Du , 2 , 2 ) ) + 1e-4* norm( u(:) - u0, 2 );

            case 'TVLp-hom'     % ||Du-w||_1 + b/a* ||w||_p
                variable w(prod(sg), 2)
                p = params.p;
                b = params.beta;
                D = diffopn(sg,1,'forward','neumann');
                Du = reshape( D * u(:) , prod(sg), 2 );
                z = norms( w , 2 , 2);
                reg = sum( norms( Du-w , 2 , 2 ) )...
                    + b/alpha * sum( pow_pos( z , p ) );
        end
    end


    % Define objective function
    minimize( dataterm + alpha * reg );

cvx_end

if (nargout > 1)
    switch fidelity
        case 'Operatorbounds'
            % get subgradient
            p = - A_lower' * mu1 + A_upper' * mu2; %-[Al;-Au]'*[mu1; mu2]
    end
end

if not(exist('g'))
    g = [];
end

if ~strcmp(cvx_status, 'Solved')
    warning('\tProblem not solved.\tCVX Status: %s', cvx_status);
end

x = full(data);

end

