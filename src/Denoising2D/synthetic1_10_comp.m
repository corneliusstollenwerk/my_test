clc
clear all
close all


load('synthetic1_0.1comp.mat');

f1 = figure
imshow(uint8(orig))
print(f1,'images/synthetic110comp/orig','-deps')

f2= figure
imshow(uint8(noisy))
print(f2,'images/synthetic110comp/noisy','-deps')

f3= figure
imshow(uint8(x_tv_pwl1))
print(f3,'images/synthetic110comp/tvpwl01','-deps')

f4= figure
imshow(uint8(x_tv_pwl2))
print(f4,'images/synthetic110comp/tvpwl1','-deps')

f5= figure
imshow(uint8(x_tv_pwl3))
print(f5,'images/synthetic110comp/tvpwl5','-deps')