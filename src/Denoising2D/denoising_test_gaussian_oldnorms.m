 clear all
 clc

seed = 5849774;
%seed = 6584564; %(squares,horiz)
rng(seed)

image = 'barbara';
sz = 128;
type = 'Gaussian';


switch image
    case 'barbara'
        if sz == 128
            barbara128 = imread('testimages/128x128/barbara_128.tif');
            orig = double(barbara128);
        elseif sz == 256
            barbara256 = imread('testimages/256x256/barbara_256.tif');
            orig = double(barbara256);
        end
    case 'brickwall'
        if sz == 128
            brickwall128 = imread('testimages/128x128/brickwall_128.tif');
            orig = double(brickwall128(:,:,1));
        elseif sz == 256
            brickwall256 = imread('testimages/256x256/brickwall_256.tif');
            orig = double(brickwall256(:,:,1));
        end
    case 'cameraman'
        if sz == 128
            cameraman128 = imread('testimages/128x128/camera_128.tif');
            orig = double(cameraman128);
        elseif sz == 256
            cameraman256 = imread('testimages/256x256/camera_256.tif');
            orig = double(cameraman256);
        end
    case 'gray'
        if sz == 128
            gray128 = imread('testimages/128x128/grayscale_128.tif');
            orig = double(gray128);
        elseif sz == 256
            gray256 = imread('testimages/256x256/grayscale_256.tif');
            orig = double(gray256(:,:,1));
        end
    case 'gull'
        if sz == 128
            gull128 = imread('testimages/128x128/gull_128.tif');
            orig = double(gull128(:,:,1));
        elseif sz == 256
            gull256 = imread('testimages/256x256/gull_256.tif');
            orig = double(gull256(:,:,1));
        end
    case 'lena'
        if sz == 128
            lena128 = imread('testimages/128x128/lena_128.tif');
            orig = double(lena128);
        elseif sz == 256
            lena256 = imread('testimages/256x256/lena_256.tif');
            orig = double(lena256);
        end
    case 'butterfly'
        if sz == 128
            butterfly128 = imread('testimages/128x128/butterfly_128.tif');
            orig = double(butterfly128);
        elseif sz == 256
            butterfly256 = imread('testimages/256x256/butterfly_256.tif');
            orig = double(butterfly256);
        end
     case 'owl'
        if sz == 128
            owl128 = imread('testimages/128x128/owl_128.tif');
            orig = double(owl128);
        elseif sz == 256
            owl256 = imread('testimages/256x256/owl_256.tif');
            orig = double(owl256);
        end
    case 'synthetic1'
        if sz == 128
            synthetic1128 = imread('testimages/128x128/synthetic1_128.tif');
            orig = double(synthetic1128);
        elseif sz == 256
            synthetic1256 = imread('testimages/256x256/synthetic1_256.tif');
            orig = double(synthetic1256);
        end
    case 'texture'
        if sz == 128
            texture128 = imread('testimages/128x128/texture_128.tif');
            orig = double(texture128(:,:,1));
        elseif sz == 256
            texture256 = imread('testimages/256x256/texture_256.tif');
            orig = double(texture256(:,:,1));
        end

end



figure
subplot(2,3,1);
imshow(uint8(orig))
title('original')

%add gaussian noise
delta = 0.05;
noisy = add_noise(orig , type , delta);
noisy = double(noisy);
subplot(2,3,2);
imshow(uint8(noisy))
title('noisy image')

%fidelity term
params.fidelity = 'L2 squared';
% params.operator = 1;

%%

%denoising using TV
params.regularizer = 'TV';
switch image
    case 'barbara'
        if sz == 128
            alpha = 6.9103;
        elseif sz == 256
            alpha = 6.9043;
        end
    case 'brickwall'
        if sz == 128
            alpha = 1;
        elseif sz == 256
            alpha = 1;
        end
    case 'cameraman'
        if sz == 128
            alpha = 7.6163;
        elseif sz == 256
            alpha = 6.2216;
        end
    case 'gray'
        if sz == 128
            alpha = 45.3972;
        elseif sz == 256
            alpha = 81.9736;
        end
    case 'gull'
        if sz == 128
            alpha = 1;
        elseif sz == 256
            alpha = 1;
        end
    case 'lena'
        if sz == 128
            alpha = 7.5825;
        elseif sz == 256
            alpha = 7.9059;
        end
    case 'butterfly'
        if sz == 128
            alpha = 5.5509;
        elseif sz == 256
            alpha = 7.0355;
        end
    case 'owl'
        if sz == 128
            alpha = 1;
        elseif sz == 256
            alpha = 1;
        end
    case 'synthetic1'
        if sz == 128
            alpha = 24.3843;
        elseif sz == 256
            alpha = 49.3516;
        end
    case 'texture'
        if sz == 128
            alpha = 1;
        elseif sz == 256
            alpha = 1;
        end
end
[x_tv] = minimization(noisy,params,alpha);
subplot(2,3,3)
imshow(uint8(x_tv))
title('TV')
SSIM_tv = ssim(x_tv,orig)

%%
%denoising using TV_pwL
params.regularizer = 'TV_pwL';
switch image
    case 'barbara'
        if sz == 128
            %params.gamma = 1.2055;
            %alpha = 7.2841;
            params.gamma = 2.6110;
            alpha = 7.7146;
        elseif sz == 256
            params.gamma = 1.0943;
            alpha = 7.5922;
        end
    case 'brickwall'
        if sz == 128
            params.gamma = 1;
            alpha = 1;
        elseif sz == 256
            params.gamma = 1;
            alpha = 1;
        end
    case 'cameraman'
        if sz == 128
            %params.gamma = 0.8765;
            %alpha = 8.3250 ;
            params.gamma = 1.0778;
            alpha = 8.4324;
        elseif sz == 256
            params.gamma = 1.1949;
            alpha = 6.9047;
        end
    case 'gray'
        if sz == 128
            %params.gamma = 1.6656;
            %alpha = 24.7264;
            params.gamma = 1.9488;
            alpha = 223.4585;
        elseif sz == 256
            params.gamma = 0.1156;
            alpha = 33.9504;
        end
    case 'gull'
        if sz == 128
            params.gamma = 1;
            alpha = 1;
        elseif sz == 256
            params.gamma = 1;
            alpha = 1;
        end
    case 'lena'
        if sz == 128
            %params.gamma = 1.0762;
            %alpha = 8.4148;
            params.gamma = 1.5857;
            alpha = 8.3451;
        elseif sz == 256
            params.gamma = 0.7741;
            alpha = 8.5013;
        end
    case 'butterfly'
        if sz == 128
            %params.gamma = 1.4317;
            %alpha = 5.7598;
            params.gamma = 4.6261;
            alpha = 6.2508;
        elseif sz == 256
            params.gamma = 1.6844;
            alpha = 7.9911;
        end
     case 'owl'
        if sz == 128
            params.gamma = 1;
            alpha = 1;
        elseif sz == 256
            params.gamma = 1;
            alpha = 1;
        end
    case 'synthetic1'
        if sz == 128
            params.gamma = 1.9481;
            alpha = 43.3891;
        elseif sz == 256
            params.gamma = 1;
            alpha = 1;
        end
     case 'texture'
        if sz == 128
            params.gamma = 1;
            alpha = 1;
        elseif sz == 256
            params.gamma = 1;
            alpha = 1;
        end
end

[x_tv_pwl] = minimization(noisy,params,alpha);
subplot(2,3,4)
imshow(uint8(x_tv_pwl))
title('TV_{pwL}')
SSIM_tv_pwl = ssim(x_tv_pwl,orig)

%%

%denoising using TVLp
params.regularizer = 'TVLp';
params.p = 2;
switch image
    case 'barbara'
        if sz == 128
            alpha = 7.8644;
            params.beta = 0.9962;
        elseif sz == 256
            alpha = 8.2154;
            params.beta = 1.2253;
        end
    case 'brickwall'
        if sz == 128
            alpha = 1;
            params.beta = 1;
        elseif sz == 256
            alpha = 1;
            params.beta = 1;
        end
    case 'butterfly'
        if sz == 128
            alpha = 6.4562;
            params.beta = 0.4840;
        elseif sz == 256
            alpha = 8.3688;
            params.beta = 1.0559;
        end
    case 'cameraman'
        if sz == 128
            alpha = 8.4816;
            params.beta = 3.0705;
        elseif sz == 256
            alpha = 6.5223;
            params.beta = 9.7184;
        end
    case 'gray'
        if sz == 128
            alpha = 64.3544;
            params.beta = 9.1003;
        elseif sz == 256
            alpha = 141.0835;
            params.beta = 23.8741;
        end
    case 'gull'
        if sz == 128
            alpha = 1;
            params.beta = 1;
        elseif sz == 256
            alpha = 1;
            params.beta = 1;
        end
    case 'lena'
        if sz == 128
            alpha= 8.5421;
            params.beta = 1.7958;
        elseif sz == 256
            alpha = 9.2742;
            params.beta = 1.5495;
        end
     case 'owl'
        if sz == 128
            alpha = 1;
            params.beta = 1;
        elseif sz == 256
            alpha = 1;
            params.beta = 1;
        end
    case 'synthetic1'
        if sz == 128
            alpha = 38.9689;
            params.beta = 5.9802;
        elseif sz == 256
            alpha = 1;
            params.beta = 1;
        end
     case 'texture'
        if sz == 128
            alpha = 1;
            params.beta = 1;
        elseif sz == 256
            alpha = 1;
            params.beta = 1;
        end
end

[x_tvlp] = minimization(noisy,params,alpha);
subplot(2,3,5)
imshow(uint8(x_tvlp))
title('TVL^p')
SSIM_tvlp = ssim(x_tvlp,orig)

%%

%denoising using TGV2
params.regularizer = 'TGV2';

switch image
    case 'barbara'
        if sz == 128
            alpha = 29.0980;%14.9230;
            params.beta = 3.0297;%2.2613;
        elseif sz == 256
            alpha = 23.6634;
            params.beta = 2.4713;
        end
    case 'brickwall'
        if sz == 128
            alpha = 1;
            params.beta = 1;
        elseif sz == 256
            alpha = 1;
            params.beta = 1;
        end
    case 'butterfly'
        if sz == 128
            alpha = 1.0471;
            params.beta = 0.8828;
        elseif sz == 256
            alpha = 10.0120;
            params.beta = 2.2071;
        end
    case 'cameraman'
        if sz == 128
            alpha = 20;%1.6191;
            params.beta = 1;%0.9993;
        elseif sz == 256
            alpha = 7.0045;
            params.beta = 2.9978;
        end
    case 'gray'
        if sz == 128
            alpha = 19.4687;
            params.beta = 11.4590;
        elseif sz == 256
            alpha = 20.0609;
            params.beta = 20.0340;
        end
    case 'gull'
        if sz == 128
            alpha = 1;
            params.beta = 1;
        elseif sz == 256
            alpha = 1;
            params.beta = 1;
        end
    case 'lena'
        if sz == 128
            alpha= 12;
            params.beta = 4;
        elseif sz == 256
            alpha = 8;
            params.beta = 5;
        end
    case 'owl'
        if sz == 128
            alpha = 1;
            params.beta = 1;
        elseif sz == 256
            alpha = 1;
            params.beta = 1;
        end
     case 'synthetic1'
        if sz == 128
            alpha = 17.0106;
            params.beta = 5.5320;
        elseif sz == 256
            alpha = 1;
            params.beta = 1;
        end
     case 'texture'
        if sz == 128
            alpha = 1;
            params.beta = 1;
        elseif sz == 256
            alpha = 1;
            params.beta = 1;
        end
end

[x_tgv2] = minimization(noisy,params,alpha);
subplot(2,3,6)
imshow(uint8(x_tgv2))
title('TGV^2')
SSIM_tgv2 = ssim(x_tgv2,orig)